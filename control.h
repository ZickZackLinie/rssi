
#ifndef _CONTROL_H
#define _CONTROL_H

#include <thread>
#include <mutex>
#include <condition_variable>	// std::condition_variable, std::cv_status
#include <vector>
#include <list>

#include <udpbuffer.h>

#define C_UNINIT    0
#define C_STARTING  1
#define C_RUNNING   2
#define C_SHUTDOWN  3
#define C_OFF       4

// time to wait for all CIs to send their sequences
#define C_SEQHDL_RESPONSE_TIME 60

class Control {
private:

	static int state;
	static std::mutex mtx_state_acc;
	static void setState (int st);  // blocking
	static int getState ();         // blocking

	// RECEIVING
	static std::thread th_NW_RX;
	static void nwReceive ();

	// TRANSMITTING
	static std::thread th_NW_TX;
	static void nwTransmit ();
	static void nwSendToAll (Packet * pShape, bool viaBC, bool inclMe);
	static std::vector<int> nwSendToList (Packet * pShape, std::vector<uint8_t> hosts);

	// HANDLE PACKETS / DOING STUFF
	static std::thread th_NW_HANDLE;
	static void nwHandle ();
	
	static int doExecCmd (std::string cmd);
	static int doSetTime ();

	static std::mutex mtx_sequence_acc;
	static int sequenceState;
	static uint16_t sequenceLevel;
	static void doSequenceStart (	struct in_addr master, 
									mtime_t sleepSecs, 
									uint16_t level);
	static void doSequenceStop ();

	static int batadvStart ();
	static int batadvStop (int sec);
	
	static NetlinkConnector * pNC;

	static UDPBuffer * pUDPB;
	static std::thread th_UDPB;
	
	static struct in_addr sin_addr_bc;	// my BC address
	static struct in_addr sin_addr;		// my own ip address
	static std::string ssin_addr; 		// same, but string
	
public:
	static int setup ();
	static int startNetworking ();
};

#endif