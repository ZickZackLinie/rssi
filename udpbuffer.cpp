#include <netinet/in.h>
#include <sys/socket.h>
//#include <cstddef>
#include <algorithm> // sort, remove_if
#include <unistd.h> // sleep
#include <ctime> // time(0)

#include <udpbuffer.h>

// ==================================================================
// PRIVATE STUFF
// ==================================================================

// CARE ABOUT OWN STATE
int UDPBuffer::
getState () {
	int ret = 0;
	mtx_state_acc.lock();
	ret = state;
	mtx_state_acc.unlock();
	return ret;
}
void UDPBuffer::
setState (int st) {
	mtx_state_acc.lock();
	if (st >= state)
		state = st;
	mtx_state_acc.unlock();
}

// CARE ABOUT RESERVATION OF UNIQUE JOBNUMBER
int UDPBuffer::
getJobNum () {
	int ret = 0;
	mtx_job_acc.lock();
	ret = ++jobnr;
	mtx_job_acc.unlock();
	return ret;
}

// CARE ABOUT LOCKING INPUT/OUTPUT BUFFER
void UDPBuffer::
lock (int mx) {
	switch (mx) {
	case UDPB_BUFFER_IN:
		mtx_in_acc.lock(); return;
	case UDPB_BUFFER_OUT:
		mtx_out_acc.lock(); return;
	case UDPB_BUFFER_ACK:
		mtx_ack_acc.lock(); return;
	}
}
void UDPBuffer::
unlock (int mx) {
	switch (mx) {
	case UDPB_BUFFER_IN:
		mtx_in_acc.unlock(); return;
	case UDPB_BUFFER_OUT:
		mtx_out_acc.unlock(); return;
	case UDPB_BUFFER_ACK:
		mtx_ack_acc.unlock(); return;
	}
}

// only lock, get, unlock, return
// NEEDS DELETE ON RETURNED PACKET!!!
Packet * UDPBuffer::
findInPacket () {   // nonblocking, can be null
	Packet * tmpPkt = nullptr;
	Packet * retPkt = nullptr;
	// CHECK IF ANY VECTOR OF PACKETS IS COMPLETE
	lock(UDPB_BUFFER_IN);
	for (size_t row = 0; row < inlist.size(); ++row) {
		// assume non-empty lists
		tmpPkt = inlist[row][0];
		if (inlist[row].size() != tmpPkt->pktmax)
			continue; // not complete
		if (tmpPkt->calcState(PKT_CS_FORIN, 0) != PKT_S_FRESH)
			continue; // already handled

		// found a ready row - SORT IT
		sort(inlist[row].begin(),
			inlist[row].end(),
			[](const Packet * a, const Packet * b) -> bool
			{
				return a->pktnum < b->pktnum;
			});

		// prepare return-packet
		tmpPkt = inlist[row][0];
		retPkt = new Packet(tmpPkt->job,
			tmpPkt->type,
			0, // represents 'all together'
			tmpPkt->pktmax,
			tmpPkt->target);
		retPkt->port = tmpPkt->port;
		retPkt->useBC = tmpPkt->useBC;
		retPkt->setContent(tmpPkt->content);
		// mark old packet as completed
		tmpPkt->reconstructed = true;
		
		// join remaining data
		for (size_t pidx = 1; pidx < inlist[row].size(); ++pidx) {
			tmpPkt = inlist[row][pidx];
			retPkt->content.insert(
				retPkt->content.end(),
				tmpPkt->content.begin(),
				tmpPkt->content.end()
				); // appending data
			// mark old packet as completed
			tmpPkt->reconstructed = true;
		}

		// delete data row
		//inlist[row].clear();
		//inlist.erase(inlist.begin()+row);
		unlock(UDPB_BUFFER_IN);
		return retPkt;
	}
	// found no ready row, return null pointer
	unlock(UDPB_BUFFER_IN);
	return retPkt;
}
// NEEDS DELETE ON RETURNED PACKET!!!
Packet * UDPBuffer::
findOutPacket () {  // nonblocking, can be null
	Packet * retPkt = nullptr;
	Packet * tmpPkt = nullptr;

	// FIRST LOOK UP ACK-LIST
	lock(UDPB_BUFFER_ACK);
	if ( ! acklist.empty() ) {
		retPkt = acklist.front();
		acklist.pop();
	}
	unlock(UDPB_BUFFER_ACK);
	if (retPkt != nullptr)
		return retPkt;

	// THEN LOOK UP OUT-LIST
	lock(UDPB_BUFFER_OUT);
	for (size_t row = 0; row < outlist.size(); ++row) {
		// assume non-empty lists
		for (size_t pidx = 0; pidx < outlist[row].size(); ++pidx) {
			tmpPkt = outlist[row][pidx];
			mtime_t now = time(0);
			if ((tmpPkt->calcState(PKT_CS_FOROUT, now) == PKT_S_FRESH)
				&& (tmpPkt->ttime < now)) { 
				// found one (false means output calculation)
				// create deletable copy
				tmpPkt->cntsnd ++;
				tmpPkt->ttime = now;
				retPkt = new Packet(
					tmpPkt->job,
					tmpPkt->type,
					tmpPkt->pktnum,
					tmpPkt->pktmax,
					tmpPkt->target );
				retPkt->port = tmpPkt->port;
				retPkt->setContent(tmpPkt->content);
				unlock(UDPB_BUFFER_OUT);
				return retPkt;
			}
		}
	}
	unlock(UDPB_BUFFER_OUT);
	return nullptr;
}


void UDPBuffer::
setAckThis (Packet * ackPkt) { // blocking
	Packet * tmpPkt = nullptr;
	mtime_t now = time(0);
	// LOOK UP OUT-LIST
	lock(UDPB_BUFFER_OUT);
	for (size_t row = 0; row < outlist.size(); ++row) {
		Packet * first = outlist[row][0];
		// assume non-empty lists
		if ( first->job != ackPkt->job) {
			continue;
		}
		// being in the right row
		// if was BC save IP:
		if (first->useBC) {
			struct in_addr ip = ackPkt->target;
			bool found = false;
			for (auto other_ip : first->answerIPs) {
				if (ip.s_addr == other_ip.s_addr) {
					found = true;
					break;
				}
			}
			if (!found)
				first->answerIPs.push_back(ip);
			first->ttime = now; // update to be longest available
		}
		// do normal stuff
		for (size_t pidx = 0; pidx < outlist[row].size(); ++pidx) {
			tmpPkt = outlist[row][pidx];
			if (tmpPkt->pktnum == ackPkt->pktnum) { // found right one
				tmpPkt->acked = true;
				tmpPkt->ttime = now;
				unlock(UDPB_BUFFER_OUT);
				return;
			}
		}
	}
	unlock(UDPB_BUFFER_OUT);
	printf("ACKTHIS: CANNOT FIND PACKET!\n");
}
Packet * UDPBuffer::
makeAckPkt (Packet * inPkt) { // creates ack-packet from incomming
	Packet * newAck = new Packet(
		inPkt->job,
		PKT_ACK,
		inPkt->pktnum,
		inPkt->pktmax,
		inPkt->target);
	// port should always be single ip -> PT_UDP_TK
	// dont set it.
	//newAck->acked = true; // always self-acked // unnecessary
	return newAck;
}


// ==================================================================
// PUBLIC STUFF
// ==================================================================

UDPBuffer::
UDPBuffer () {
	state = UDPB_S_STARTING;
}

// use condition vars
// NEEDS DELETE ON RETURNED PACKET!!!
Packet * UDPBuffer::
getInPacket () {  // blocking, can be null (if shut down), data: 0..inf
	// FIRST: CHECK STATE
	int cur_state = getState();
	if (cur_state != UDPB_S_RUNNING)
		return nullptr;
	std::unique_lock<std::mutex> lck(mtx_in_notify);
	Packet * retPkt = nullptr;
	
	while (1) {
		// CHECK STATE AGAIN
		cur_state = getState(); // blocking
		if ((cur_state == UDPB_S_OFF) || 
			(cur_state == UDPB_S_SHUTDOWN) || 
			(cur_state == UDPB_S_CLEANUP))
			return nullptr; // someone switched us off, thats okay
		
		// CHECK AGAIN FOR DATA (here because of notification or first LU)
		if ((retPkt = findInPacket()) != nullptr)
			return retPkt;

		// WAIT FOR DATA
		while (cv_in.wait_for(lck,std::chrono::seconds(UDPB_TIMEOUT))
				== std::cv_status::timeout) {
			// use timeout to check running state
			cur_state = getState(); // blocking
			if ((cur_state == UDPB_S_OFF) || 
				(cur_state == UDPB_S_SHUTDOWN) || 
				(cur_state == UDPB_S_CLEANUP))
				return nullptr; // someone switched us off, thats okay
			if ((retPkt = findInPacket()) != nullptr)
				return retPkt; // there is data - we missed a notification
		}
	}
}
// NEEDS DELETE ON RETURNED PACKET!!!
Packet * UDPBuffer::
getOutPacket () { // blocking, can be null (if shut down), data: 0..503
	// FIRST: CHECK STATE
	int cur_state = getState();
	if (cur_state != UDPB_S_RUNNING)
		return nullptr;
	std::unique_lock<std::mutex> lck(mtx_out_notify);
	Packet * retPkt = nullptr;
	
	while (1) {
		// CHECK STATE AGAIN
		cur_state = getState(); // blocking
		if ((cur_state == UDPB_S_OFF) || (cur_state == UDPB_S_CLEANUP))
			return nullptr; // someone switched us off, thats okay
		
		// CHECK AGAIN FOR DATA (here because of notification or first LU)
		if ((retPkt = findOutPacket()) != nullptr)
			return retPkt;

		// WAIT FOR DATA
		while (cv_out.wait_for(lck,std::chrono::seconds(UDPB_TIMEOUT))
				== std::cv_status::timeout) {
			// use timeout to check running state
			cur_state = getState(); // blocking
			if ((cur_state == UDPB_S_OFF) || (cur_state == UDPB_S_CLEANUP))
				return nullptr; // someone switched us off, thats okay
			if ((retPkt = findOutPacket()) != nullptr)
				return retPkt; // there is data - we missed a notification
		}
	}
}

// add packets
void UDPBuffer::
receivedThis (Packet * pkt) {
	int cur_state = getState();
	if (cur_state != UDPB_S_RUNNING)
		return;

	if (pkt->pktnum > pkt->pktmax || pkt->pktnum == 0 || pkt->pktmax == 0)
		return;

	// 0. note down signs of life (node is up and chatty)
	gotSomethingFrom(pkt->target);

	// 1. check if ack-type
	//    -> ack the packet & return
	if (pkt->type == PKT_ACK)
		return setAckThis(pkt);
	// 2. check if shouldBeAcked
	//    -> create ack to send, set ack
	if (pkt->shouldBeAcked()) {
		Packet * ackPkt = makeAckPkt(pkt);
		lock(UDPB_BUFFER_ACK);
		acklist.push(ackPkt);
		unlock(UDPB_BUFFER_ACK);
		pkt->acked = true;
		cv_out.notify_one();
	}
	// 3. check if is in inlist
	//    in: update ttime
	//    not: 
	//        has others of queue: append
	//        has not: insert as new
	Packet * tmpPkt = nullptr;
	mtime_t now = time(0);
	lock(UDPB_BUFFER_IN);
	for (size_t row = 0; row < inlist.size(); ++row) {
		// assume non-empty lists
		Packet * first = inlist[row][0];
		if ((first->job != pkt->job) || 
			(first->target.s_addr != pkt->target.s_addr))
			continue;
		bool found = false;
		for (size_t pidx = 0; pidx < inlist[row].size(); ++pidx) {
			tmpPkt = inlist[row][pidx];
			tmpPkt->ttime = now; // always keep the row up-to-date
			if (tmpPkt->pktnum == pkt->pktnum) { // found: already in
				found = true;
				tmpPkt->cntsnd ++; // only for stats, cnt how often rxed
				printf("UDPB: received packet multiple times: ");
				tmpPkt->printHdr();
			}
		}
		// if not found in list, append
		if (!found) {
			inlist[row].push_back(pkt);
			cv_in.notify_one();
		}
		unlock(UDPB_BUFFER_IN);
		return;
	}
	// insert as new
	inlist.push_back(std::vector<Packet *>(1, pkt) );
	unlock(UDPB_BUFFER_IN);
	cv_in.notify_one();
}
int UDPBuffer::
sendThis (int type, 
	std::vector<uint8_t> &data, 
	struct in_addr target) {
	int cur_state = getState();
	if (cur_state != UDPB_S_RUNNING) {
		printf("Packet declined: not running\n");
		return -1;
	}
	// create packets from given data
	// make as much packets as necessary
	std::vector<std::vector<Packet *> > newlist =
		std::vector<std::vector<Packet *> >
		(1, std::vector<Packet *>() );
	size_t szall = data.size();
	size_t num = (((int)szall-1) / PKT_MAX_SIZE) + 1;
	if (num >= PKT_MAX_SPLIT) {
		printf("Packet data too big: only %u * %uB = %u bytes usable!\n",
			PKT_MAX_SPLIT,
			PKT_MAX_SIZE,
			PKT_MAX_SPLIT * PKT_MAX_SIZE -1);
	}
	int jobnr = getJobNum();
	for (size_t idx = 0; idx < num; idx++) {
		size_t start = idx * PKT_MAX_SIZE;
		size_t bytes = szall - start;
		bytes = (bytes > PKT_MAX_SIZE ? PKT_MAX_SIZE : bytes); // cap!
		// initialize!
		Packet * tmp = new Packet(
			jobnr,
			type,
			(uint8_t)idx + 1,
			(uint8_t)num,
			target );
		// assume single ip for target
		// --> dont set port, should be PT_UDP_TK
		tmp->setContent(&data[start], bytes);
		newlist[0].push_back(tmp);
	}

	// determine priority by type!
	// append to/place in front of outlist
	if (newlist[0][0]->isTimeCritical()) {
		// place in front of outlist
		lock(UDPB_BUFFER_OUT);
		outlist.insert(
			outlist.begin(),  // FRONT
			newlist.begin(),
			newlist.end() );
		unlock(UDPB_BUFFER_OUT);
	} else {
		// append to outlist
		lock(UDPB_BUFFER_OUT);
		outlist.insert(
			outlist.end(),    // END
			newlist.begin(),
			newlist.end() );
		unlock(UDPB_BUFFER_OUT);
	}
	return jobnr;
}


// USE WITH CARE: content won't be validated
void UDPBuffer::
placeInBuffer (Packet * pkt, const int BUFFER) {
	int cur_state = getState();
	if (cur_state != UDPB_S_RUNNING) {
		printf("Packet declined: not running\n");
		return;
	}
	if (BUFFER == UDPB_BUFFER_IN) {
		lock(UDPB_BUFFER_IN);
		inlist.push_back(std::vector<Packet *>(1, pkt) );
		unlock(UDPB_BUFFER_IN);
		cv_in.notify_one();
	} else if (BUFFER == UDPB_BUFFER_OUT) {
		lock(UDPB_BUFFER_OUT);
		if (pkt->isTimeCritical()) {
			// push to front
			outlist.insert(outlist.begin(), std::vector<Packet *>(1, pkt));
		} else {
			// push to end
			outlist.push_back(std::vector<Packet *>(1, pkt) );
		}
		unlock(UDPB_BUFFER_OUT);
		cv_out.notify_one();
	} else if (BUFFER == UDPB_BUFFER_ACK) {
		lock(UDPB_BUFFER_ACK);
		acklist.push(pkt);
		unlock(UDPB_BUFFER_ACK);
		cv_out.notify_one();
	}
}


bool UDPBuffer::
findAnswerIPs(int jobnr, std::vector<struct in_addr> &ips) {
	// LOOK UP OUT-LIST
	lock(UDPB_BUFFER_OUT);
	for (auto row : outlist) {
		// assume non-empty lists
		Packet * first = row[0];
		if ( first->job != jobnr)
			continue;
		// being in the right row
		for (auto other_ip : first->answerIPs) {
			bool found = false;
			for (auto in_ip : ips) {
				if (in_ip.s_addr == other_ip.s_addr) {
					found = true;
					break;
				}
			}
			// only insert ip if not already in
			if (!found)
				ips.push_back(other_ip);
		}
		unlock(UDPB_BUFFER_OUT);
		return true;
	}
	unlock(UDPB_BUFFER_OUT);
	return false;	
}


// STATS ABOUT ALL PARTICIPANTS
std::vector<struct in_addr> UDPBuffer::
currentNodes () {
	mtime_t acceptable = time(0) - UDPB_TIME_RMNODE;
	std::vector<struct in_addr> ret;
	// SORT OUT TOO OLD IPs
	lastSeenList.erase(
		std::remove_if(
			lastSeenList.begin(), lastSeenList.end(),
			[=](std::pair<struct in_addr, mtime_t> _pair){
				return (_pair.second < acceptable);
			} ),
		lastSeenList.end() );
	// FILL RETURN BUFFER
	for (auto _pair : lastSeenList)
		ret.push_back(_pair.first);
	return ret;
}

void UDPBuffer::
gotSomethingFrom (struct in_addr node) {
	bool found = false;
	mtime_t now = time(0);
	for (auto _pair : lastSeenList) {
		if (_pair.first.s_addr == node.s_addr) {
			found = true;
			_pair.second = now;
			break;
		}
	}
	if (!found)
		lastSeenList.push_back(
			std::pair<struct in_addr, mtime_t>(node, now));
}


// thread
void UDPBuffer::
start_thread (UDPBuffer *pbu) {
	pbu->run_blocking();
}

void UDPBuffer::
run_blocking () {
	//int rounds = 0;
	int cur_state = getState();
	Packet * tmpPkt = nullptr;
	// only start once!
	if (cur_state != UDPB_S_STARTING) {
		setState(UDPB_S_OFF);
		return;
	}
	// do init-stuff
	setState(UDPB_S_RUNNING);
	while (1) {
		// 1 second before rescan data for delete
		//printf("run: sleep then check, round %d\n", rounds);
		sleep(1);
		// CHECK STATE AGAIN
		cur_state = getState(); // blocking
		if ((cur_state == UDPB_S_OFF) || (cur_state == UDPB_S_SHUTDOWN))
			break; // someone switched us off, thats okay
		//if (rounds++ > 40)
		//	break;

		mtime_t now = time(0);
		mtime_t acceptable = now - UDPB_DISCARD_TIME;
		
		// CLEANING OUT-LIST
		lock(UDPB_BUFFER_OUT);
		for (size_t row = 0; row < outlist.size(); ++row) {
			outlist[row].erase(
				std::remove_if(
					outlist[row].begin(), outlist[row].end(),
					[=](Packet * ptr){
						if (ptr->calcState(PKT_CS_FOROUT, acceptable) == PKT_S_OVER) {
							printf("\033[0;34mDELETE (out): ");ptr->printHdr();
							delete ptr;
							return true;
						} else {
							//printf("WAIT FOR OUT: ");ptr->printHdr();
							return false;
						}
					} ),
				outlist[row].end() );
		}
		// now remove every empty vector
		outlist.erase(
			std::remove_if(
				outlist.begin(), outlist.end(),
				[](std::vector<Packet *> &vec){
					return vec.size() == 0;
				} ),
			outlist.end() );
		unlock(UDPB_BUFFER_OUT);

		// CLEANING IN-LIST
		lock(UDPB_BUFFER_IN);
		for (size_t row = 0; row < inlist.size(); ++row) {
			inlist[row].erase(
				std::remove_if(
					inlist[row].begin(), inlist[row].end(),
					[=](Packet * ptr){
						if (ptr->calcState(PKT_CS_FORIN, acceptable) == PKT_S_OVER) {
							printf("\033[0;34mDELETE (in): ");ptr->printHdr();
							delete ptr;
							return true;
						} else {
							//printf("WAIT FOR IN: ");ptr->printHdr();
							return false;
						}
					} ),
				inlist[row].end() );
		}
		// now remove every empty vector
		inlist.erase(
			std::remove_if(
				inlist.begin(), inlist.end(),
				[](std::vector<Packet *> &vec){
					return vec.size() == 0;
				} ),
			inlist.end() );
		unlock(UDPB_BUFFER_IN);
		
	}
	setState(UDPB_S_SHUTDOWN);
	// 2 + 5sec for getXPacket timeout
	sleep(UDPB_TIMEOUT + 2);
	
	printf("\nCLEANING UP!!\n");
	
	// DELETE ALL ACKs
	lock(UDPB_BUFFER_ACK);
	while (!acklist.empty()) {
		tmpPkt =  acklist.front();
		printf("\033[0;34mDELETE(ack): ");tmpPkt->printHdr();
		delete tmpPkt;
		acklist.pop();
	}
	unlock(UDPB_BUFFER_ACK);

	// DELETE ALL IN-PACKETS
	lock(UDPB_BUFFER_IN);
	for (size_t row = 0; row < inlist.size(); ++row) {
		for (size_t idx = 0; idx < inlist[row].size(); ++idx) {
			tmpPkt = inlist[row][idx];
			printf("\033[0;34mDELETE(in): ");tmpPkt->printHdr();
			delete tmpPkt;
		}
		inlist[row].clear();
	}
	inlist.clear();
	unlock(UDPB_BUFFER_IN);

	// DELETE ALL OUT-PACKETS
	lock(UDPB_BUFFER_OUT);
	for (size_t row = 0; row < outlist.size(); ++row) {
		for (size_t idx = 0; idx < outlist[row].size(); ++idx) {
			tmpPkt = outlist[row][idx];
			printf("\033[0;34mDELETE(out): ");tmpPkt->printHdr();
			delete tmpPkt;
		}
		outlist[row].clear();
	}
	outlist.clear();
	unlock(UDPB_BUFFER_OUT);

	setState(UDPB_S_OFF);
}

void UDPBuffer::
stop () {
	setState (UDPB_S_SHUTDOWN);
}