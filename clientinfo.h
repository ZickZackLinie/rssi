#ifndef _CLIENTINFO_H
#define _CLIENTINFO_H

#include <map>
#include <vector>
#include <mutex>
#include <cstring>

#include <messreihe.h>
#include <utils.h>

class ClientInfo {
private:
	float lat; // latitude
	float lon; // longitude
	uint32_t txpower; // transmitting power of antenna * 100
	unsigned char mac[ETH_ALEN]; // own mac address
	std::string smac; // text version of mac address
	mtime_t lastSeen; // when was last update (/append)
	int maxDataKeep = MAX_DATA_KEEP;
	
	// 
	// MESSDATEN in unordered_map !!!
	//
	std::map<uint16_t, std::map<std::string, Messreihe *> > mapping;
	std::mutex mtx;
	
	void lock(const char * reason, bool silent);
	void unlock(const char * reason, bool silent);

public:
	// how many different time-boxes exist
	//uint16_t maxLevel = 1;
	// the current selected box
	//uint16_t curentLevel = 0;
	// knows its position?
	uint8_t anchorState;
	// ending of ip address
	uint8_t hostnum;
	// if every data is live data
	bool isLiveData = true;
	
	// par ex.:
	// char tmpmac[] = "02:12:34:56:78:9a";
	// ClientInfo CI(tmpmac, 2000, CI_AN_YES, 51.48642f, 11.98093f);
	ClientInfo(	const char * mac_name, uint8_t hostnum, 
				uint32_t txPower, uint8_t anchorState,
				float latitude, float longitude);

	void append(uint16_t level, const char * name, mtime_t time, uint8_t rssi);
	mtime_t getLastSeen();
	std::string getName();
	bool getPos(float &outlat, float &outlon);
	
	std::vector<std::string> getConnectionNames(uint16_t level);
	Messreihe * getConnection (uint16_t level, std::string Mac);
	void eraseConnections(std::string Mac);

	// update myself from other instance
	// if lastSeen is newer
	int update(ClientInfo * ci);

	void allMakeMeanOnly ();
	void allMakeLiveData ();
	void allSetMaxKeep (int maxNum);
	// shrink value pairs to the last "num"
	void allShrinkToLast(int num);
	// delete every pair older than seconds
	void allShrinkToNewerThan(uint16_t level, mtime_t timeInSeconds);

	std::vector<uint16_t> getLevels(bool onlySignificant);
	void eraseLevels(std::vector<uint16_t> levels);
	
	// write itself into vector buffer.
	// return # bytes written
	// OPT: only significant stations/connections
	// OPT: max # connections per station
	//		iff = 1: use mean values!
	int serialize (	std::vector<uint8_t> &streambuffer,
					bool onlySignificant, uint16_t maxPerStation,
					std::vector<uint16_t> levels );
	int deserialize (std::vector<uint8_t> streambuffer);
	bool buildDistanceFiles (uint16_t level, std::string prefixDir);
	bool buildHistFile (int toHost, std::string prefixDir);
	ClientInfo * copy();

	void print(bool onlySignificant, uint16_t maxPerStation);

};

#endif