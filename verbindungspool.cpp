#include <iostream>
#include <fstream> // buildGraph
#include <cstdio>
#include <cstring>
#include <string>
#include <unordered_map>
#include <map>
#include <vector>
#include <mutex>


#include <verbindungspool.h>
#include <clientinfo.h>
#include <messreihe.h> // buildGraph
#include <utils.h>

// use:
	/* nope... */


void VerbindungsPool::
createMyInfo (	ClientInfo * ci) {
	mergeOrInsert(ci);
	pmyself = ci;
}
void VerbindungsPool::
createMyInfo (	const char * macstring, uint8_t hostnum,
				uint32_t txpower, uint8_t anchor,
				float latitude, float longitude) {
	ClientInfo * ci = new ClientInfo(macstring, hostnum, txpower, anchor, latitude, longitude);
	mergeOrInsert(ci);
	pmyself = ci;
}



void VerbindungsPool::
mergeOrInsert (ClientInfo * ci) {
	lock("(merge or insert)", true); //lock mutex

	std::string name = ci->getName();
	// first look up:
	std::unordered_map<std::string, ClientInfo * >::const_iterator got
		= mapping.find(name);


	ClientInfo * tmp = nullptr;
	if (got == mapping.end()) {
		mapping.insert(std::pair<std::string, ClientInfo *>(name, ci));
	} else {
		tmp = got->second;
		tmp->update(ci);
		delete ci;
	}
	
	unlock("(merge or insert)", true); // release mutex
}

void VerbindungsPool::
mergeOrInsert (std::vector<uint8_t> &streambuffer) {
	ClientInfo * ci = new ClientInfo("00:00:00:00:00:00",0,0,CI_AN_UNKNOWN,0,0);
	ci->deserialize(streambuffer);
	mergeOrInsert(ci);
}


int VerbindungsPool::
appendToMe (uint16_t level, const char * name, mtime_t ttime, uint8_t rssi) {
	if (pmyself == nullptr) {
		return -1;
	}
	pmyself->append(level, name, ttime, rssi);
	return 0;
}

void VerbindungsPool::
cleanUp (int vp_rm_howFar) {
	// LOCK TO CLEAN UP
	lock("(cleanUp)", true);
	for (std::pair<std::string, ClientInfo *> m_s_ci: mapping) {
		ClientInfo * CI = m_s_ci.second;
		if (vp_rm_howFar >= VP_RM_DATA) {
			CI->allShrinkToLast(0);
		}
	}
	if (vp_rm_howFar >= VP_RM_CIOTHER) {
		for (auto it = mapping.begin(); it != mapping.end(); ) {
			if (it-> second != pmyself) {
				delete it->second;
				it = mapping.erase(it);
			} else if (vp_rm_howFar >= VP_RM_CIALL) {
				delete it->second;
				pmyself = nullptr;
				it = mapping.erase(it);
			} else {
				++ it;
			}
		}
	}
	unlock("(cleanUp)", true);
}


ClientInfo * VerbindungsPool::
get (std::string staName) {
	if (staName == "")
		return pmyself;

	std::unordered_map<std::string, ClientInfo * >::const_iterator got
		= mapping.find(staName);

	ClientInfo * tmp = nullptr;
	if (got != mapping.end()) {
		tmp = got->second;
	}
	return tmp;
}

int VerbindungsPool::
count () {
	lock("(counting)", true); //lock mutex
	int ret = mapping.size();
	unlock("(counting)", true); // release mutex
	return ret;
}


void VerbindungsPool::
buildGraph () {

	/*// SHALL WE DO SOMETHING??
	if (!Options::useBINFileOut && !Options::useASCIIFileOut)
		return;
	// node name resolution
	uint16_t nodeidx = 0;
	std::map<uint16_t,uint16_t> nameres; // [hostnum]->nodeidx
	std::map<uint16_t,uint16_t> nameres_rev; // [nodeidx]->hostnum ??
	std::unordered_map<std::string, ClientInfo *> mapcopy;
	std::vector< int > disscnt;// { c(i,j), {c(i,j) ...} (i<j!)
	std::vector< double > dissval;// { v(i,j), v(i,j) ...} (i<j!)
	std::vector< std::vector<double> > coordinates; // { {x,y}, {x,y} ...}
	std::vector<uint16_t> anchors; // { idx, idx, idx, ...}
	
	
	// at first: copy every CI to not destroy them
	lock("(building graph)", false);
	for (std::pair<std::string, ClientInfo *> m_s_ci: mapping) {
		ClientInfo * CI = m_s_ci.second;
		mapcopy.insert(
			std::pair<std::string, ClientInfo *>(
				std::string(m_s_ci.first), CI->copy()));
		// save coordinates if existing
		float ci_lat = 0;
		float ci_lon = 0;
		if (CI->getPos(ci_lat, ci_lon)) {
			std::vector<double> ci_pos;
			ci_pos.push_back(ci_lon); // LON = X (007,...E)
			ci_pos.push_back(ci_lat); // LAT = Y ( 53,...N)
			coordinates.push_back(ci_pos);
			anchors.push_back(nodeidx);
		} else {
			// gets randomized later (in NMDS)
			coordinates.push_back(std::vector<double>(2, 0.0));
		}
		// insert in node-LUT
		nameres[CI->hostnum] = nodeidx;
		nameres_rev[nodeidx] = CI->hostnum;
		nodeidx++;
	}
	unlock("(building graph)", false);
	
	// then shrink them to get the mean values only
	for (std::pair<std::string, ClientInfo *> m_s_ci: mapcopy) {
		m_s_ci.second->allMakeMeanOnly();

		std::vector<std::string> connectedMacs
			= m_s_ci.second->getConnectionNames();

		// add all "other-side-CIs" to node-LUT
		for (auto conn : connectedMacs) {
			int host = get_num_of_mac(conn) + 1;
			if (host < 1) {
				printf("UNBEKANNTES ITF GEFUNDEN: %s\nABBRUCH\n", conn.c_str());
				return;
			}
			// if not already in LUT, insert
			if (nameres.find(host) == nameres.end()){
				nameres[host] = nodeidx;
				nameres_rev[nodeidx] = host;
				nodeidx++;
			}
		}
	}

	// now we have <nodeidx> elements
	printf("build graph over %d nodes - %zu of them are anchors\n",
		nodeidx, anchors.size());
	int elements = nodeidx;
	int edges = 0;
	dissval = std::vector<double> ((elements*(elements-1))/2, 0.0);
	disscnt = std::vector<int> ((elements*(elements-1))/2, 0);

	for (std::pair<std::string, ClientInfo *> m_s_ci: mapcopy) {
		ClientInfo * CI = m_s_ci.second;
		int cihost = CI->hostnum;

		std::vector<std::string> connectedMacs
			= CI->getConnectionNames();

		// add all "other-side-CIs" to node-LUT
		for (auto conn : connectedMacs) {
			int host = get_num_of_mac(conn) + 1;
			Messreihe * MR = CI->getConnection(conn);
			if ((host < 1) || (MR == nullptr)) {
				printf("2: UNBEKANNTES ITF oder nullptrMR GEFUNDEN: %s\nABBRUCH\n", conn.c_str());
				return;
			}

			// LU idxs
			int i = nameres[cihost];
			int j = nameres[host];
			int idx;
			if (i < j) {
				idx = (i*elements + j) - (i+1)*(i+2)/2;
			} else if (j < i) {
				idx = (j*elements + i) - (j+1)*(j+2)/2;
			} else {
				printf("EINE CI HAT SICH SELBST GEMESSEN?? %d\nABBRUCH\n",i);
				return;
			}
			double meanrssi = MR->getMean(); // calced on makeMean
			int mul = disscnt[idx];
			dissval[idx] = ((mul * dissval[idx]) + meanrssi) / (mul + 1);
			mul ++;
			if (mul == 1) {
				edges ++;
			}
		}
	}

	// now serialize that to binary file!
	if (Options::useBINFileOut) {
		std::vector<uint8_t> streambuffer;
		size_t sz = 0;
		uint32_t nNodes = elements;
		uint32_t nAnchors = anchors.size();
		uint32_t nEdges = edges;
		printf("nNodes(%u), nAnchors(%u), nEdges(%u), ", nNodes, nAnchors, nEdges);
		sz += hton_vec_any(nNodes,	streambuffer);
		sz += hton_vec_any(nAnchors,streambuffer);
		sz += hton_vec_any(nEdges,	streambuffer);
		// node names
		for (int i = 0; i<elements; i++) {
			uint16_t hostnum = nameres_rev[i];
			sz += hton_vec_any(hostnum,	streambuffer);
		}
		// anchors
		for (size_t i = 0; i<anchors.size(); i++) {
			int idx = anchors[i];
			uint16_t hostnum = nameres_rev[idx];
			uint32_t nLng = coordinates[idx][0] * 100000.0;
			uint32_t nLat = coordinates[idx][1] * 100000.0;
			sz += hton_vec_any(hostnum,	streambuffer);
			sz += hton_vec_any(nLat,	streambuffer);
			sz += hton_vec_any(nLng,	streambuffer);
		}
		// edges
		for (int i = 0; i < elements; i++) {
			uint16_t I = nameres_rev[i];
			for (int j = i+1; j < elements; j++) {
				uint16_t J = nameres_rev[j];
				int idx = (i*elements + j) - (i+1)*(i+2)/2;
				if (disscnt[idx] > 0) {
					uint32_t nDiss = dissval[idx] * 100.0;
					sz += hton_vec_any(I,	streambuffer);
					sz += hton_vec_any(J,	streambuffer);
					sz += hton_vec_any(nDiss,streambuffer);				
				}
			}
		}
		printf("size = %zu, diff = %zd\n", sz, (sz-streambuffer.size()));

		char filename[250] = {0};
		sprintf(filename, "%s/out%ld.bin",
			Options::usePathForOutFile.c_str(), time(0));
		std::ofstream outfile(filename, std::ofstream::out | std::ofstream::binary);
		outfile.write( (const char *)&streambuffer[0], streambuffer.size());
		outfile.flush();
		outfile.close();
	}

	if (Options::useASCIIFileOut) {
		// write to txt file
		std::string streambuffer = "";
		uint32_t nNodes = elements;
		uint32_t nAnchors = anchors.size();
		uint32_t nEdges = edges;
		printf("nNodes(%u), nAnchors(%u), nEdges(%u), ", nNodes, nAnchors, nEdges);
		streambuffer += std::to_string(nNodes) + "\n";
		streambuffer += std::to_string(nAnchors) + "\n";
		streambuffer += std::to_string(nEdges) + "\n";
		// node names
		for (int i = 0; i<elements; i++) {
			uint16_t hostnum = nameres_rev[i];
			streambuffer += std::to_string(hostnum) + "\n";
		}
		// anchors
		for (size_t i = 0; i<anchors.size(); i++) {
			int idx = anchors[i];
			uint16_t hostnum = nameres_rev[idx];
			uint32_t nLng = coordinates[idx][0] * 100000.0;
			uint32_t nLat = coordinates[idx][1] * 100000.0;
			streambuffer += std::to_string(hostnum) +
						 ' ' +std::to_string(nLat)+ 
						 ' ' +std::to_string(nLng)+ "\n";
		}
		// edges
		for (int i = 0; i < elements; i++) {
			uint16_t I = nameres_rev[i];
			for (int j = i+1; j < elements; j++) {
				uint16_t J = nameres_rev[j];
				int idx = (i*elements + j) - (i+1)*(i+2)/2;
				if (disscnt[idx] > 0) {
					uint32_t nDiss = dissval[idx] * 100.0;
					streambuffer += std::to_string(I) + ' '
								 +std::to_string(J)+ ' '
								 +std::to_string(nDiss)+ "\n";
				}
			}
		}
		printf("size = %zu\n", streambuffer.size());

		char filename[250] = {0};
		sprintf(filename, "%s/out%ld.txt",
			Options::usePathForOutFile.c_str(), time(0));
		std::ofstream outfile(filename, std::ofstream::out | std::ofstream::binary);
		outfile.write(streambuffer.c_str(), streambuffer.size());
		outfile.flush();
		outfile.close();
	}*/
	
}


// label should be a number representing distance in meter (step=5)
void VerbindungsPool::
buildDistanceFiles (std::string prefixDir) {
	// LOCK TO HAVE CONSTANT DATA
	lock("(buildDistanceFiles)", false);
	for (std::pair<std::string, ClientInfo *> m_s_ci: mapping) {
		ClientInfo * CI = m_s_ci.second;
		std::vector<uint16_t> ciLevels = CI->getLevels(false);
		for (auto lvl : ciLevels)
			printf("worked: %u\n", CI->buildDistanceFiles(lvl, prefixDir));
	}
	unlock("(buildDistanceFiles)", false);
}


// save every CI in dir 'absDir'
void VerbindungsPool::
saveAll (std::string absDir) {

	// LOCK TO HAVE CONSTANT DATA
	lock("(saveAll)", false);
	for (std::pair<std::string, ClientInfo *> m_s_ci: mapping) {
		ClientInfo * CI = m_s_ci.second;
		std::vector<uint8_t> streambuffer;
		std::vector<uint16_t> levels = CI->getLevels(false);
		CI->serialize(streambuffer, false, 65000, levels);
		try {
			char filename[250] = {0};
			sprintf(filename, "%s/ci_%hhu_%s.bin",
				absDir.c_str(),
				CI->hostnum,
				CI->getName().c_str());

			std::ofstream outfile(filename, std::ofstream::out | std::ofstream::trunc);
			outfile.write((const char *) &streambuffer[0], streambuffer.size());
			outfile.flush();
			outfile.close();
		} catch (int e) {
			printf("EXCEPTION saveAll: %d\n", e);
			unlock("VP:saveAll ERROR end", false);
			return;
		}
		
	}
	unlock("(saveAll)", false);
}


void VerbindungsPool::
print (bool onlySignificant, uint16_t maxPerStation) {
	lock("(printing)", true); //lock mutex
	if (pmyself == nullptr) {
		printf("\033[1;36mVerbindungsPool von <unbekannt> hat %zu Eintraege\033[0m\n", mapping.size());
	} else {
		printf("\033[1;36mVerbindungsPool von %s\033[0m\n", pmyself->getName().c_str());
		pmyself->print(onlySignificant, maxPerStation);
	}
	for (std::pair<std::string, ClientInfo *> x: mapping){
		if (x.second != pmyself)
			x.second->print(onlySignificant, maxPerStation);
	}
	unlock("(printing)", true); // release mutex
}


// ------------------ (private member)
// 

// mapping between
// station name and values
std::unordered_map<std::string, ClientInfo *> VerbindungsPool::mapping;

// mutex for exclusive
// access - internal use only
std::mutex VerbindungsPool::mtx;

// ptr to own data
ClientInfo * VerbindungsPool::pmyself;

void VerbindungsPool::
lock (const char * reason, bool silent) {
	if (!silent)
		printf("\033[0;31mLock Pool\033[0m %s\n", reason );
	VerbindungsPool::mtx.lock();
}

void VerbindungsPool::
unlock (const char * reason, bool silent) {
	VerbindungsPool::mtx.unlock();
	if (!silent)
		printf("\033[0;32mUnlock Pool\033[0m %s\n", reason );
}
