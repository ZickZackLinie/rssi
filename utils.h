#ifndef _UTILS_H
#define _UTILS_H

#include <algorithm> // sorting vector for intersection
#include <fstream> // reading macs file
#include <vector> // mac addresses from file

#include <stdint.h> // SHOULD be included by inttypes
#include <inttypes.h> // for uint64_t and so on

#define DO_QUOTE(X)  #X
#define QUOTE(X)     DO_QUOTE(X)
#ifndef FILE_MACS
#define FILE_MACS    /etc/rssipos/rssi.macs
#endif
#ifndef FILE_LOGSND
#define FILE_LOGSND  /etc/rssipos/rssisend.log
#endif

#ifndef DIR_QUICKSAVE
#define DIR_QUICKSAVE  /var/www/html/map/web
#endif

// ports for udp(-bc) and tcp
#define PT_UDP_BC	0xA000 // 40960 udp broadcast
#define PT_UDP_TK	0xA001 // 40961 udp talking

// array sizes for mac addresses
#define ETH_ALEN 6
#define ETH_CLEN 18 // xx:xx:xx:xx:xx:xx

/*// possible broadcast messages
#define MSG_QUIT		"QUITSERVER"
#define MSG_NTP			"NTPRESTART"
#define MSG_DOSTUFF		"DOTHESTUFF"
#define MSG_SAYHI		"ANSWERTOME"
#define MSG_SENDMEAN	"SNDMEANVAL"*/

// CI's anchor states:
#define CI_AN_YES		2
#define CI_AN_UNKNOWN	1
#define CI_AN_NO		0

// time to...
#define TM_MAX_INACTIVE 1   // make station dump inactive (rssi->0)
#ifndef TM_POLL 
#define TM_POLL         500 // poll next sta dump rssi|<-TM_POLL->|rssi
#endif


// for Messreihe (how many datasets (un)til...)
#ifndef MAX_DATA_KEEP
#define MAX_DATA_KEEP      40 // how many data to keep in Messreihe
#endif
#ifndef NUM_TO_SIGNIFICANT 
#define NUM_TO_SIGNIFICANT 20 // how many rssi vals per sta min needed
#endif

// sending information
#define SND_MAX_NODES	0xFF // max # of sendable CIs

// stuff for batman-adv startup
#define BAT_CHANNEL 1
#define BAT_MTU     1532
#ifndef BAT_POLL 
#define BAT_POLL    500 // update rate in ms (shall match TM_POLL)
#endif


typedef uint32_t mtime_t;

// compare two byte-arrays of ETH_ALEN size
// 0 iff equal
// otherwise -1, 1
int cmp_itf (	unsigned char * left,
				unsigned char * right);

int mac_addr_n2a (	char *			mac_addr,
					unsigned char *	arg );

// mac address string to six bytes array
// out mac_addr
// in arg (string xx:xx:xx:xx:xx:xx)
// return 0 iff all okay
int mac_addr_a2n (	unsigned char *	mac_addr,
					char *			arg );

// read file with mac addresses (1 per line)
// fills vector
int read_mac_file(std::vector<std::string> &macs);

// traverse all mac addresses known
// return index of first match
// or -1 if not found
int get_num_of_mac(std::string mac);
int get_num_of_mac(unsigned char * mac);

struct in_addr hostToIP(uint8_t host);


// create directory 'absDir'
int doCreateDir(std::string absDir);


// calculating string for duration from seconds
std::string calcStrDuration(time_t seconds);

// letters to lowercase
std::string toLower(std::string in);

// from stackoverflow.com/question/809902
// conversion for size independant numbers to
// and from little-endian streams
// data[] = "1000 0000 | 1111 1111" (little endian)
// short  =    8    0      F    F
// 
// no matter what internal endianness is
template <typename T>
static inline T
hton_any(const T &input) {
	T output(0);
	const std::size_t size = sizeof(input);
	uint8_t *data = reinterpret_cast<uint8_t *>(&output);

	for (std::size_t i = 0; i < size; i++){
		data[i] = input >> ((size -i -1) * 8);
	}

	return output;
}

// reverse is same thing
#define ntoh_any(x) hton_any(x)


// write input into dest (hton) if space is big enough
// changes space
template <typename T>
static inline const std::size_t
hton_a_any(const T &input, uint8_t * dest, std::size_t &space) {
	//T output(0);
	const std::size_t size = sizeof(input);
	
	if (size > space)
		return 0;

	//uint8_t *data = reinterpret_cast<uint8_t *>(&output);

	for (std::size_t i = 0; i < size; i++){
		dest[i] = input >> ((size -i -1) * 8);
	}
	//memcpy(dest,data,size);
	space = space - size;
	return size;
}

// write input into vecdest (hton)
template <typename T>
static inline const std::size_t
hton_vec_any(const T &input, std::vector<uint8_t> &vecDest) {
	//T output(0);
	const std::size_t size = sizeof(input);
	
	for (std::size_t i = 0; i < size; i++){
		vecDest.push_back(input >> ((size -i -1) * 8));
	}
	return size;
}
template <typename T>
static inline const std::size_t
ntoh_vec_any(T &output, std::vector<uint8_t> &vecSrc, std::size_t ptr) {
	output = T(0);
	const std::size_t size = sizeof(output);
	
	for (std::size_t i = 0; i < size; i++){
		output += (vecSrc[ptr + i] << (8 * (size -i -1)));
	}
	return size;
}


// build intersection of two vectors
template <typename T>
static inline std::vector<T>
intersection(std::vector<T> v1, std::vector<T> v2) {
	std::vector<T> v3;
	sort(v1.begin(), v1.end());
	sort(v2.begin(), v2.end());
	set_intersection(
			v1.begin(), v1.end(),
			v2.begin(), v2.end(),
			back_inserter(v3) );
	return v3;
}
	


class Options {
private:
	
public:
	static float latitude;
	static float longitude;
	static std::string smac_address;
	static int hostnum;
	static uint8_t anchorState;
	static bool useWindow;
	static bool handleNTP;
	static bool handleBatAdv;
	static bool listenAllIPs;
	static bool useASCIIFileOut;
	static bool useBINFileOut;
	static std::string curPrefix;
	static std::string usePathForOutFile;
	static std::vector<std::string> MACS;
	static bool parseOptions (int argc, char *argv[]);
};


#endif