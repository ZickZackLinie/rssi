<!DOCTYPE html>
<?php
	////////////////////////
	$subdir = (string)$_GET['subdir'];
	if (!$subdir || $subdir == "") $subdir = ".";
	$host_A = intval($_GET['hostA']);
	if (!$host_A) $host_A = 1;
	$host_B = intval($_GET['hostB']);
	if (!$host_B) $host_B = 6;
	$x = intval($_GET['X']);
	if (!$x) $x = 10;
	$limit = intval($_GET['limit']);
	if (!$limit) $limit = 50;
	$sndrow = intval($_GET['sndrow']);
	if (!isset($sndrow)) $sndrow = 1;
	$refresh = intval($_GET['refresh']);
	if (!$refresh) $refresh = 0;
	////////////////////////
?>
<html>
	<head>
		<meta name="viewport" content="initial-scale=1.0, width=device-width"/>
		<meta charset="utf-8"/>
		<?php if($refresh>0) echo "<meta http-equiv=\"refresh\" content=\"$refresh\"/>";?>
		<title>Distances</title>
		<style>
		html, body {
			height: 100%;
			margin: 0;
			padding: 0;
		}
		#map {
			height: 100%;
		}
		.chartBox {
			min-width: 320px;
			max-width: 1800px;
			height: 520px;
			margin: 0 auto;
		}
		.chartDetail {
			min-width: 320px;
			max-width: 1800px;
			height: 520px;
			margin: 0 auto;
		}
		.chartDiff {
			min-width: 320px;
			max-width: 1800px;
			height: 320px;
			margin: 0 auto;
		}
		.chartTime {
			min-width: 320px;
			max-width: 1800px;
			height: 520px;
			margin: 0 auto;
		}
		</style>
	</head>
	<body>
		<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
		<script type="text/javascript" src="https://code.highcharts.com/highcharts.js"></script>
		<script type="text/javascript" src="https://code.highcharts.com/highcharts-more.js"></script>
		<script type="text/javascript" src="http://code.highcharts.com/modules/exporting.js"></script>
		<script type="text/javascript" src="makeChart.js"></script>
		<!--script type="text/javascript">doStuff('jsonp.php?filename=stuff.json&callback=?');</script-->
		<script type="text/javascript">doStuff('createJSON.php?subdir=<?php echo $subdir;?>&hostA=<?php echo $host_A;?>&hostB=<?php echo $host_B;?>&X=<?php echo $x;?>&limit=<?php echo $limit;?>&sndrow=<?php echo $sndrow;?>&callback=?','<?php echo $host_A;?>','<?php echo $host_B;?>',<?php echo $limit;?> );</script>
		<div id="container"></div>
		
	</body>
</html>
