<?php
$config = array();

function readOutTxtFile($filename, $ignoreNonAnchors) {

	$handle = @fopen($filename, "r");
	if ($handle) {
		$retNds = array();
		$retEgs = array();
		$line = trim(fgets($handle));
		$nNodes = intval($line);
		$line = trim(fgets($handle));
		$nAnchors = intval($line);
		$line = trim(fgets($handle));
		$nEdges = intval($line);
		//echo "header: $nNodes, $nAnchors, $nEdges\n";
			
		$cnt = 0; // node names
		while (($cnt < $nNodes) && (($line = fgets($handle)) != false)) {
			$nodenum = trim($line);
			if (!$ignoreNonAnchors) {
				$retNds[$nodenum] = array(false,0,0, $nodenum);
			}
			//echo "node: '$nodenum'\n";
			$cnt = $cnt+1;
		}
		//var_dump($retNds);
		$cnt = 0; // anchor nodes
		while (($cnt < $nAnchors) && (($line = fgets($handle)) != false)) {
			$sline = explode(" ", $line);
			$nodenum = $sline[0];
			$lat = floatval($sline[1]);
			$lon = floatval($sline[2]);
			//echo "anchors: $nodenum, $lat, $lon\n";
			if ($ignoreNonAnchors) {
				$retNds[$nodenum] = array(false,0,0, $nodenum);
			}
			$retNds[$nodenum][0] = true;
			$retNds[$nodenum][1] = $lat;
			$retNds[$nodenum][2] = $lon;
			$cnt = $cnt+1;
		}
		//var_dump($retNds);
		$cnt = 0; // edges
		while (($cnt < $nEdges) && (($line = fgets($handle)) != false)) {
			$sline = explode(" ", $line);
			$nodenumA = $sline[0];
			$nodenumB = $sline[1];
			$rssi = floatval($sline[2]);
			//echo "$nodenumA, $nodenumB, $rssi\n";
			if ($ignoreNonAnchors) {
				if ((!$retNds[$nodenumA][0]) ||
					(!$retNds[$nodenumB][0]))
					continue;
			}
			$lat1 = $retNds[$nodenumA][1];
			$lng1 = $retNds[$nodenumA][2];
			$lat2 = $retNds[$nodenumB][1];
			$lng2 = $retNds[$nodenumB][2];
			$lat_mid = ($lat1 + $lat2)/2;
			$lng_mid = ($lng1 + $lng2)/2;
			$retEgs[] = array($nodenumA, $nodenumB, $rssi, $lat_mid, $lng_mid);
			$cnt = $cnt+1;
		}

		if (!feof($handle)) {
			fclose($handle);
			return false;
		}
		fclose($handle);
		return array(
			"nNodes" => $nNodes,
			"nAnchors" => $nAnchors,
			"nEdges" => $nEdges,
			"nodes" => $retNds,
			"edges" => $retEgs
			);
	}
	return false;
}

function strMidpointMap($nodeArray){
	$lat_sum = 0;
	$lng_sum = 0;
	$cnt = 0;
	foreach ($nodeArray["nodes"] as $nodenum => $con) {
		if($con[0]){
			$lat_sum += $con[1];
			$lng_sum += $con[2];
			$cnt++;
		}
	}
	$lat_mid = $lat_sum / $cnt;
	$lng_mid = $lng_sum / $cnt;
	return "{lat: $lat_mid, lng: $lng_mid}";
}

function strLineLab($edge, $nodeA, $nodeB){
	//markernodeopts${node[3]}
	$poly = "//generate label for edge ${edge[0]}--${edge[1]}\n" .
			"var lineedge${edge[0]}${edge[1]} = " .
			"new google.maps.Polyline({\n".
			"    path:[markernodeopts${nodeA[3]}.position," .
					  "markernodeopts${nodeB[3]}.position],\n" .
			"    strokeColor:\"#5050FF\",\n" .
			"    strokeOpacity:0.5,\n" .
			"    strokeWeight:2,\n" .
			"    map:map });\n";

	return 	$poly.
			"var markeredge${edge[0]}${edge[1]}" .
				" = new google.maps.Marker({\n" .
			"    position: {lat: ${edge[3]}, lng: ${edge[4]} },\n" .
			"    map: map,\n" .
			"    visible: false\n});" .
			"\nvar labeledge${edge[0]}${edge[1]} = new Label();\n" .
			"labeledge${edge[0]}${edge[1]}.bindTo('position', " .
				"markeredge${edge[0]}${edge[1]}, 'position');\n" .
			"labeledge${edge[0]}${edge[1]}.set('text', " .
				"'-${edge[2]}dBm');\n" .
			"labeledge${edge[0]}${edge[1]}.setMap(map);\n";
}

function strNodeLab($node){
	return 	"\n//generate label for node ${node[3]}\n" .
			"var markernodeopts${node[3]} = {\n" .
			"    position: {lat: ${node[1]}, lng: ${node[2]} },\n" .
			//"    position: new google.maps.LatLng(${node[1]}, ${node[2]}),\n" .
			"    map: map,\n" .
			"    visible: false\n};" .
			"var markernode${node[3]} = new google.maps.Marker(" .
				"markernodeopts${node[3]} );\n" .
			"var labelnode${node[3]} = new Label();\n" .
			"labelnode${node[3]}.bindTo('position', " .
				"markernode${node[3]}, 'position');\n" .
			"labelnode${node[3]}.set('text', " .
				"'${node[3]}');\n" .
			"labelnode${node[3]}.setMap(map);\n";
}

function strStaMap($node){
	return "\nnode${node[3]}: {\n" .
			"    center: {lat: ${node[1]}, lng: ${node[2]} },\n" .
			"    radius: 2\n}";
}

function createString($filename){
	global $config;
	if (($config = readOutTxtFile($filename, true)) == false)
		return "";
	$nodes = $config["nodes"];
	//var_dump($nodes);
	$nodelabels = "\n";
	$stastr = "var stamap = {";
	$stas = array();
	foreach ($nodes as $nd => $ndConf) {
		$stas[] = strStaMap($ndConf);
		$nodelabels .= strNodeLab($ndConf);
	}

	// Egs[] = array($nodeA, $nodeB, $rssi, $lat_mid, $lng_mid);
	$edges = $config["edges"];
	$polylinestr = "\n";
	foreach ($edges as $edge) {
		$nodeA = $nodes[$edge[0]];
		$nodeB = $nodes[$edge[1]];
		$polylinestr .= strLineLab($edge,$nodeA,$nodeB);
	}

	$all = $nodelabels ."\n". $polylinestr."\n". $stastr . implode(",", $stas) . "};\n\n";
	return $all;
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no"/>
		<meta charset="utf-8"/>
		<title>Circles</title>
		<style>
		html, body {
			height: 100%;
			margin: 0;
			padding: 0;
		}
		#map {
			height: 100%;
		}
		</style>
	</head>
	<body>
		<div id="map"></div>
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCrOvrqBfa4jLiCDq2EkHIT30YRafYqj7k&signed_in=true"></script>
		<script type="text/javascript" src="label.js"></script>
		<script type="text/javascript">

function initMap(){

	<?php $stuff = createString("../examples/out1.txt");?>

	var map = new google.maps.Map(document.getElementById('map'),{
			zoom: 18,
			scaleControl: true,
			center: <?php echo strMidpointMap($config); ?>,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		});
	
	<?php echo $stuff;?>

	for (var sta in stamap) {
		var rssiCircle = new google.maps.Circle({
			strokeColor: '#FF0000',
			strokeOpacity: 0.8,
			strokeWeight: 2,
			fillColor: '#FF0000',
			fillOpacity: 0.35,
			map: map,
			center: stamap[sta].center,
			radius: stamap[sta].radius
		});
	}
}
initMap();
		</script>
	</body>
</html>