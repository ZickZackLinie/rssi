
function doStuff (dataurl, hostA, hostB, limit) {
	$.getJSON( dataurl, function (chartrow) {

		var cIdx1= 0;
		var cIdx2= 3;
		var cIdxdiff= 4;

		//alert(chartrow);
		// add x values
		//dataset.data = Highcharts.map(dataset.data, function (val, j) {
		//	return [chartrow.xData[j], val];
		//});
		
		$('<div class="chartBox">')
		.appendTo('#container')
		.highcharts({
			chart: {
				zoomType: 'xy',
				marginLeft: 40, // Keep all charts left aligned
				spacingTop: 20,
				spacingBottom: 20
			},
			title: {
				text: "RSSI[dBm]/Entfernung[m] (Mittelwert)",
				align: 'left',
				margin: 0,
				x: 30
			},
			credits: {
				enabled: false
			},
			legend: {
				enabled: true
			},
			xAxis: {
				crosshair: true,
				//labels: {
				//	format: '{value} m'
				//}
				categories: chartrow.data.xaxis
			},
			yAxis: {
				reversed: true,
				max: 0,
				title: {
					text: null
				}
			},
			tooltip: {
				positioner: function() {
					return {
						x: this.chart.chartWidth-this.label.width,
						y: -1
					};
				},
				borderWidth: 0,
				backgroundColor: 'none',
				pointFormat: '{point.y}',
				headerFormat: '',
				shadow: false,
				shared: true,
				style: {
					fontSize: '18px'
				},
				valueDecimals: 1
			},
			series: [{
				data: chartrow.data.cand1,
				name: "Host "+hostA,
				type: "boxplot",
				color: Highcharts.getOptions().colors[cIdx1],
				fillOpacity: 0.5,
				enableMouseTracking: false,
				tooltip: {
					valueSuffix: ' dBm'
				}
			},
			{
				data: chartrow.data.cand2,
				name: "Host "+hostB,
				type: "boxplot",
				color: Highcharts.getOptions().colors[cIdx2],
				fillOpacity: 0.5,
				enableMouseTracking: false,
				tooltip: {
					valueSuffix: ' dBm'
				}
			},{
				data: chartrow.data.spln1,
				name: "Host "+hostA+" avg.",
				type: "spline",
				color: Highcharts.getOptions().colors[cIdx1],
				fillOpacity: 0.8,
				tooltip: {
					valueSuffix: ' dBm) ',
					valuePrefix: "("+hostA+": "
				}
			},{
				data: chartrow.data.spln2,
				name: "Host "+hostB+" avg.",
				type: "spline",
				color: Highcharts.getOptions().colors[cIdx2],
				fillOpacity: 0.8,
				tooltip: {
					valueSuffix: ' dBm) ',
					valuePrefix: "("+hostB+": "
				}
			}]
		});

		$('<div class="chartDiff">')
		.appendTo('#container')
		.highcharts({
			chart: {
				zoomType: 'xy',
				marginLeft: 40, // Keep all charts left aligned
				spacingTop: 20,
				spacingBottom: 20
			},
			title: {
				text: "Diff. Avg.RSSI[dBm]/Entfernung[m]",
				align: 'left',
				margin: 0,
				x: 30
			},
			credits: {
				enabled: true
			},
			legend: {
				enabled: true
			},
			xAxis: {
				title: { text: null},
				categories: chartrow.data.xaxis,
				crosshair: true,
				tickInterval: 1,
				//min: 1,
				//max: limit+3 // to print the label nicely
			},
			yAxis: {
				tickInterval: 1,
				/*crosshair: {
					width: 1,
					color: 'green'
				},*/
				plotLines: [{
					value: chartrow.data.diffs.mean,
					color: 'red',
					width: 1,
					label: {
						text: "avg. diff. of avg. RSSI: "+chartrow.data.diffs.mean.toFixed(2)+" dbm",
						align: 'center',
						style: {
							color: 'gray'
						}
					}
				}],
				min: chartrow.data.diffs.mean -3,
				max: chartrow.data.diffs.mean +3,
				title: {
					text: null
				}
			},
			tooltip: {
				positioner: function() {
					return {
						x: this.chart.chartWidth-this.label.width,
						y: -1
					};
				},
				borderWidth: 0,
				backgroundColor: 'none',
				pointFormat: '{point.y}',
				headerFormat: '',
				shadow: false,
				shared: true,
				style: {
					fontSize: '18px'
				},
				valueDecimals: 1
			},
			series: [{
				data: chartrow.data.diffs.data,
				name: "Diff. avg. RSSI",
				type: "spline",
				color: Highcharts.getOptions().colors[cIdxdiff],
				fillOpacity: 0.3,
				tooltip: {
					headerFormat: '<b>{series.name}</b><br/>',
					pointFormat: '{point.y}dBm'
				}
			}]
		});

		$('<div class="chartDetail">')
		.appendTo('#container')
		.highcharts({
			chart: {
				zoomType: 'xy',
				marginLeft: 40, // Keep all charts left aligned
				spacingTop: 20,
				spacingBottom: 20
			},
			title: {
				text: "RSSI[dBm]/Entfernung[m] (Detail)",
				align: 'left',
				margin: 0,
				x: 30
			},
			credits: {
				enabled: true
			},
			legend: {
				enabled: true
			},
			xAxis: {
				title: { text: "Dist/m"},
				//categories: chartrow.data[0],
				tickInterval: 5,
				min: 1,
				max: limit+3 // to print the label nicely
			},
			yAxis: {
				reversed: true,
				max: 0,
				title: {
					text: "RSSI/dBm"
				}
			},
			tooltip: {
				positioner: function() {
					return {
						x: this.chart.chartWidth-this.label.width,
						y: -1
					};
				},
				borderWidth: 0,
				backgroundColor: 'none',
				pointFormat: '{point.y}',
				headerFormat: '',
				shadow: false,
				shared: true,
				style: {
					fontSize: '18px'
				},
				valueDecimals: 1
			},
			series: [{
				data: chartrow.data.sctt1,
				name: "Host "+hostA+" data",
				type: "scatter",
				color: Highcharts.getOptions().colors[cIdx1],
				fillOpacity: 0.3,
				tooltip: {
					headerFormat: '<b>{series.name}</b><br/>',
					pointFormat: '{point.x}m, {point.y}dBm'
				}
			},{
				data: chartrow.data.sctt2,
				name: "Host "+hostB+" data",
				type: "scatter",
				color: Highcharts.getOptions().colors[cIdx2],
				fillOpacity: 0.3,
				tooltip: {
					headerFormat: '<b>{series.name}</b><br/>',
					pointFormat: '{point.x}m, {point.y}dBm'
				}
			}]
		});

		
		$('<div class="chartTime">')
		.appendTo('#container')
		.highcharts({
			chart: {
				zoomType: 'xy',
				type: 'spline',
				marginLeft: 40, // Keep all charts left aligned
				spacingTop: 20,
				spacingBottom: 20
			},
			title: {
				text: "RSSI[dBm]/Zeit[s] (Detail)",
				align: 'left',
				margin: 0,
				x: 30
			},
			credits: {
				enabled: true
			},
			legend: {
				enabled: true
			},
			xAxis: {
				type: 'datetime',
				crosshair: true,
				dateTimeLabelFormats: {
					millisecond: '%H:%M:%S',
					second: '%H:%M:%S',
					month: '%e. %b',
					year: '%b'
				},
				title: { text: "Dist/m"}
			},
			yAxis: {
				reversed: true,
				max: 0,
				title: {
					text: "RSSI/dBm"
				}
			},
			tooltip: {
				positioner: function() {
					return {
						x: this.chart.chartWidth-this.label.width,
						y: -1
					};
				},
				borderWidth: 0,
				backgroundColor: 'none',
				headerFormat: '',
				shadow: false,
				shared: true,
				style: {
					fontSize: '18px'
				},
				valueDecimals: 1
			},
			series: [{
				data: chartrow.data.time1,
				name: "Host "+hostA+" data",
				color: Highcharts.getOptions().colors[cIdx1],
				fillOpacity: 0.3,
				tooltip: {
					headerFormat: '<b>{series.name}</b><br/>',
					pointFormat: '('+hostA+') {point.x: %H:%M:%S} {point.y}dBm '
				}
			},{
				data: chartrow.data.time2,
				name: "Host "+hostB+" data",
				color: Highcharts.getOptions().colors[cIdx2],
				fillOpacity: 0.3,
				tooltip: {
					headerFormat: '<b>{series.name}</b><br/>',
					pointFormat: '('+hostB+') {point.x: %H:%M:%S} {point.y}dBm '
				}
			}]
		});

	});

}