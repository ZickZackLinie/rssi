<?php

$callback = (string)$_GET['callback'];
if (!$callback) $callback = 'callback';
$host_A = intval($_GET['hostA']);
if (!$host_A) $host_A = 1;
$host_B = intval($_GET['hostB']);
if (!$host_B) $host_B = 6;
$subdir = $_GET['subdir'];
if (!$subdir || $subdir == "") $subdir = ".";
$x = intval($_GET['X']);
if (!$x) $x = 10;
$limit = intval($_GET['limit']);
if (!$limit) $limit = 100;
$sndrow = intval($_GET['sndrow']);
if (!isset($sndrow)) $sndrow = 1;

function doStep($hostA, $hostB, $step, $X) {

//file_get_contents(filename);
	global $subdir;
	$filename = join(DIRECTORY_SEPARATOR, array($subdir,"m${hostA}_${hostB}_${step}m.txt"));
	if (!file_exists($filename)) {
		return $filename;
	}
	$content = file_get_contents($filename);
	$stvalues = explode(',',$content);
	$values = array();
	$tvalues = array();
	foreach ($stvalues as $i => $value) {
		if ($i % 2 != 0)
			$values[] = floatval($value);
		else {
			$tvalues[] = array(
				1000 * intval($value),    // time
				floatval($stvalues[$i+1]) // rssi
			);
		}
	}

	$values_save = $values;
	sort($values);
	$ret = array();
	$ret["values"] = $values_save;
	$ret["tvalues"] = $tvalues;
	$sz = count($values);
	if($sz < 5)
		return false;
	// calc mean without upper and lower X percent of vals
	$cut = ($sz/100) * $X;
	$cnt = $sz - 2*($cut);
	$sum = 0;
	for ($i=0; $i < $cnt; $i++) { 
		$sum += $values[$i+$cut];
	}
	$ret["amean"] = $sum / $cnt; // arithm. mean value of (^)
	$ret["gmean"] = $values[$sz/2]; // geometric mean
	$ret["min"] = $values[$cut];
	$ret["max"] = $values[$sz-$cut-1];
	$ret["top"] = $values[$sz-1];
	$ret["bot"] = $values[0];
	return $ret;
}

$all = array();
$all["xaxis"] = array(); // x-axis            0
$all["cand1"] = array(); // candlestick 1     1
$all["cand2"] = array(); // candlestick 2     2
$all["spln1"] = array(); // mean 1 spline     3
$all["spln2"] = array(); // mean 2 spline     4
$all["sctt1"] = array(); // scatter 1         5
$all["sctt2"] = array(); // scatter 2         6
$all["diffs"] = array(
	"mean" => null,
	"data" => array());  // diffs betw. means 7
$all["time1"] = array(); // time-to-rssi 1    8
$all["time2"] = array(); // time-to-rssi 2    9


$steps = array();//,15,20,25,30,35,40,45,50,55,60,65,70,75,80,85,90,95,100);
for ($i=5; $i <= $limit ; $i+=5) { 
	$steps[] = $i;
	if ($sndrow)
		$steps[] = $i+1;
}


foreach ($steps as $step) {
	$all["xaxis"][] = $step; // add to timeline
	$dataset = doStep($host_A, $host_B, $step, $x);
	if (is_array($dataset)) {
		// candlestick 1
		$all["cand1"][] = array(
			$dataset["bot"],
			$dataset["min"],
			$dataset["gmean"],
			$dataset["max"],
			$dataset["top"]
			);
		// mean 1 spline
		$all["spln1"][] = $dataset["amean"];
		// scatter 1
		foreach ($dataset["values"] as $scatterval) {
			$all["sctt1"][] = array($step,$scatterval);
		}
		// time 1
		$all["time1"] = array_merge($all["time1"],$dataset["tvalues"]);
	} else {
		$all["error1"] = $dataset;
		// candlestick 1
		$all["cand1"][] = array(null, null, null, null, null);
		// mean 1 spline
		$all["spln1"][] = null;
		// scatter 1
		// leave empty
	}
	$dataset = doStep($host_B, $host_A, $step, $x);
	if (is_array($dataset)) {
		// candlestick 2
		$all["cand2"][] = array(
			$dataset["bot"],
			$dataset["min"],
			$dataset["gmean"],
			$dataset["max"],
			$dataset["top"]
			);
		// mean 2 spline
		$all["spln2"][] = $dataset["amean"];
		// scatter 2
		foreach ($dataset["values"] as $scatterval) {
			$all["sctt2"][] = array($step+0.5,$scatterval);
		}
		// time 2
		$all["time2"] = array_merge($all["time2"],$dataset["tvalues"]);
	} else {
		$all["error2"] = $dataset;
		// candlestick 2
		$all["cand2"][] = array(null, null, null, null, null);
		// mean 2 spline
		$all["spln2"][] = null;
		// scatter 2
		// leave empty
	}
}

$dif_sum = 0;
$dif_cnt = 0;
foreach ($steps as $i => $step) {
	$avgA = $all["spln1"][$i]; // amean
	$avgB = $all["spln2"][$i]; // amean
	if (($avgA !== null) && ($avgB !== null)) {
		// absolute difference
		$dif = abs($avgB-$avgA);
		$dif_sum += $dif;
		$dif_cnt ++;
		$all["diffs"]["data"][] = $dif;
	} else {
		$all["diffs"]["data"][] = null;
	}
}
$dif_avg = 0;
if ($dif_cnt > 0)
	$dif_avg = $dif_sum / $dif_cnt;
$all["diffs"]["mean"] = $dif_avg;

// write out to data
//var_dump($all);
//$out = array("data" => $all);
$text = json_encode(array("data" => $all),JSON_PRETTY_PRINT);
//var_dump($text);
// Send the output
header('Content-Type: text/javascript');
echo "$callback($text);";
?>
