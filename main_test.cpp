

// Main software:
// Shoud be independent of being a server or a client
#include <unistd.h> // time_t and time(int)
#include <thread>
#include <fstream>
#include <iterator>

#include <utils.h>
#include <netlinkconnector.h>
#include <verbindungspool.h>
#include <control.h>
#include <udpbuffer.h>
#include <packet.h>


std::vector<uint8_t> readFile(const char* filename)
{
    // open the file:
    std::ifstream file(filename, std::ios::binary);

    // Stop eating new lines in binary mode!!!
    file.unsetf(std::ios::skipws);

    // get its size:
    std::streampos fileSize;

    file.seekg(0, std::ios::end);
    fileSize = file.tellg();
    file.seekg(0, std::ios::beg);

    // reserve capacity
    std::vector<uint8_t> vec;
    vec.reserve(fileSize);

    // read the data:
    vec.insert(vec.begin(),
               std::istream_iterator<uint8_t>(file),
               std::istream_iterator<uint8_t>());

    return vec;
}

void rewriteCI (ClientInfo * ci, const char* filename) {
    std::vector<uint8_t> streambuffer;
    std::vector<uint16_t> levels = ci->getLevels(false);
    ci->serialize(streambuffer, false, 65000, levels);
    try {
        std::ofstream outfile(filename, std::ofstream::out | std::ofstream::trunc);
        outfile.write((const char *) &streambuffer[0], streambuffer.size());
        outfile.flush();
        outfile.close();
    } catch (int e) {
        printf("EXCEPTION rewrite %s: %d\n", filename, e);
    }
}



int main(int argc, char *argv[]) {

    read_mac_file(Options::MACS);
    
    Options::curPrefix = "remastered";//"test"; // name of subdir
    Options::usePathForOutFile = "/var/www/html/map/web";//"../examples"; // path: VP:buildGraph


	// parse options
	if (argc > 2) {
        int action = atoi(argv[1]);
        
    	std::vector<uint8_t> streambuffer = readFile(argv[2]);
    	ClientInfo * ci = new ClientInfo("00:00:00:00:00:00",0,0,CI_AN_UNKNOWN,0,0);
    	ci->deserialize(streambuffer);
    	//ci->print(false, 3);
    	VerbindungsPool::createMyInfo(ci);
    	if (action == 0) {
            for (size_t i = 0; i < Options::MACS.size(); ++i)
            {
                printf("(%zd) %s\n", i+1, toLower(Options::MACS[i]).c_str());
            }

            printf("\n%s:\n", ci->getName().c_str());
            // first line of table
            printf("     \t");
            for (size_t i = 0; i < Options::MACS.size(); ++i)
            {
                printf("(%zd)\t", i+1);
            }
            printf("\n");
            
            std::vector<uint16_t> vecLevels = ci->getLevels(false); // all
            for (auto level : vecLevels) {
                /*std::vector<std::string> vecMacs = ci->getConnectionNames(level);
                for(auto m : vecMacs) {
                    printf("%s ", m.c_str());
                }
                printf("\n");*/
                
                // second and following
                printf("[%03hu]\t", level);
                for (size_t i = 0; i<Options::MACS.size(); i++){
                    Messreihe * tmp = ci->getConnection(level, 
                        toLower(Options::MACS[i]).c_str());
                    //Messreihe * tmp = ci->getConnection(level, "00:08:ca:66:0c:d2");
                    if (tmp == nullptr) {
                        printf("---\t");
                    } else {
                        printf("%03d\t", tmp->count());
                    }

                }
                printf("\n");
            }
        } else if (action == 1) {
            int startarg = 3;
            std::vector<uint16_t> vecEraseLevels;
            while (startarg < argc) {
                uint16_t _lvl = atoi(argv[startarg]);
                startarg ++;
                vecEraseLevels.push_back(_lvl);
            }
            for (auto _lvl : vecEraseLevels)
                printf("loesche level %hu\n", _lvl);
            ci->eraseLevels(vecEraseLevels);
            std::string fname = argv[2];
            fname += ".rem";
            rewriteCI(ci, fname.c_str());
        } else if (action == 2) {
            std::string mmac = argv[3];
            printf("loesche %s\n", mmac.c_str());
            ci->eraseConnections(mmac);
            std::string fname = argv[2];
            fname += ".rem";
            rewriteCI(ci, fname.c_str());
        } else if (action == 3) {
            int startarg = 3;
            std::vector<int> vecHostNums;
            while (startarg < argc) {
                int _host = atoi(argv[startarg]);
                startarg ++;
                vecHostNums.push_back(_host);
            }
            doCreateDir(Options::usePathForOutFile + "/" + Options::curPrefix);
            VerbindungsPool::buildDistanceFiles (Options::curPrefix);
            for (auto _host : vecHostNums)
                VerbindungsPool::get("")->buildHistFile(_host, Options::curPrefix);
            
        }


    } else {
        printf("usage: '%s <action> /path/to/ci.bin'\n", argv[0]);
        printf("Action 0 - anzeigen level:ci:anzahl\n");
        printf("Action 1 - loesche folgende levels\n");
        printf("Action 2 - loesche folgende Mac Adresse\n");
        printf("Action 3 - buildHistFile zu folgenden hosts\n");
    }

	// [end]
	return 0;

}
