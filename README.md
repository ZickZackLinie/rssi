

## Inhalt

Das rssi-Projekt enhält ein Programm zum Sammeln und versenden von RSSI-Werten.
Die zugehörige Masterarbeit ist im Projekt [rssidoc](https://gitlab.com/ZickZackLinie/rssidoc) zu erreichen.

Die Teilprogramme bestehen aus:
* rssipos (Sammeln, Verwalten und Versenden von RSSI-Werten)
* rssisend (Auslösen von synchronen Aktionen über Befehle)

## Installation

Alle Teilprogramme lassen sich mit make übersetzen.
```sh
make all && make install
```
Dieser Befehl übersetzt die Programme und verschiebt alle notwendigen Ressourcen nach ```/etc/rssipos```.

## Batman-adv
Zur Übertragung wurde das Batman-adv-Protokoll auf Layer-2 verwendet, das von open-mesh.org zu beziehen und zu installieren ist.
Nach der Installation kann das Mesh-Netzwerk automatisiert gestartet werden, wenn das Programm mit folgendem Befehl gestartet wird:
```sh
rssipos bat # startet und verwaltet das batman-adv Protokoll
```
Die zur Verwendung vorgesehenen Netzwerkadapter müssen in die ```rssi.macs```-Datei eingetragen werden.
Diese befindet sich im Installationspfad.

## Verwendung der Programme
Die Programme können mit der ```-h```-Option gestartet werden, um ausführliche Informationen der Verwendung zu erhalten.
```sh
(rssipos|rssisend) -h
```

Prinzipiell muss jedes teilnehmende Gerät einmal rssipos starten lassen.
Jedes am Mesh-Netzwerk teilnehmende Gerät kann über rssisend Steuerbefehle absetzen,
um eine neue Datensammlung zu initiieren, zu speichern oder zu übertragen.
