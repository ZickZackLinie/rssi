#ifndef _NETLINK_CONNECTOR_H
#define _NETLINK_CONNECTOR_H

#include "netlink/genl/ctrl.h"
#include <fstream> // reading macs file
#include <vector> // mac addresses from file

#include <utils.h>

// copy this from iw

class NetlinkConnector {

private:
	static bool sucheNurFavorit;
	static int usedMacIdx;
	static int itfidx;
	static int expectedId;
	static int lastStasFound;
	static unsigned int lastAnswerSeq;
	static uint32_t txpower;
	static std::string name;
	static std::string desiredMac;
	static unsigned char mac[ETH_ALEN];
	static uint16_t currentLevel;
	static uint16_t currentRound;
	//static std::vector<std::string> macs;
	nl_sock* sk;
	int dumpRSSI(nl_sock* sk, int usedItfIdx = 0);
	bool dumpInterfaces(nl_sock* sk);
	static int print_sta_handler(struct nl_msg *msg, void *arg);
	static int print_iface_handler(struct nl_msg *msg, void *arg);
	static int msgFinish(struct nl_msg *msg, void *arg);
	

public:
	NetlinkConnector ();
	int lookForRssi(uint16_t level);
	// try to use this mac
	// find one iff null
	// sets up VP
	bool lookForMac(const char * macstring);
	unsigned char * getItfMac();
	const char * getItfName(); // wlan0 or similar
	uint32_t getTxPower();
	bool hasValidItf();

};

#endif