// g++ -o sendBC sendBC.cpp

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <errno.h>
#include <fstream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <vector>

#include <udpbuffer.h>
#include <utils.h>
#include <packet.h>
#define DEBUG
#ifdef DEBUG
	#define printer(fmt, ...) \
		do { printf(fmt, __VA_ARGS__); } while (0)
#else
	#define printer(fmt, ...)
#endif

#define SERVERBC 0xA000 // the port users will be connecting to
#define SERVERSG 0xA001 // the port users will be connecting to

std::vector<unsigned char> supportedPackets;
std::vector<std::vector<struct in_addr> > groups;

std::ofstream logfile;

int sendTo(Packet * pkt, unsigned int groupnr) {
	int sockfd;
	struct sockaddr_in their_addr; // connector's address information
	int numbytes;
	int broadcast = 1;
	



	if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
		perror("socket");
		exit(1);
	}

	// this call is what allows broadcast packets to be sent:
	if (setsockopt(sockfd, SOL_SOCKET, SO_BROADCAST, &broadcast,
		sizeof broadcast) == -1) {
		perror("setsockopt (SO_BROADCAST)");
		exit(1);
	}

	// NON-BROADCASTING
	if (groupnr > 0) {

		std::vector<unsigned char> datastream = pkt->make();

		for (auto _host : groups[groupnr]) {

			their_addr.sin_family = AF_INET;			// host byte order
			their_addr.sin_port = htons(SERVERSG);	// short, network byte order
			their_addr.sin_addr = _host; 
			// memsetting sin_zero to 0 - removed in newer versions

			if ((numbytes = sendto(sockfd, &datastream[0],
					datastream.size(), 0,
					(struct sockaddr *)&their_addr, sizeof their_addr)) == -1) {
				perror("sendto");
				exit(1);
			}

			pkt->target = their_addr.sin_addr;
			pkt->printHdr();

		}

	// BROADCASTING
	} else {
		std::vector<unsigned char> datastream = pkt->make();
		
		inet_aton("192.168.2.255", &their_addr.sin_addr);
		their_addr.sin_family = AF_INET;			// host byte order
		their_addr.sin_port = htons(SERVERBC);	// short, network byte order

		if ((numbytes = sendto(sockfd, &datastream[0],
				datastream.size(), 0,
				(struct sockaddr *)&their_addr, sizeof their_addr)) == -1) {
			perror("sendto");
			exit(1);
		}

		pkt->target = their_addr.sin_addr;
		pkt->printHdr();
	}
	close(sockfd);

	return 0;
}

std::vector<struct in_addr> readGroup (unsigned int grnr) {
	std::vector<struct in_addr> hostlist;
	uint8_t x = 0;
	do {
		char line [256] = {0};
		printf("beteiligte Hostnummer (gr %u) (0=Ende): ", grnr);
		fgets(line, sizeof(line), stdin);
		int matches = sscanf(line, "%hhu%*s", &x);
		if (matches > 0 && x != 0) {
			char ipaddress[16] = {0};
			sprintf(ipaddress, "192.168.2.%u", x);
			struct in_addr sin_addr;
			inet_aton(ipaddress, &sin_addr);
			bool found = false;
			for (auto add : hostlist) {
				if (add.s_addr == sin_addr.s_addr) {
					found = true;
					break;
				}
			}
			if (!found) {
				hostlist.push_back(sin_addr);
				printf("IP: %s\n", ipaddress);
			} else {
				printf("IP: %s bereits bekannt..\n", ipaddress);
			}
		}
	} while (x!=0);
	printf("Gruppe %u hat %zu IP-Adressen gesammelt\n", grnr, hostlist.size());
	return hostlist;
}


bool doStuff () {
	std::string logger = "";
	time_t k_stamp = time(0);
	char timebuffer[80];
	strftime(timebuffer,80,"%T",localtime(&k_stamp));
	logger += timebuffer; logger += " : ";
	logfile.open(QUOTE(FILE_LOGSND), std::ofstream::out | std::ofstream::app);
	
	unsigned int groupnr;
	unsigned int arg1;
	unsigned int arg2;
	uint8_t pkttype;
	struct in_addr emptyaddr;
	char line [356] = {0};
	printf("(gr) (pktchr) (args...):  ");
	fgets(line, sizeof(line), stdin);
	int matches = sscanf(line, "%u %c %u %u%*s", &groupnr, &pkttype, &arg1, &arg2);
	if (matches > 1 && groupnr < groups.size()) {
		switch (pkttype) {
		case PKT_EXECCMD: {
			char line2 [356] = {0};
			//char dirname2 [356] = {0};
			printf("command to execute:  ");
			fgets(line2, sizeof(line2), stdin);
			//sscanf(line2, "%s%*s", dirname2);
			std::string command(line2);
			if (command.size()>0) {
				//command.at(command.size()-1) = ' ';
				command.erase(command.size()-1);
			}

			uint16_t strLen = (uint16_t) command.size();
			std::vector<uint8_t> sizeContent;
			hton_vec_any(strLen, sizeContent);

			Packet * tmp = new Packet(0,pkttype,1,1,emptyaddr);
			// insert filename
			tmp->setContent((uint8_t *) command.c_str(), strLen);
			// push size to front
			tmp->content.insert(
				tmp->content.begin(),
				sizeContent.begin(),
				sizeContent.end() );

			sendTo(tmp, groupnr);
			logger += std::to_string(groupnr)+" "+(char)PKT_EXECCMD;
			logger += " '"+command+"'\n";
			logger += "         > " + tmp->getHdr()+"\n";
			printf("%s, %zd\n", logger.c_str(), command.size());
			logfile.write(logger.c_str(), logger.size());
			logfile.flush();
			delete tmp;
			break;
		}
		case 68: {
			if (matches >= 3) {
				
				size_t szall = arg1;
				std::vector<uint8_t> data (szall, '0');
				
				// create packets from given data
				// make as much packets as necessary
				std::vector<Packet *> newlist;
				size_t num = (((int)szall-1) / PKT_MAX_SIZE) + 1;
				int jobnr = 0;
				for (size_t idx = 0; idx < num; idx++) {
					printf("Baue Paket %zu\n", idx);
					size_t start = idx * PKT_MAX_SIZE;
					size_t bytes = szall - start;
					bytes = (bytes > PKT_MAX_SIZE ? PKT_MAX_SIZE : bytes); // cap!
					// initialize!
					Packet * tmp = new Packet(
						jobnr,
						68,
						(uint8_t)idx + 1,
						(uint8_t)num,
						emptyaddr );
					// assume single ip for target
					// --> dont set port, should be PT_UDP_TK
					tmp->setContent(&data[start], bytes);
					newlist.push_back(tmp);
				}

				for (auto pk : newlist) {
					sendTo(pk, 0);
				}
				for (auto pk : newlist) {
					delete pk;
				}

			} else {
				printf("argumente 'D <4:bytes to send>'\n");
			}
			break;
		}
		case PKT_GETCIS: {
			uint16_t maxPerStation = arg1;
			if (matches >= 3) {
				printf("maxPerStation %hu\n", maxPerStation);
				Packet * tmp = new Packet(0,pkttype,1,1,emptyaddr);
				hton_vec_any(maxPerStation, tmp->content);
				sendTo(tmp, groupnr);
				delete tmp;
			} else {
				printf("argumente 'G <2:MR-maxsize>'\n");
			}
			break;
		}
		case PKT_SETMAX: {
			uint16_t maxPerStation = arg1;
			if (matches >= 3) {
				printf("maxPerStation %hu\n", maxPerStation);
				Packet * tmp = new Packet(0,pkttype,1,1,emptyaddr);
				hton_vec_any(maxPerStation, tmp->content);
				sendTo(tmp, groupnr);
				delete tmp;
			} else {
				printf("argumente 'M <2:MR-maxsize>'\n");
			}
			break;
		}
		case PKT_DATAPRINT: {
			Packet * tmp = new Packet(0,pkttype,1,1,emptyaddr);
			sendTo(tmp, groupnr);
			delete tmp;
			break;
		}
		case PKT_DATASAVE: {
			char line2 [356] = {0};
			char dirname2 [356] = {0};
			printf("absolute dir (for CIs):  ");
			fgets(line2, sizeof(line2), stdin);
			sscanf(line2, "%s%*s", dirname2);
			std::string fname(dirname2);
			
			uint16_t strLen = (uint16_t) fname.size();
			std::vector<uint8_t> sizeContent;
			hton_vec_any(strLen, sizeContent);

			Packet * tmp = new Packet(0,pkttype,1,1,emptyaddr);
			// insert filename
			tmp->setContent((uint8_t *) fname.c_str(), strLen);
			// push size to front
			tmp->content.insert(
				tmp->content.begin(),
				sizeContent.begin(),
				sizeContent.end() );
			sendTo(tmp, groupnr);
			delete tmp;
			break;
		}
		case PKT_SEQ_SETLVL: {
			uint16_t level = arg1;
			mtime_t waittime = arg2;
			if (matches >= 4) {
				printf("level %u waittime %u\n", level, waittime);
				Packet * tmp = new Packet(0,pkttype,1,1,emptyaddr);
				hton_vec_any(level, tmp->content);
				hton_vec_any(waittime, tmp->content);
				sendTo(tmp, groupnr);
				delete tmp;
			} else {
				printf("argumente 's <2:lvl> <4:wait>'\n");
			}
			break;
		}
		case PKT_SEQ_TERM: {
			Packet * tmp = new Packet(0,pkttype,1,1,emptyaddr);
			sendTo(tmp, groupnr);
			delete tmp;
			break;
		}
		case PKT_PREFIX: {
			char line2 [356] = {0};
			char dirname2 [356] = {0};
			printf("prefix for savedir (for CIs):  ");
			fgets(line2, sizeof(line2), stdin);
			sscanf(line2, "%s%*s", dirname2);
			std::string fname(dirname2);
			
			uint16_t strLen = (uint16_t) fname.size();
			std::vector<uint8_t> sizeContent;
			hton_vec_any(strLen, sizeContent);

			Packet * tmp = new Packet(0,pkttype,1,1,emptyaddr);
			// insert filename
			tmp->setContent((uint8_t *) fname.c_str(), strLen);
			// push size to front
			tmp->content.insert(
				tmp->content.begin(),
				sizeContent.begin(),
				sizeContent.end() );
			sendTo(tmp, groupnr);
			delete tmp;
			break;
		}
		case PKT_QUITALL: {
			Packet * tmp = new Packet(0,pkttype,1,1,emptyaddr);
			sendTo(tmp, groupnr);
			delete tmp;
			break;
		}
		case PKT_TIMEDIFF: {
			uint8_t host = arg1;
			if (matches >= 3) {
				printf("hostnum %hhu\n", host);
				Packet * tmp = new Packet(0,pkttype,1,1,emptyaddr);
				hton_vec_any(host, tmp->content);
				sendTo(tmp, groupnr);
				delete tmp;
			} else {
				printf("argumente 'x <1:#host>'\n");
			}
			break;
		}
		case PKT_TIMESYNC: {
			Packet * tmp = new Packet(0,pkttype,1,1,emptyaddr);
			sendTo(tmp, groupnr);
			delete tmp;
			break;
		}
		default:
			printf("Packettype nicht unterstuetzt\n");
			break;
		}
		logfile.close();
		return true;
	} else {
		logfile.close();
		matches = sscanf(line, "%s%*s", line);
		std::string zeile = std::string(line);
		if (strcmp(zeile.c_str(), "help")==0) {
			printf("(PKT_EXECCMD)\t'C \"command\"'\n");
			printf("(nur fuellung)\t'D <4:bytes to send>'\n");
			printf("(PKT_GETCIS)\t'G <2:MR-maxsize>'\n");
			printf("(PKT_SETMAX)\t'M <2:MR-maxsize>'\n");
			printf("(PKT_DATAPRINT)\t'P'\n");
			printf("(PKT_PREFIX)\t'p \"prefix (for CIs dir)\"'\n");
			printf("(PKT_DATASAVE)\t'S \"absolute dir (for CIs)\"'\n");
			printf("(PKT_SEQ_SETLVL)'s <2:lvl> <4:wait>'\n");
			printf("(PKT_SEQ_TERM)\t't'\n");
			printf("(PKT_QUITALL)\t'q'\n");
			printf("(PKT_TIMEDIFF)\t'x <1:#host>'\n");
			printf("(PKT_TIMESYNC)\t'z'\n");
			return true;
		} else  if (strcmp(zeile.c_str(), "quit")==0) {
			return false;
		} else return true;
	}
	return false;
}

bool doStuffArg (int argc, int startarg, char *argv[]) {
	uint8_t pkttype = argv[startarg][0];
	struct in_addr emptyaddr;
	//printf("(gr) (pktchr) (args...):  ");
	switch (pkttype) {
	case PKT_EXECCMD: {
		std::string command(argv[startarg+1]);
		uint16_t strLen = (uint16_t) command.size();
		std::vector<uint8_t> sizeContent;
		hton_vec_any(strLen, sizeContent);

		Packet * tmp = new Packet(0,pkttype,1,1,emptyaddr);
		// insert filename
		tmp->setContent((uint8_t *) command.c_str(), strLen);
		// push size to front
		tmp->content.insert(
			tmp->content.begin(),
			sizeContent.begin(),
			sizeContent.end() );
		sendTo(tmp, 0);
		
		delete tmp;
		break;
	}
	case 68: { // 'D' like data

		size_t szall = atoi(argv[startarg+1]);
		std::vector<uint8_t> data (szall, '0');
		
		// create packets from given data
		// make as much packets as necessary
		std::vector<Packet *> newlist;
		size_t num = (((int)szall-1) / PKT_MAX_SIZE) + 1;
		int jobnr = 0;
		for (size_t idx = 0; idx < num; idx++) {
			printf("Baue Paket %zu\n", idx);
			size_t start = idx * PKT_MAX_SIZE;
			size_t bytes = szall - start;
			bytes = (bytes > PKT_MAX_SIZE ? PKT_MAX_SIZE : bytes); // cap!
			// initialize!
			Packet * tmp = new Packet(
				jobnr,
				68,
				(uint8_t)idx + 1,
				(uint8_t)num,
				emptyaddr );
			// assume single ip for target
			// --> dont set port, should be PT_UDP_TK
			tmp->setContent(&data[start], bytes);
			newlist.push_back(tmp);
		}

		for (auto pk : newlist) {
			sendTo(pk, 0);
		}
		for (auto pk : newlist) {
			delete pk;
		}

		break;
	}
	case PKT_GETCIS: {
		uint16_t maxPerStation = (uint16_t) atoi(argv[startarg+1]);
		Packet * tmp = new Packet(0,pkttype,1,1,emptyaddr);
		hton_vec_any(maxPerStation, tmp->content);
		sendTo(tmp, 0);
		delete tmp;
		break;
	}
	case PKT_SETMAX: {
		uint16_t maxPerStation = (uint16_t) atoi(argv[startarg+1]);
		Packet * tmp = new Packet(0,pkttype,1,1,emptyaddr);
		hton_vec_any(maxPerStation, tmp->content);
		sendTo(tmp, 0);
		delete tmp;
		break;
	}
	case PKT_DATAPRINT: {
		Packet * tmp = new Packet(0,pkttype,1,1,emptyaddr);
		sendTo(tmp, 0);
		delete tmp;
		break;
	}
	case PKT_DATASAVE: {
		std::string fname(argv[startarg+1]);		
		uint16_t strLen = (uint16_t) fname.size();
		std::vector<uint8_t> sizeContent;
		hton_vec_any(strLen, sizeContent);

		Packet * tmp = new Packet(0,pkttype,1,1,emptyaddr);
		// insert filename
		tmp->setContent((uint8_t *) fname.c_str(), strLen);
		// push size to front
		tmp->content.insert(
			tmp->content.begin(),
			sizeContent.begin(),
			sizeContent.end() );
		sendTo(tmp, 0);
		delete tmp;
		break;
	}
	case PKT_SEQ_SETLVL: {
		uint16_t level = (uint16_t) atoi(argv[startarg+1]);
		mtime_t waittime = (mtime_t) atoi(argv[startarg+2]);;
		Packet * tmp = new Packet(0,pkttype,1,1,emptyaddr);
		hton_vec_any(level, tmp->content);
		hton_vec_any(waittime, tmp->content);
		sendTo(tmp, 0);
		delete tmp;
		break;
	}
	case PKT_SEQ_TERM: {
		Packet * tmp = new Packet(0,pkttype,1,1,emptyaddr);
		sendTo(tmp, 0);
		delete tmp;
		break;
	}
	case PKT_PREFIX: {
		std::string fname(argv[startarg+1]);
		uint16_t strLen = (uint16_t) fname.size();
		std::vector<uint8_t> sizeContent;
		hton_vec_any(strLen, sizeContent);

		Packet * tmp = new Packet(0,pkttype,1,1,emptyaddr);
		// insert filename
		tmp->setContent((uint8_t *) fname.c_str(), strLen);
		// push size to front
		tmp->content.insert(
			tmp->content.begin(),
			sizeContent.begin(),
			sizeContent.end() );
		sendTo(tmp, 0);
		delete tmp;
		break;
	}
	case PKT_QUITALL: {
		Packet * tmp = new Packet(0,pkttype,1,1,emptyaddr);
		sendTo(tmp, 0);
		delete tmp;
		break;
	}
	case PKT_TIMEDIFF: {
		uint8_t host = (uint8_t) atoi(argv[startarg+1]);
		Packet * tmp = new Packet(0,pkttype,1,1,emptyaddr);
		hton_vec_any(host, tmp->content);
		sendTo(tmp, 0);
		delete tmp;
		break;
	}
	case PKT_TIMESYNC: {
		Packet * tmp = new Packet(0,pkttype,1,1,emptyaddr);
		sendTo(tmp, 0);
		delete tmp;
		break;
	}
		default:
		printf("Packettype nicht unterstuetzt\n");
		printf("(PKT_EXECCMD)\t'C \"command\"'\n");
		printf("(nur fuellung)\t'D <4:bytes to send>'\n");
		printf("(PKT_GETCIS)\t'G <2:MR-maxsize>'\n");
		printf("(PKT_SETMAX)\t'M <2:MR-maxsize>'\n");
		printf("(PKT_DATAPRINT)\t'P'\n");
		printf("(PKT_PREFIX)\t'p \"prefix (for CIs dir)\"'\n");
		printf("(PKT_DATASAVE)\t'S \"absolute dir (for CIs)\"'\n");
		printf("(PKT_SEQ_SETLVL)'s <2:lvl> <4:wait>'\n");
		printf("(PKT_SEQ_TERM)\t't'\n");
		printf("(PKT_QUITALL)\t'q'\n");
		printf("(PKT_TIMEDIFF)\t'x <1:#host>'\n");
		printf("(PKT_TIMESYNC)\t'z'\n");
		break;
	}
	return true;
}

int main(int argc, char *argv[]) {
	if (argc > 1) {
		logfile.open(QUOTE(FILE_LOGSND), std::ofstream::out | std::ofstream::app);
		if (doStuffArg(argc, 1, argv)){
			logfile.flush();
			logfile.close();
			return 0;
		} else {
			logfile.close();
			return 1;
		}
	}
	//printer("Hallo Welt %d!\n", 3);
	supportedPackets.push_back(PKT_EXECCMD);
	supportedPackets.push_back(PKT_GETCIS);
	supportedPackets.push_back(PKT_SETMAX);
	supportedPackets.push_back(PKT_DATAPRINT);
	supportedPackets.push_back(PKT_DATASAVE);
	supportedPackets.push_back(PKT_SEQ_SETLVL);
	supportedPackets.push_back(PKT_SEQ_TERM);
	supportedPackets.push_back(PKT_PREFIX);
	supportedPackets.push_back(PKT_QUITALL);
	supportedPackets.push_back(PKT_TIMEDIFF);
	supportedPackets.push_back(PKT_TIMESYNC);
	
	groups.push_back(std::vector<struct in_addr>());

	unsigned int groupsz = 0;
	char line [256] = {0};
	printf("Anzahl der Gruppen (Gr.0=BC): ");
	fgets(line, sizeof(line), stdin);
	int matches = sscanf(line, "%u%*s", &groupsz);
	if (matches > 0 && groupsz != 0) {
		for (unsigned int i = 1; i <= groupsz; i++) {
			groups.push_back(readGroup(i));
			printf("=============\n");
		}
	}

	if (groups.size() > 1)
		while (doStuff());
	return 0;
	//return sendBC(argc, argv);
}