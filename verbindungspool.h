
#ifndef _VERBINDUNGSPOOL_H_
#define _VERBINDUNGSPOOL_H_

#include <unordered_map>
#include <vector>
#include <mutex>
#include <cstring>

#include <clientinfo.h>

#define VP_RM_DATA    0
#define VP_RM_CIOTHER 1
#define VP_RM_CIALL   2

class VerbindungsPool {
private:
	// bei char* funktioniert das look-up nicht korrekt
	static std::unordered_map<std::string, ClientInfo *> mapping;
	static std::mutex mtx;
	static ClientInfo * pmyself;
	static void lock(const char * reason, bool silent);
	static void unlock(const char * reason, bool silent);

public:
	//VerbindungsPool();
	static void createMyInfo (	ClientInfo * ci );
	static void createMyInfo (	const char * macstring, uint8_t hostnum,
								uint32_t txpower,
								uint8_t anchor,
								float latitude, float longitude);
	static void mergeOrInsert (ClientInfo * ci);
	static void mergeOrInsert (std::vector<uint8_t> &streambuffer);

	static int appendToMe (uint16_t level, const char * name, mtime_t ttime, uint8_t rssi);

	static void cleanUp(int vp_rm_howFar);
	// get ptr to CI with name 'staName'
	// ret. null iff not existing
	// ret. ptr to myself iff staName empty
	static ClientInfo * get (std::string staName);
	static int count(); // number of known CIs
	static void buildGraph();
	static void buildDistanceFiles(std::string prefixDir);
	static void saveAll(std::string absDir);
	static void print(bool onlySignificant, uint16_t maxPerStation);

};

#endif
