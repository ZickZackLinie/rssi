#ifndef _PACKET_H_
#define _PACKET_H_

#include <netinet/in.h>
#include <sys/socket.h>
#include <vector>
#include <cstring>

#include <utils.h> // datatypes and converting

#define PKT_BUF_SIZE   1024// max. transfer size
#define PKT_HDR_SIZE   9   // 9 bytes header
#define PKT_MAX_SIZE   ( PKT_BUF_SIZE - PKT_HDR_SIZE ) // bytes for pure data
#define PKT_MAX_SPLIT  255 // maximum subpackets
#define PKT_MAX_RESEND 5   // after #attempts, never sent again

// living status of packet
#define PKT_S_FRESH    0   // unhandled
#define PKT_S_KEEP     1   // enough handled but stay
#define PKT_S_OVER     2   // stay no longer, remove

// definition for IN and OUT of calcState
#define PKT_CS_FORIN   true
#define PKT_CS_FOROUT  false

// possible packet types
#define PKT_UNKNOWN    48  // 0
#define PKT_ACK        65  // A (reuse<injob><pkt>)// noack
#define PKT_RESEND     66  // B (reuse<injob><pkt>)// noack
#define PKT_EXECCMD    67  // C <2:sz><sz:cmdline>
// clientinfos
#define PKT_GETCIS     71  // G <2:MR-maxsz>
#define PKT_TAKECIS    72  // H <ci-data>
#define PKT_RMCIS      75  // K
#define PKT_SETMAX     77  // M <2:MR-maxsz>
// datalines
//#define PKT_DATAOPEN 79  // O
#define PKT_DATAPRINT  80  // P
#define PKT_RMDATA     82  // R
#define PKT_DATASAVE   83  // S <2:sz><sz:absdir>
// do something
//#define PKT_SEQHDLCRT  99  // c <2:lvl><4:wait><1:cont.>  // timecritic!
//#define PKT_SEQHDLDEL  100 // d                           // timecritic!
#define PKT_PREFIX     112 // p <2:sz><sz:prefix>
#define PKT_QUITALL    113 // q
#define PKT_SEQ_SETLVL 115 // s <2:lvl><4:wait>           // timecritic!
#define PKT_SEQ_TERM   116 // t                           // timecritic!
#define PKT_TIMEDIFF   120 // x <1:host>           // noack, timecritic!
#define PKT_TIMEDIFFME 121 // y <time0> [<time1>]  // noack, timecritic!
#define PKT_TIMESYNC   122 // z

class Packet {

public:
	uint8_t type = PKT_UNKNOWN; // what is it?
	mtime_t ttime = 0; // when pkt was created
	uint16_t job = 0; // my job for all packets | to be acked
	uint8_t pktnum = 0; // # of packet [1..pktmax]
	uint8_t pktmax = 0; // max # of packets for whole data
	std::vector<uint8_t> content; // the datastuff

	struct in_addr target; // where from / to
	unsigned short port = PT_UDP_TK; // target port, assume no BC
	bool acked = false; // got an ack for this
	uint8_t cntsnd = 0; // how many times been sent
	//uint8_t state = PKT_S_FRESH;
	bool useBC = false; // assume no BC
	bool reconstructed = false; // use for completed packets
	std::vector<struct in_addr> answerIPs; // add ip, if BC answer occurs


	Packet (uint16_t jobnr,
			uint8_t type,
			uint8_t pktnum,
			uint8_t pktmax,
			struct in_addr target);
	Packet (std::vector<uint8_t> &datastream);

	std::vector<uint8_t> make ();

	bool shouldBeAcked();
	bool isTimeCritical();
	uint8_t calcState(bool forIN, mtime_t oldestacceptable);

	void setContent(uint8_t * datastream, size_t numBytes);
	void setContent(std::vector<uint8_t> datastream);

	static std::string strPktType(int type);
	void printHdr();
	std::string getHdr();
};

#endif
