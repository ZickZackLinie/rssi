#include <iostream>
#include <fstream>

#include <messreihe.h>
#include <clientinfo.h>
#include <utils.h>

// ClientInfo

/*int ClientInfo::
doStuff(){
		
	//char tmpmac[] = "02:12:34:56:78:9a";
	//mac_addr_a2n(mac, tmpmac);

	return 0;

}*/

ClientInfo::
ClientInfo(	const char * mac_name, uint8_t host, 
			uint32_t txPower, uint8_t nAnchorState,
			float nlat, float nlon){

	lat = nlat;
	lon = nlon;
	txpower = txPower;

	anchorState = nAnchorState;
	hostnum = host;
	lastSeen = 0;

	// use live data as default
	// -> collect everything
	//    not only mean values
	isLiveData = true;
	// copy and reinterpret mac address as byte array
	smac = std::string(mac_name);
	char mac_address[ETH_CLEN] = {0};
	memcpy(mac_address, mac_name, ETH_CLEN-1);
	mac_addr_a2n(mac, mac_address);
}


void ClientInfo::
append (uint16_t level, const char * name, mtime_t ttime, uint8_t rssi) {
	lock("(append)", true); //lock mutex
	// first look up:
	std::map<std::string, Messreihe* >::const_iterator got
		= mapping[level].find(name);

	Messreihe * tmp = nullptr;
	if (got == mapping[level].end()) {
		tmp = new Messreihe(maxDataKeep);
		mapping[level].insert(std::pair<std::string, Messreihe *>(name, tmp));
	} else {
		tmp = got->second;
	}
	
	tmp->insert(ttime, rssi);
	if (ttime > lastSeen)
		lastSeen = ttime;
	unlock("(append)", true); // release mutex
}

mtime_t ClientInfo::
getLastSeen () {
	return lastSeen;
}

std::string ClientInfo::
getName () {
	return smac;
}

bool ClientInfo::
getPos (float &outlat, float &outlon) {
	if (anchorState != CI_AN_YES)
		return false;
	outlat = lat;
	outlon = lon;
	return true;
}

// return list of mac address strings
// keys of mapping
std::vector<std::string> ClientInfo::
getConnectionNames (uint16_t level) {
	std::vector<std::string> retvec;
	for (auto _pair : mapping[level]) 
		retvec.push_back(_pair.first);
	return retvec;
}

// nullptr iff not found,
// MR * else
Messreihe * ClientInfo::
getConnection (uint16_t level, std::string strMac) {
	// look up name an return MR *
	std::map<std::string, Messreihe* >::const_iterator got
		= mapping[level].find(strMac);
	Messreihe * tmp = nullptr;
	if (got != mapping[level].end()) {
		tmp = got->second;
	}
	return tmp;
}

// remove every sample to mac address
void ClientInfo::
eraseConnections (std::string Mac) {
	lock("erase Mac", true);
	for (auto _pair_l_mp : mapping) {
		uint16_t _level = _pair_l_mp.first;
		if (mapping[_level].find(Mac) != mapping[_level].end()) {
			// if existent, remove
			delete mapping[_level][Mac];
			mapping[_level].erase(Mac);
			// loesche mapping[level] wenn leer
			if (mapping[_level].size() == 0) {
				mapping.erase(_level);
			}
		}
	}
	unlock("erase Mac", true);
}

int ClientInfo::
update (ClientInfo * ci) {
	//ci->print(false, 20);
	// lock???
	std::map<uint16_t, std::map<std::string, Messreihe*> >::iterator ciout;
	std::map<std::string, Messreihe* >::iterator myin;
	std::map<std::string, Messreihe* >::iterator ciin;
	for (ciout = ci->mapping.begin(); ciout != ci->mapping.end(); ++ciout) {
		uint16_t lvl = ciout->first;
		for (
			ciin = ci->mapping[lvl].begin(); 
			ciin != ci->mapping[lvl].end(); ++ciin) {

			myin = mapping[lvl].find(ciin->first);

			Messreihe * tmp = nullptr;
			if (myin == mapping[lvl].end()) {
				tmp = ciin->second;
				mapping[lvl].insert(std::pair<std::string, Messreihe *>(ciin->first, tmp));
			} else {
				tmp = myin->second;
				tmp->merge(ciin->second);
			}
		}
	}
	return 0;
}


void ClientInfo::
allMakeMeanOnly () {
	isLiveData = false;
	for (auto _outerpair : mapping)
		for (std::pair<std::string, Messreihe *> x: _outerpair.second)
			x.second->makeMeanOnly();
}
void ClientInfo::
allMakeLiveData () {
	isLiveData = true;
	for (auto _outerpair : mapping)
		for (std::pair<std::string, Messreihe *> x: _outerpair.second)
			x.second->makeLiveData();
}
void ClientInfo::
allSetMaxKeep (int maxNum) {
	maxDataKeep = maxNum;
	for (auto _outerpair : mapping)
		for (std::pair<std::string, Messreihe *> x: _outerpair.second){
			/*int keep = */x.second->setMaxKeep(maxNum);
			//printf("block %u keeps %d elements\n", _outerpair.first, keep);
		}
}
void ClientInfo::
allShrinkToLast (int num) {
	for (auto _outerpair : mapping) {
		for (std::pair<std::string, Messreihe *> x: _outerpair.second) {
			x.second->shrinkToLast(num);
			if (num == 0) 
				delete x.second;
		}
		if (num == 0)
			_outerpair.second.clear();
	}
	if (num == 0)
		mapping.clear();
}
void ClientInfo::
allShrinkToNewerThan (uint16_t level, mtime_t timeInSeconds) {
	for (std::pair<std::string, Messreihe *> x: mapping[level])
		x.second->shrinkToNewerThan(timeInSeconds);
}

// non empty only
std::vector<uint16_t> ClientInfo::
getLevels (bool onlySignificant) {
	std::vector<uint16_t> retvec;
	for (auto _pair : mapping) {
		int numOfStas = 0;
		for (auto x : _pair.second){
			// ignore connections with too less measurements
			if (onlySignificant && !x.second->isSignificant())
				continue;
			numOfStas++;
		}
		if (numOfStas > 0)
			retvec.push_back(_pair.first);
	}
	return retvec;
}

void ClientInfo::
eraseLevels (std::vector<uint16_t> levels) {
	lock("erase lvl", true);
	for (auto _level : levels) {
		if (mapping.find(_level) != mapping.end()) {
			// if existent
			for (auto _pair_s_mr : mapping[_level]) {
				delete _pair_s_mr.second;
			}
			mapping.erase(_level);
		}
	}
	unlock("erase lvl", true);
}


int ClientInfo::
serialize (
	std::vector<uint8_t> &streambuffer,
	bool onlySignificant, 
	uint16_t maxPerStation,
	std::vector<uint16_t> levels ) {

	lock("serialization", false);
	
	// == Content ==			== Bytes ==
	// [FROM-MAC address]		[6] 
	// [ANCHORSTATE]			[1]
	// [LAT] [LON] (position)	[4] [4]
	// [TXPOWER]				[4]
	// 
	// [# of level-entries]		[2]
	// [isLiveData] [setToMax]	[1] [2]
	// [levelnum] [#stas]		[2] [1]
	// STA1 - [name] [entries]	[6] [2]
	// 		- [time1] [rssi1]	[4] [1]
	//		- [time2] [rssi2]	 |   |
	//		- ...				[4] [1]
	// STA2 - ...
	// [levelnum] [#stas]		[2] [1]
	// ...
	
	// [FROM-MAC address] [6]
	streambuffer.insert(streambuffer.end(), mac, mac+ETH_ALEN);

	// [ANCHORSTATE]			[1]
	hton_vec_any(anchorState, streambuffer);
	
	// [LAT] [LON] (position)
	// convert position
	//lat=4.2;
	//lon=0.0255;
	uint32_t nlat = (uint32_t)(100000 * lat);
	uint32_t nlon = (uint32_t)(100000 * lon);
	hton_vec_any(nlat, streambuffer);
	hton_vec_any(nlon, streambuffer);

	// [TXPOWER]
	hton_vec_any(txpower, streambuffer);
	
	// [# of level-entries]		[2]
	std::vector<uint16_t> availLevels = getLevels(onlySignificant);
	std::vector<uint16_t> useLevels = intersection(levels, availLevels);
	uint16_t numOfLevels = useLevels.size();
	//printf("SZ: num of levels: %u\n", numOfLevels);
	hton_vec_any(numOfLevels, streambuffer);

	// [isLiveData] [setToMax]	[1] [2]
	uint8_t shallBeLiveData = (maxPerStation ==1);
	hton_vec_any(shallBeLiveData, streambuffer);
	hton_vec_any(maxPerStation, streambuffer);

	printf("numOfLevels: %u, maxPerStation: %u\n", numOfLevels, maxPerStation);

	for (uint16_t lvl : useLevels) {
		
		// [levelnum] [#stas]		[2] [1]
		hton_vec_any(lvl, streambuffer);
		// count amount of stations to include
		uint8_t numOfStas = 0;
		for (std::pair<std::string, Messreihe *> x: mapping[lvl]){
			// ignore connections with too less measurements
			if (onlySignificant && !x.second->isSignificant())
				continue;
			numOfStas++;
		}
		//printf("SZ lvl(%u) #stas(%hhu)\n", lvl, numOfStas);
		hton_vec_any(numOfStas, streambuffer);


		// serialize them
		for (std::pair<std::string, Messreihe *> x: mapping[lvl]){
			// ignore connections with too less measurements
			if (onlySignificant && !x.second->isSignificant())
				continue;
			// convert mac address
			unsigned char tmpmac[ETH_ALEN] = {0};
			char ctmpmac[ETH_CLEN] = {0};
			memcpy(ctmpmac, x.first.c_str(), x.first.size());
			mac_addr_a2n(tmpmac, ctmpmac);
			// write mac address
			for (int i = 0; i<ETH_ALEN; i++){
				streambuffer.push_back(tmpmac[i]);
			}
			// write # of data and data (mtime_t, uint8_t)
			int maxnummr = x.second->serializeInto(streambuffer, maxPerStation);
			printf("Serialized %d datas in lvl %u\n", maxnummr, lvl);
		}
	}

	unlock("serialization ended", false);
	int bytes = streambuffer.size();
	//printf("%d bytes serialized\n", bytes);
	return bytes;
}

int ClientInfo::
deserialize (std::vector<uint8_t> streambuffer) {
	const size_t size = streambuffer.size();

	if (size < (ETH_ALEN + (2 * sizeof(uint32_t)) + sizeof(uint8_t)) )
		return -1; // no mac address, position and size included

	size_t ptr = 0;

	// (S/)MAC ADDRESS STUFF
	for (int i = 0; i<ETH_ALEN; i++){
		mac[i] = streambuffer[i];
	}
	char ctmpmac[ETH_CLEN] = {0};
	mac_addr_n2a(ctmpmac, mac);
	smac = std::string(ctmpmac);
	hostnum = (uint8_t)get_num_of_mac(mac) +1;
	ptr += ETH_ALEN;

	// [ANCHORSTATE]			[1]
	ptr += ntoh_vec_any(anchorState, streambuffer, ptr);
	
	// [LAT] [LON] (position)	[4] [4]
	uint32_t nLat, nLon;
	ptr += ntoh_vec_any(nLat, streambuffer, ptr);
	ptr += ntoh_vec_any(nLon, streambuffer, ptr);
	lat = nLat/100000.0;
	lon = nLon/100000.0;
	
	// [TXPOWER]				[4]
	uint32_t ntxpwr;
	ptr += ntoh_vec_any(ntxpwr, streambuffer, ptr);
	txpower = ntxpwr;
	
	// [# of level-entries]		[2]
	uint16_t numOfLevels;
	ptr += ntoh_vec_any(numOfLevels, streambuffer, ptr);
	
	// [isLiveData] [setToMax]	[1] [2]
	uint8_t shallBeLiveData = 0;
	uint16_t setToMax = 0;
	ptr += ntoh_vec_any(shallBeLiveData, 	streambuffer, ptr);
	ptr += ntoh_vec_any(setToMax, 		streambuffer, ptr);
	isLiveData = (shallBeLiveData > 0) || (setToMax > 1);
	
	
	for (uint16_t l = 0; l < numOfLevels; l++) {
		// [levelnum] [#stas]		[2] [1]
		uint16_t level;
		ptr += ntoh_vec_any(level, streambuffer, ptr);
		uint8_t numOfStas;
		ptr += ntoh_vec_any(numOfStas, streambuffer, ptr);

		// STA1 - [name] [entries]	[6] [2]
		// 		- [time1] [rssi1]	[4] [1]
		//		- [time2] [rssi2]	 |   |
		//		- ...				[4] [1]
		// STA2 - ...
		for (int s = 0; s < numOfStas; s++) {

			// MIT EINZELNER STATION WEITERMACHEN
			// ..sollte das die Messreihe machen?
			//
			// mac address [name]   [6]
			unsigned char l_tmpmac[ETH_ALEN] = {0};
			for (int x = 0; x < ETH_ALEN; x++) {
				l_tmpmac[x] = streambuffer[ptr + x];
			}
			char l_ctmpmac[ETH_CLEN] = {0};
			mac_addr_n2a(l_ctmpmac, l_tmpmac);
			std::string l_smac(l_ctmpmac);
			ptr += ETH_ALEN;
			// size of following data [entries]	[2]
			uint16_t l_datasize;
			ptr += ntoh_vec_any(l_datasize, streambuffer, ptr);
			
			if (l_datasize == 0) {
				//printf("station empty --> continue\n");
				continue;
			}
			//printf("DE: blk(%u) -> sta(%s) num: %d\n", level, l_smac.c_str(), l_datasize);
			for (int k = 0; k < l_datasize; k++) {
				// 
				// need to check remaining byte size?
				//
				mtime_t k_time = 0;
				uint8_t k_rssi = 0;
				ptr += ntoh_vec_any(k_time, streambuffer, ptr);
				ptr += ntoh_vec_any(k_rssi, streambuffer, ptr);
				append(level, l_smac.c_str(), k_time, k_rssi);
			} // end data loop
		} // end station loop
	} // end level loop

	
	// reorganize/resize data
	if (!isLiveData) {
		allMakeMeanOnly();
	}/* else {
		allSetMaxKeep(setToMax);
	}*/
	return 0;
}


bool ClientInfo::
buildDistanceFiles (uint16_t level, std::string prefixDir) {

	lock("CI:buildDistanceFiles", false);
	for (std::pair<std::string, Messreihe *> x: mapping[level]) {
		Messreihe * tmp = x.second;
		int macIdx = get_num_of_mac(x.first);
		if (macIdx < 0) 
			continue; // unknown counterpart
		if (tmp->count() == 0)
			continue; // empty MR
		printf("\tbaue Messreihe (%u) zu (%d) [%hu]\n", hostnum, (macIdx+1), level);
		std::string contentC = tmp->serializeComma();
		std::string contentG = tmp->serializeGnuplot();
		try {
			char filename_c[250] = {0};
			char filename_g[250] = {0};
			sprintf(filename_c, "%s/%s/m%d_%d_%sm.txt",
				Options::usePathForOutFile.c_str(), 
				prefixDir.c_str(),
				(int)hostnum,
				(macIdx+1),
				std::to_string(level).c_str());
            sprintf(filename_g, "%s/%s/g%d_%d_%sm.txt",
				Options::usePathForOutFile.c_str(), 
				prefixDir.c_str(),
				(int)hostnum,
				(macIdx+1),
				std::to_string(level).c_str());

			std::ofstream outfile_c(filename_c, std::ofstream::out | std::ofstream::trunc);
			std::ofstream outfile_g(filename_g, std::ofstream::out | std::ofstream::trunc);
			outfile_c.write(contentC.c_str(), contentC.size());
			outfile_g.write(contentG.c_str(), contentG.size());
			outfile_c.flush();
			outfile_g.flush();
			outfile_c.close();
			outfile_g.close();
		} catch (int e) {
			printf("EXCEPTION buildDistanceFiles: %d\n", e);
			unlock("CI:buildDistanceFiles ERROR end", false);
			return false;
		}
	}
	unlock("CI:buildDistanceFiles end", false);
	return true;
}


bool ClientInfo::
buildHistFile (int toHost, std::string prefixDir) {

	lock("CI:buildHistFile", false);
	std::string content = "";
	int from = 20;
	int to = 100;
	std::vector<uint16_t> levels;// = getLevels(false);// not only significant
	for (uint16_t x = 0; x <= 105; x+=5)
		levels.push_back(x); // create every fifth level
	std::map<uint16_t, std::vector<int> > map_data;
	for (uint16_t level : levels) { 
		if (level % 5 != 0)
			continue;
		if (mapping.find(level) == mapping.end()) {
			// no real existing level
			// build fake histogram
			std::vector<int> _fakehist (to+1,0);
			map_data[level] = _fakehist;
			continue;
		}
		for (std::pair<std::string, Messreihe *> x: mapping[level]) {
			Messreihe * tmp = x.second;
			int macIdx = get_num_of_mac(x.first);
			if (macIdx < 0) 
				continue; // unknown counterpart
			if (tmp->count() == 0)
				continue; // empty MR
			if (toHost != (macIdx+1))
				continue; // wrong host
			printf("\tbaue Histogramm (%u) zu (%d) [%hu]\n", hostnum, (macIdx+1), level);
		
			std::vector<int> hist = tmp->getHistogram(from, to);
			map_data[level]	= hist;
			break;
		}
	}
	bool doNL = true;
	for (int dist = from; dist <= to; ++dist)
	{
		for (auto level : levels)
		{
			if (level % 5 != 0)
				continue;
			if (map_data.find(level) == map_data.end()) {
				doNL = false;
				continue;
			}
			doNL = true;
			int cnt = map_data[level][dist];
			content += std::to_string(dist) + " ";
			content += std::to_string(level) + " ";
			content += std::to_string(cnt) + "\n";
		}
		if (doNL)
			content += "\n";
	}

	try {
		char filename[250] = {0};
		sprintf(filename, "%s/%s/h%d_%d.txt",
			Options::usePathForOutFile.c_str(), 
			prefixDir.c_str(),
			(int)hostnum,
			(toHost));

		std::ofstream outfile(filename, std::ofstream::out | std::ofstream::trunc);
		outfile.write(content.c_str(), content.size());
		outfile.flush();
		outfile.close();
	} catch (int e) {
		printf("EXCEPTION buildHistFile: %d\n", e);
		unlock("CI:buildHistFile ERROR end", false);
		return false;
	}
	unlock("CI:buildHistFile end", false);
	return true;
}

/*bool ClientInfo::
buildHistFiles (std::string prefixDir) {

}*/


ClientInfo * ClientInfo::
copy () {
	ClientInfo * ci = new ClientInfo(
		smac.c_str(),hostnum,
		txpower,anchorState,
		lat,lon);
	ci->lastSeen = lastSeen;
	memcpy(ci->mac, mac, ETH_ALEN);
	ci->isLiveData = isLiveData;

	lock("CI:copy", false);
	for (auto _outerpair : mapping)
		for (std::pair<std::string, Messreihe *> x: _outerpair.second) {
			Messreihe * tmp = x.second->copy();
			ci->mapping[_outerpair.first].insert(
				std::pair<std::string, Messreihe *>(x.first, tmp));
		}
	unlock("CI:copy end", false);
	return ci;
}



void ClientInfo::
print (bool onlySignificant, uint16_t maxPerStation) {
	std::string llive[2] = {"(mean values)", "(live data)"};
	printf("\033[1;36mStation '%s' (tx: %d.%.2d dBm) %s:\033[0m\n", 
		smac.c_str(), txpower / 100, txpower % 100, llive[isLiveData].c_str());
	if (anchorState == CI_AN_YES)
		printf("\033[1;36mPosition: (%f, %f)\033[0m\n", lat, lon);
	else if (anchorState == CI_AN_UNKNOWN)
		printf("\033[1;36mPosition: nicht nachgedacht\033[0m\n");
	else
		printf("\033[1;36mPosition: unbekannt\033[0m\n");
	
	time_t stamp = lastSeen;
	if (lastSeen == 0)
		printf("\033[1;36mnoch keine Daten erhoben\033[0m\n");
	else
		printf("\033[1;36mZuletzt Daten erhalten: (%u) %s\033[0m",lastSeen, asctime(localtime(&stamp))	);

	for (auto _outerpair : mapping)
		for (std::pair<std::string, Messreihe *> x: _outerpair.second){
			int cnt = x.second->count();
			// ignore connections with too less measurements
			if (onlySignificant && !x.second->isSignificant())
				continue;
			// convert mac address
			printf("\tblk %u: Messreihe ( -> '%s') mit %d Eintraegen:\n",
				_outerpair.first, x.first.c_str(), cnt);
			x.second->printPairs(maxPerStation);
		}
	printf("\033[1;36m=========\033[0m\n");

}


// ------------------ (private member)
// 


void ClientInfo::
lock (const char * reason, bool silent) {
	if (!silent)
		printf("\033[0;31mLock CI\033[0m %s\n", reason );
	mtx.lock();
}

void ClientInfo::
unlock (const char * reason, bool silent) {
	mtx.unlock();
	if (!silent)
		printf("\033[0;32mUnlock CI\033[0m %s\n", reason );
}
