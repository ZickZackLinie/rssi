#ifndef _NMDS_H
#define _NMDS_H

#include <vector>
#include <inttypes.h> // for uint64_t and so on

double gaussrand (double sigma = 1.0, double mue = 0.0);

class NMDS {
private:
	double threshold = 0;
	size_t elems = 0;
	std::vector< std::vector<double> > dists;
	std::vector< std::vector<double> > disparities;
	std::vector< std::vector<double> > dissimilarities;// { {d,i,j}, {d,i,j} ...} (i<j!)
	std::vector< std::vector<double> > coordinates; // { {x,y}, {x,y} ...}
	std::vector< std::vector<bool> > valid;
	std::vector<bool> anchors;
public:
	NMDS ();
	void init (std::vector< std::vector<double> > &coords);
	int initFromBinFile (std::vector<uint8_t> &datastream);
	int initFromTxtFile (std::string &datastream);

	void printDists ();
	void printDispars ();
	void printCoords ();
	void printDissimilarities ();
	void printRelatives ();
	
	void setAnchor(int i);
	void setDissimilarities (int i, int j, double d);

	double calcDistance (double X1, double Y1, double X2, double Y2);
	void calcDistances ();
	void calcDisparities ();

	void doRanking();
	void doRound();

	double getStress1();
	double getStress2();
};

#endif