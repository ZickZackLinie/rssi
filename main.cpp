

// Main software:
// Shoud be independent of being a server or a client
#include <unistd.h> // time_t and time(int)
#include <thread>

#include <utils.h>
#include <netlinkconnector.h>
#include <verbindungspool.h>
#include <control.h>
#include <udpbuffer.h>
#include <packet.h>

int main(int argc, char *argv[]) {

	// [option parsing]
	//	should a ncurse terminal be displayed
	//	...
	if ( !Options::parseOptions(argc, argv))
		return 0; // got some unknown args

	// CALC max usable memory
	unsigned int required = (unsigned int)-1;
	char *mem = NULL;
	while (mem == NULL) {
		//printf("Required %X\n", required);
		mem = (char *)malloc (required);
		required >>= 1;
	}
	free (mem);
	printf("==========================================================\n");
	printf("max memory %X = %u bytes\n", required, required);
	time_t zeit = required/5;
	printf("equal to %'u samples\n", required/5);
	printf("equal to max (best) %s runtime\n\n", calcStrDuration(zeit).c_str());

	required = PKT_MAX_SPLIT * PKT_MAX_SIZE;
	printf("max packet usable data: %u * %uB = %u bytes!\n",
		PKT_MAX_SPLIT,
		PKT_MAX_SIZE,
		required);
	zeit = required/5;
	printf("equal to max (best) %s runtime\n", calcStrDuration(zeit).c_str());
	printf("==========================================================\n");
	
	int ret = Control::setup();
	//printf("MACS ist (von MAIN aus) an %p\n", &Options::MACS);
	if (ret != 0) {
		return 1;
	}


	// [get data]
	//	start to poll for rssi data via netlink
	//	save in background
	sleep(1);
	//Control::wnd_start();
	
	while (Control::startNetworking() == 0) {
		printf("RUNDE IST RUM\nRESTART\n");
	}
	printf("Jetzt ist ENDE\n");
	
	// ======
	//
	// DO NETWORKING ROUNDS:
	//  while (startNetworking() == 0){
	//    // das ist NACH einer fertigen runde
	//  }
	//  
	// IF READY, THEN TERMINATE
	//
	// ======

	//Control::wnd_stop();
	VerbindungsPool::print(false, 10);
	VerbindungsPool::buildGraph();

	printf("loesche mich und die anderen\n");
	VerbindungsPool::cleanUp(VP_RM_CIALL);
	


	// meanwhile..........
	// [master check]
	//	is there any prior master
	//	or shall I collect others data


	// [end]
	return 0;

}