//#include "netlink/netlink.h"
//#include "netlink/genl/genl.h"
#include <arpa/inet.h>
#include <sys/types.h>
#include <cstring> // strchr
#include <cstdio> // getchar
#include <ctime> // printing time duration
#include <fstream> // reading macs file
#include <vector> // mac addresses from file

#include <utils.h>

//std::vector<std::string> MACS;



int cmp_itf (	unsigned char * left,
				unsigned char * right) {

	/*printf("left: ");
	for (int j = 0; j< ETH_ALEN; j++)
		printf("%02x ", left[j]);
	printf(" --  ");
	for (int j = 0; j< ETH_ALEN; j++)
		printf("%02x ", right[j]);
	printf(" right\n");*/

	int i;
	for (i = 0; i< ETH_ALEN ; i++) {
		if (left[i] == right[i]) {
			//printf(" %02x", left[i]);
			continue;
		}
		//printf(" (%02x!=%02x)\n", left[i], right[i] );
		return (left[i] < right[i] ? -1 : 1);
	}
	//printf("\n");
	return 0;
}

int mac_addr_n2a (	char *			mac_addr,
					unsigned char *	arg ) {
	int i, l;

	l = 0;
	for (i = 0; i< ETH_ALEN ; i++) {
		if (i == 0) {
			sprintf(mac_addr+l, "%02x", arg[i]);
			l += 2;
		} else {
			sprintf(mac_addr+l, ":%02x", arg[i]);
			l += 3;
		}
	}
	return 0;
}
// mac address string to six bytes array
// out mac_addr
// in arg (string xx:xx:xx:xx:xx:xx)
// return 0 iff all okay
int mac_addr_a2n (	unsigned char *	mac_addr,
					char *			arg ) {
	unsigned int xa, xb, xc, xd, xe, xf;
	if ( 6 == sscanf(arg, "%x:%x:%x:%x:%x:%x%*s",
		&xa, &xb, &xc, &xd, &xe, &xf) ) {
		mac_addr[0] = (uint8_t) xa;
		mac_addr[1] = (uint8_t) xb;
		mac_addr[2] = (uint8_t) xc;
		mac_addr[3] = (uint8_t) xd;
		mac_addr[4] = (uint8_t) xe;
		mac_addr[5] = (uint8_t) xf;
		return 0;
	} else return -1;
}




// read file with mac addresses (1 per line)
// fills vector
int read_mac_file(std::vector<std::string> &macs){
	std::ifstream file;
	file.open (QUOTE(FILE_MACS));
	int num = 0;
	if (!file.is_open()) return -1;

	std::string mac;
	while (file >> mac){
		if (mac[0] != '#'){
			macs.push_back(mac);
			num ++;
		}
	}
	file.close();
	return num;
}


// traverse all mac addresses known
// return index of first match
// or -1 if not found
int get_num_of_mac(std::string mac) {
	unsigned char umac[ETH_ALEN];
	if( mac_addr_a2n(umac, (char *)mac.c_str()) == 0)
		return get_num_of_mac(umac);
	return -1;
}
int get_num_of_mac(unsigned char * mac) {
	// compare mac address with every mac address from file of known addresses:
	unsigned xi = 0;
	std::vector<std::string>::size_type sz = Options::MACS.size();
	for ( ; xi < sz; xi++) {
		std::string stmp = Options::MACS[xi];
		unsigned char tmpmac[ETH_ALEN];
		mac_addr_a2n(tmpmac, (char *)stmp.c_str()); // str to byte array

		if (cmp_itf(tmpmac, mac) == 0)
			return xi;
		//printf("comparator sagt: %d\n", (cmp_itf(tmpmac, mac) == 0));
	}
	return -1;
}


struct in_addr hostToIP (uint8_t host) {
	struct in_addr sin_addr ;
	if (host == 0) {
		inet_aton("192.168.2.255", &sin_addr);
	} else {
		char ipaddress[16] = {0};
		sprintf(ipaddress, "192.168.2.%u", host);
		inet_aton(ipaddress, &sin_addr);
	}
	return sin_addr;
}


// create directory 'absDir'
int doCreateDir(std::string absDir){
	std::string command = "mkdir -p " + absDir;
	int ret = system(command.c_str());
	printf("'mkdir -p %s' ausgefuehrt mit returncode %d\n", absDir.c_str(), ret);
	return ret;
}


// calculating string for duration from seconds
std::string calcStrDuration (time_t seconds) {
	std::string ret = "";
	char buffer [50];
	struct tm * timeinfo = gmtime(&seconds);
	if (timeinfo->tm_year > 70) ret += std::to_string(timeinfo->tm_year-70) + " years, ";
	sprintf(buffer, "%u days and %02u:%02u:%02u", 
		timeinfo->tm_yday,
		timeinfo->tm_hour,
		timeinfo->tm_min,
		timeinfo->tm_sec);
	ret += std::string(buffer);
	return ret;
}


// letters to lowercase
std::string toLower (std::string in) {
	std::string ret = "";
	for (unsigned char c : in) {
		if ((65 <= c) && (c <= 90))
			c += 32;
		ret += c;
	}
	return ret;
}


float Options::latitude = 0.0f;
float Options::longitude = 0.0f;
std::string Options::smac_address = ""; // "xx:xx:xx:xx:xx:xx";
int Options::hostnum = -1;
uint8_t Options::anchorState = CI_AN_UNKNOWN;
bool Options::useWindow = false;
bool Options::handleNTP = false; // do the bat-adv system calls or not
bool Options::handleBatAdv = false; // do the bat-adv system calls or not
bool Options::listenAllIPs = false; // bind recvBC on ip or on all
bool Options::useASCIIFileOut = false; // VP:buildGraph writes a txt-file
bool Options::useBINFileOut = false; // VP:buildGraph writes a bin-file
std::string Options::curPrefix = "";//"test"; // name of subdir
std::string Options::usePathForOutFile = QUOTE(DIR_QUICKSAVE);//"../examples"; // path: VP:buildGraph
std::vector<std::string> Options::MACS;
bool Options::
parseOptions (int argc, char *argv[]) {
	read_mac_file(MACS);
	//printf("Lese %d Mac Adressen\n", read_mac_file(MACS));
	
	if (argc == 1)
		return true; // no options given
	for (int i = 1; i < argc; i++) {
		if (std::string(argv[i]) == "mac"){
			smac_address = std::string(argv[i+1]);
			i++;
		} else if (std::string(argv[i]) == "host"){
			hostnum = atoi(argv[i+1]);
			i++;
		} else if (std::string(argv[i]) == "pos") {
			latitude = atof(argv[i+1]);
			longitude = atof(argv[i+2]);
			anchorState = CI_AN_YES;
			i += 2;
		//} else if (std::string(argv[i]) == "win") {
		//	useWindow = true;
		} else if (std::string(argv[i]) == "ntp") {
			handleNTP = true;
		} else if (std::string(argv[i]) == "bat") {
			handleBatAdv = true;
		} else if (std::string(argv[i]) == "all") {
			listenAllIPs = true;
		} else if (std::string(argv[i]) == "txt") {
			listenAllIPs = true;
		} else if (std::string(argv[i]) == "bin") {
			listenAllIPs = true;
		} else if (std::string(argv[i]) == "path") {
			usePathForOutFile = std::string(argv[i+1]);
			i++;
		} else {
			printf("usage: %s [<args>]\n", argv[0]);
			printf("<args> could be:\n");
			//printf("\twin\tdisplay ncurses window (not working)\n");
			printf("\tbat\tstarts and stops batman-adv on interface\n");
			printf("\tall\tUDP listens on all interfaces\n");
			printf("\ttxt\tafter all save graph in txt-file\n");
			printf("\tbin\tafter all save graph in bin-file\n");
			printf("\tpath <path-to-dir>\n\t\t"
						"use dir to output txt|bin-files (txt|bin needed)\n");
			printf("\tpos <latitude> <longitude>\n\t\t"
						"assume own position as known at these coordinates\n");
			printf("\tmac <mac address>\n\t\t"
						"use this mac address for communication\n"
						"\t\t(separated by : )\n\n");
			printf("\thost <free hostnumber 1..254>\n\t\t"
						"use this host number for communication\n"
						"\t\tneeded if mac given but not in macfile\n\n");
			
			printf("example 1: '%s bat mac C4:e9:84:1D:55:cF all'\n", argv[0]);
			printf("\t\tThis will try to find itf with given mac.\n");
			printf("\t\tIf mac not found, cancel program,\n");
			printf("\t\tif mac found: start batman-adv on it,\n");
			printf("\t\tbut the UDP listener is bound\n"
					"\t\tto <any> interface (all) anyway.\n");
			printf("\t\tAfter end batman-adv will be stopped.\n\n");

			printf("example 2: '%s bat mac 02:12:34:56:78:9a host 4'\n", argv[0]);
			printf("\t\tThis sets up batman-adv and tries to find mac.\n");
			printf("\t\tIf mac is not in macfile (so unknown) the host\n");
			printf("\t\tis used to build ip address to listen on.\n"
					"\t\tIP address will be 192.168.2.host -> ...4\n\n");

			printf("example 3: '%s txt path /my/out/dir bin'\n", argv[0]);
			printf("\t\tThis won't set up batman-adv and UDP is only\n");
			printf("\t\tlistening on one ip and broadcast address.\n");
			printf("\t\tIf an output is generated, a .txt file AND\n"
					"\t\ta .bin file is used (bin first always).\n");
			printf("\t\tFiles will be /my/out/dir/out<timeinsecs>.(txt|bin)\n");			
			
			return false;
		}
	}
	return true;
}
