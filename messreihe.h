
#ifndef _MESSREIHE_H_
#define _MESSREIHE_H_

// Ein Interface zum Verarbeiten der gemessenen
// RSSI-Werte mit zugehoerigen Zeiten.
// Eine Messreihe sollte an eine Station (MAC)
// gekoppelt sein.

#include <cstring>
#include <vector>
#include <utility>
#include <queue>

#include <utils.h>

class Messreihe {

protected:
	int anzahl = 0;
	double mean = 0;
	//int meanWeight = 4;
	bool isLiveData = 1;
	int maxData = MAX_DATA_KEEP;
	int numToSignificant = NUM_TO_SIGNIFICANT;
	std::vector<std::pair< mtime_t, uint8_t> > pairs;

public:
	Messreihe(int maxDataKeep);
	//~SplMessreihe();
	int count();
	bool isSignificant ();
	void makeMeanOnly ();
	void makeLiveData ();
	int setMaxKeep (int maxNum);

	void insert(mtime_t time, uint8_t value);
	void merge(Messreihe * other);

	// CALCULATE mean value 
	double setMean();
	// GET mean value 
	double getMean();
	// shrink value pairs to the last "num"
	int shrinkToLast(int num);
	// delete every pair older than seconds
	int shrinkToNewerThan(mtime_t timeInSeconds);
	// 
	int serializeInto(std::vector<uint8_t> &vecDest, uint16_t maxNum);
	std::string serializeComma();
	std::string serializeGnuplot();
	std::vector<int> getHistogram(int from, int to);
	Messreihe * copy ();

	// debug values
	void printPairs(uint16_t maxNum);
};


#endif
