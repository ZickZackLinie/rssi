#include <netlink/socket.h>
#include "netlink/netlink.h"
#include "netlink/genl/genl.h"
#include "netlink/genl/ctrl.h"
#include <net/if.h>
#include <cstdio> // getchar
#include <fstream> // reading macs file
#include <cstring> // strncpy
#include <vector> // mac addresses from file

#include <unistd.h>	// ..., usleep

// copy this from iw
#include "nl80211.h"
#include <netlinkconnector.h>
#include <verbindungspool.h>
#include <utils.h>

bool NetlinkConnector::sucheNurFavorit = false;
int NetlinkConnector::usedMacIdx = -1;
int NetlinkConnector::itfidx = -1;
int NetlinkConnector::expectedId = 0;
int NetlinkConnector::lastStasFound = 0;
unsigned int NetlinkConnector::lastAnswerSeq = 0;
uint32_t NetlinkConnector::txpower = 0;
std::string NetlinkConnector::name; // wlan0 or similar
std::string NetlinkConnector::desiredMac = "";
unsigned char NetlinkConnector::mac[ETH_ALEN];
uint16_t NetlinkConnector::currentLevel = 0;
uint16_t NetlinkConnector::currentRound = 0;

// forward-declaration from netlink-private/types.h
struct nl_sock
{
	struct sockaddr_nl	s_local;
	struct sockaddr_nl	s_peer;
	int					s_fd;
	int 				s_proto;
	unsigned int 		s_seq_next;
	unsigned int 		s_seq_expect;
	int 				s_flags;
	struct nl_cb *		s_cb;
	size_t 				s_bufsize;
};

NetlinkConnector::NetlinkConnector () {
	
	//allocate socket
	sk = nl_socket_alloc();

	// connect to generic netlink
	genl_connect(sk);

	// find the nl80211 driver ID
	expectedId = genl_ctrl_resolve(sk, "nl80211");
	
	nl_socket_modify_cb(sk, NL_CB_FINISH, NL_CB_CUSTOM,
		NetlinkConnector::msgFinish,NULL);
}

// starts a new RSSI-scan
int NetlinkConnector::
lookForRssi (uint16_t level) {
	if (! hasValidItf()) {
		printf("NC: kein gueltiges Interface (Idx %d)\n", itfidx);
		return -2;
	}
	if (currentLevel != level)
		currentRound = 0;
	currentLevel = level;
	// send message over netlink, get #answered stations
	int stas = dumpRSSI(sk, itfidx);
	currentRound++;
	return stas;
}

// starts a new scan for available interface
bool NetlinkConnector::
lookForMac (const char * macstring) {
	if (macstring != nullptr) {
		desiredMac = std::string(macstring);
		sucheNurFavorit = true;
	} else {
		desiredMac = "";
		usedMacIdx = -1;
		sucheNurFavorit = false;
	}
	return dumpInterfaces(sk);
}

unsigned char * NetlinkConnector::
getItfMac () {
	return mac;
}

const char * NetlinkConnector::
getItfName () {
	return name.c_str();
}

bool NetlinkConnector::
hasValidItf () {
	return ( ! (itfidx < 0) );
}

// antenna power of last found interface (* 100)
uint32_t NetlinkConnector::
getTxPower () {
	return txpower;
}

// set up netlink for callback print_iface_handler
// prints short information about every wifi interface available
bool NetlinkConnector::
dumpInterfaces (nl_sock* sk){
	//attach a callback
	nl_socket_modify_cb(sk, NL_CB_VALID, NL_CB_CUSTOM, 
		NetlinkConnector::print_iface_handler, NULL);
	
	//allocate a message
	nl_msg* msg = nlmsg_alloc();

	nl80211_commands cmd = NL80211_CMD_GET_INTERFACE;
	// zeige fuer alle
	int flags = NLM_F_DUMP;
	// setup the message
	genlmsg_put(msg, 0, 0, expectedId, 0, flags, cmd, 0);
	// add message attributes
	// -> keine noetig.
	// send the message (this frees it)
	nl_send_auto(sk, msg);
	//block for message to return
	nl_recvmsgs_default(sk);
	// THEORETICAL PLACE TO LOOK UP RESULT...
	return hasValidItf();
}

// set up netlink for callback print_sta_handler
// prints information about every connected stations connection
// use 'wlan0' if usedItfIdx is 0
// returns -1 in case of error
// returns number of Stations found
int NetlinkConnector::
dumpRSSI(nl_sock* sk, int usedItfIdx){
	//printf("  <Starte Suchlauf nach RSSI ItfIdx:%d>\n", usedItfIdx);
	//attach a callback
	nl_socket_modify_cb(sk, NL_CB_VALID, NL_CB_CUSTOM, 
		NetlinkConnector::print_sta_handler, NULL);

	//printf("NEXT SEQ = %u\n", sk->s_seq_next);
	
	//allocate a message
	nl_msg* msg = nlmsg_alloc();

	nl80211_commands cmd = NL80211_CMD_GET_STATION;

	int ifIndex = (usedItfIdx == 0 ? if_nametoindex("wlan0") : usedItfIdx);
	
	// show all you can find
	int flags = NLM_F_DUMP;
	// reset station counter
	lastStasFound = 0;
	// setup the message
	genlmsg_put(msg, 0, 0, expectedId, 0, flags, cmd, 0);
	// add message attributes
	//NLA_PUT(msg, NL80211_ATTR_MAC, ETH_ALEN, mac_addr);
	NLA_PUT_U32(msg, NL80211_ATTR_IFINDEX, ifIndex);
	// send the message (this frees it)
	nl_send_auto(sk, msg);
	//block for message to return
	nl_recvmsgs_default(sk);
	// THEORETICAL PLACE TO LOOK UP RESULT...
	return lastStasFound;
	nla_put_failure:
	printf("dump rssi failure end\n");
	nlmsg_free(msg);

	return -1;
}



// callback to parse station
int NetlinkConnector::
print_sta_handler(struct nl_msg *msg, void *arg)
{
	struct nlmsghdr* ret_hdr = nlmsg_hdr(msg);
	lastAnswerSeq = ret_hdr->nlmsg_seq;
	struct nlattr *tb[NL80211_ATTR_MAX + 1];
	
	struct genlmsghdr *gnlh = (struct genlmsghdr*) nlmsg_data(ret_hdr);
	struct nlattr *sinfo[NL80211_STA_INFO_MAX + 1];
	char mac_addr[20], dev[20];
	static struct nla_policy stats_policy[NL80211_STA_INFO_MAX + 1];
	stats_policy[NL80211_STA_INFO_INACTIVE_TIME] = { type: NLA_U32 };
	stats_policy[NL80211_STA_INFO_SIGNAL] = { type: NLA_U8 };

	nla_parse(tb, NL80211_ATTR_MAX, genlmsg_attrdata(gnlh, 0),
		  genlmsg_attrlen(gnlh, 0), NULL);

	if (!tb[NL80211_ATTR_STA_INFO]) {
		fprintf(stderr, "sta stats missing!\n");
		return NL_OK;
	}
	if (nla_parse_nested(sinfo, NL80211_STA_INFO_MAX,
			     tb[NL80211_ATTR_STA_INFO],
			     stats_policy)) {
		fprintf(stderr, "failed to parse nested attributes!\n");
		return NL_SKIP;
	}

	mtime_t result_time = time(0);
	mtime_t inactive = 0;
	uint8_t result_rssi = 0;

	mac_addr_n2a(mac_addr, (unsigned char *)nla_data(tb[NL80211_ATTR_MAC]));
	if_indextoname(nla_get_u32(tb[NL80211_ATTR_IFINDEX]), dev);
	//printf("Station %s (on %s)", mac_addr, dev);

	if (sinfo[NL80211_STA_INFO_INACTIVE_TIME]){
		inactive = nla_get_u32(sinfo[NL80211_STA_INFO_INACTIVE_TIME]) / 1000;
		//printf("\n\tinactive time:\t%u s",	inactive);
	}
	//if (sinfo[NL80211_STA_INFO_SIGNAL])
	//	printf("\n\tsignal:  \t%d dBm",
	//		(int8_t)nla_get_u8(sinfo[NL80211_STA_INFO_SIGNAL]) );

	if (sinfo[NL80211_STA_INFO_SIGNAL]) {//_AVG]) {
		int8_t tmpsig = (int8_t)nla_get_u8(sinfo[NL80211_STA_INFO_SIGNAL]);//_AVG]);
		if (inactive > TM_MAX_INACTIVE) 
			result_rssi = 0;
		else
			result_rssi = -tmpsig;
		//printf("\n\tsignal:\t%d dBm", tmpsig );
	}
	//if (sinfo[NL80211_STA_INFO_CONNECTED_TIME])
	//	printf("\n\tconnected time:\t%u seconds",
	//		nla_get_u32(sinfo[NL80211_STA_INFO_CONNECTED_TIME]));

	//printf("\n");

	// DONT USE TIME, use rounds instead
	result_time = currentRound;
	int ret = VerbindungsPool::appendToMe(
		currentLevel, mac_addr, result_time, result_rssi);
	//printf("\ttime: %u, signal:\t%d dBm\n", result_time, result_rssi);
	if (ret < 0) {
		printf("Verbindung konnte nicht eingetragen werden (ret = %d)\n", ret);
	}
	lastStasFound++;
	return NL_OK;
}


// cut from iw/interface.c
int NetlinkConnector::
print_iface_handler(struct nl_msg *msg, void *arg)
{
	//printf("print_iface_handler beginnt\n");
	
	struct nlmsghdr* ret_hdr = nlmsg_hdr(msg);
	lastAnswerSeq = ret_hdr->nlmsg_seq;
	struct genlmsghdr *gnlh = (struct genlmsghdr*) nlmsg_data(ret_hdr);
	struct nlattr *tb_msg[NL80211_ATTR_MAX + 1];
	const char *indent = "";

	nla_parse(tb_msg, NL80211_ATTR_MAX, genlmsg_attrdata(gnlh, 0),
		  genlmsg_attrlen(gnlh, 0), NULL);

	std::string tmpname = ""; // wlan0 or similar
	if (tb_msg[NL80211_ATTR_IFNAME]) {
		tmpname = nla_get_string(tb_msg[NL80211_ATTR_IFNAME]);
		printf("%sInterface %s\n", indent, tmpname.c_str());
	} else {
		return NL_OK;
	}
	
	int idx = 0;
	if (tb_msg[NL80211_ATTR_IFINDEX]) {
		idx = nla_get_u32(tb_msg[NL80211_ATTR_IFINDEX]);
		//printf("%s\tifindex %d\n", indent, idx);
	}
	uint32_t tmptxpwr = 0;
	if (tb_msg[NL80211_ATTR_WIPHY_TX_POWER_LEVEL]) {
		uint32_t txp = nla_get_u32(tb_msg[NL80211_ATTR_WIPHY_TX_POWER_LEVEL]);

		printf("%s\ttxpower %d.%.2d dBm\n",
		       indent, txp / 100, txp % 100);
		tmptxpwr = txp;
	}
	if (tb_msg[NL80211_ATTR_MAC]) {
		char mac_addr[20];
		unsigned char * data = (unsigned char*) nla_data(tb_msg[NL80211_ATTR_MAC]);
		mac_addr_n2a(mac_addr, data);
		
		if ( ! sucheNurFavorit) {
			// compare mac address with every mac address from file of known addresses:
			int tmpMacIdx = get_num_of_mac(data);
			// 1) must not be -1 (-1 means not known) and
			// 2.1) there is no Itf before, so take it or
			// 2.2) the Itf before is worse than this, take this
			if (  (tmpMacIdx != -1) && 
				  ((usedMacIdx == -1) || (usedMacIdx > tmpMacIdx)) ) {
				printf("%s\taddr %s bekannt (%d)\n",indent, mac_addr, (tmpMacIdx+1));
				usedMacIdx = tmpMacIdx;
				itfidx = idx; // merke itf index (nimm erstes bekanntes interface)
				name = tmpname;
				txpower = tmptxpwr;
				memcpy(mac, data, ETH_ALEN); // save mac addr.
			} else {
				printf("%s\taddr %s nicht in liste\n", indent, mac_addr);
			}
		} else {
			// suche nur favorit (desiredMac != "")
			char smac_addr_desired[ETH_CLEN] = {0};
			memcpy(smac_addr_desired, desiredMac.c_str(), ETH_CLEN-1);
			unsigned char bmac_addr_desired[ETH_ALEN] = {0};
			mac_addr_a2n(bmac_addr_desired, smac_addr_desired);
			if (cmp_itf(bmac_addr_desired, data) == 0) {
				itfidx = idx; // merke itf index 
				name = tmpname;
				txpower = tmptxpwr;
				memcpy(mac, data, ETH_ALEN); // save mac addr.
				printf("%s\taddr %s bevorzugt\n",indent, mac_addr);
			} else {
				printf("%s\taddr %s nicht gefordert\n",indent, mac_addr);
			}
		}
	}

	return NL_OK;//NL_SKIP
}


int NetlinkConnector::
msgFinish(struct nl_msg *msg, void *arg)
{
	struct nlmsghdr* ret_hdr = nlmsg_hdr(msg);
	//printf("(MSG %u finished)\n", ret_hdr->nlmsg_seq);
	if (lastAnswerSeq != ret_hdr->nlmsg_seq) {
		//printf("( - nothing found - )\n");
	}
	return NL_STOP;
}
