#include <messreihe.h>
#include <iostream>
#include <cstring>
#include <vector>

// use:
	/*Messreihe * pMessreihe = new Messreihe();
	printf("Anzahl Elemente: %d.\n", pMessreihe->count());
	pMessreihe->insert(100,53);
	pMessreihe->insert(110,54);
	pMessreihe->insert(115,55);
	pMessreihe->insert(120,0);
	pMessreihe->insert(130,-42);
	*/

Messreihe::
Messreihe(int maxDataKeep){
	if (maxDataKeep != 0)
		maxData = maxDataKeep;
	/*std::cout 	<< "inhalte: " << times.size() << ", "
				<< values.size() << "\n";*/
}

/*Messreihe::
~Messreihe(){
	std::cout 	<< "ciao !\n";
}*/

int Messreihe::
count(){
	return anzahl;
}

bool Messreihe::
isSignificant () {
	if (	(isLiveData && (anzahl >= numToSignificant) )
		|| 	((!isLiveData) && (anzahl >= 1)) )
		return true;
	return false;
}

void Messreihe::
makeMeanOnly () {
	isLiveData = false;
	setMaxKeep(1);
	setMean();
}

void Messreihe::
makeLiveData () {
	isLiveData = true;
}
	
int Messreihe::
setMaxKeep (int maxNum){
	// no live data - mean only
	if (! isLiveData)
		maxNum = 1;
	else if (maxNum < numToSignificant) // otherwise on live data..
		maxNum = numToSignificant;
	maxData = maxNum;
	if (anzahl > maxData) // resize
		shrinkToLast(maxData);
	return maxNum;
}

void Messreihe::
insert(mtime_t time, uint8_t value){
	/*if (!isLiveData) {
		// calc mean instantly
		double v = value;
		mean = ((meanWeight * mean) + v) / (meanWeight + 1);
	}*/
	pairs.push_back(std::make_pair(time, value));
	anzahl++;
	if (anzahl > maxData) // resize
		shrinkToLast(maxData);
	if (!isLiveData)
		setMean();
}

void Messreihe::
merge(Messreihe * other) {
	// my and others data
	size_t szmy = pairs.size();
	size_t szot = other->pairs.size();
	size_t ptrm = 0;
	size_t ptro = 0;

	while (1) { // one operation each step
		if (ptro == szot)
			break; // alle drin - keine neuen werte
		if (ptrm == szmy) {
			// nur noch neue werte da - stur uebernehmen
			pairs.insert(pairs.end(),
				other->pairs.begin()+ptro,
				other->pairs.begin()+szot);
			break;
		}
		if (pairs[ptrm].first < other->pairs[ptro].first){
			ptrm ++; // move forward
		} else if (pairs[ptrm].first == other->pairs[ptro].first) {
			ptro ++; // ignore identical values
		} else {
			// take value
			pairs.insert(pairs.begin()+ptrm, other->pairs[ptro]);
			ptrm++;
			szmy++;
			ptro++;
		}
	}

	anzahl = pairs.size();
	if (anzahl > maxData) // resize
		shrinkToLast(maxData);
	setMean();
}

double Messreihe::
setMean() {
	int sum = 0;
	for(auto valpair: pairs){
		sum += valpair.second;
	}
	mean = sum/anzahl;
	return mean;
}

double Messreihe::
getMean() {
	return mean;
}

int Messreihe::
shrinkToLast (int num) {
	// minimum
	num = (num > anzahl) ? anzahl : num;
	int todel = anzahl - num;
	pairs.erase(pairs.begin(), pairs.begin()+todel);
	anzahl -= todel;
	return anzahl; // how many really left?
}

int Messreihe::
shrinkToNewerThan (mtime_t timeInSeconds) {
	std::vector<std::pair< mtime_t, uint8_t> > tmppairs;
	for( std::pair< mtime_t, uint8_t> valpair: pairs){
		if (valpair.first > timeInSeconds)
			tmppairs.push_back(
				std::make_pair(valpair.first, valpair.second) );
	}
	pairs.clear();
	pairs.assign(tmppairs.begin(),tmppairs.end());
	anzahl = pairs.size();
	return anzahl;
}


int Messreihe::
serializeInto (std::vector<uint8_t> &vecDest, uint16_t maxNum) {

	// using MEAN VALUES
	if ((maxNum == 1) && (anzahl > 0)) {
		// calculate mean value
		uint8_t m = setMean();
		mtime_t t = time(0);
		hton_vec_any(maxNum, vecDest); // save size
		hton_vec_any(t,vecDest);
		hton_vec_any(m,vecDest);
		printf("berechne MEAN\n");
		return 1;
	} else if (maxNum == 1) {
		// get last known mean value (can be zero)
		uint8_t m = getMean();
		mtime_t t = 0;
		hton_vec_any(maxNum, vecDest); // save size
		hton_vec_any(t,vecDest);
		hton_vec_any(m,vecDest);
		printf("verwende MEAN\n");
		return 1;
	}

	// using REAL VALUES
	if (anzahl < maxNum)
		maxNum = anzahl;
	hton_vec_any(maxNum, vecDest); // save size
	int i = 1;
	std::vector<std::pair< mtime_t, uint8_t> >::iterator it;
	for ( it = pairs.end()-maxNum; it != pairs.end() ; ++it) {
		/*int a = */hton_vec_any((*it).first, vecDest);
		/*int b = */hton_vec_any((*it).second, vecDest);
		//printf("%d/%u: brauche platz (%d (%u), %d (%hhu))\n",i, maxNum, a, (*it).first, b, (*it).second);
		i++;
	}
	printf("%d------------\n", (i-1));
	return maxNum;	
}


std::string Messreihe::
serializeComma () {
	std::string ret = "";
	int cnt = 1;
	//printf("\nBAUE MR:\n");
	std::vector<std::pair< mtime_t, uint8_t> >::iterator it;
	for ( it = pairs.begin(); it != pairs.end() ; ++it) {
		mtime_t ttime = (*it).first;
		int rssi = -(int)((*it).second);
		//printf("    ttime(%u), rssi(%d : %hhu)\n", ttime, rssi, (*it).second);
		ret.append(std::to_string(ttime));
		ret.append(",");
		ret.append(std::to_string(rssi));
		if (cnt < anzahl)
			ret.append(",");
		cnt++;
	}
	return ret;
}


std::string Messreihe::
serializeGnuplot () {
	std::string ret = "";
	int cnt = 1;
	//printf("\nBAUE MR:\n");
	std::vector<std::pair< mtime_t, uint8_t> >::iterator it;
	for ( it = pairs.begin(); it != pairs.end() ; ++it) {
		mtime_t ttime = (*it).first;
		int rssi = -(int)((*it).second);
		//printf("    ttime(%u), rssi(%d : %hhu)\n", ttime, rssi, (*it).second);
		ret.append(std::to_string(cnt-1));
		ret.append(" ");
		ret.append(std::to_string(rssi));
		if (cnt < anzahl)
			ret.append("\n");
		cnt++;
	}
	return ret;
}


std::vector<int> Messreihe::
getHistogram (int from, int to) {
	std::vector<int> ret (to+1,0);
	std::vector<std::pair< mtime_t, uint8_t> >::iterator it;
	for ( it = pairs.begin(); it != pairs.end() ; ++it) {
		int rssi = (int)((*it).second);
		if ((from <= rssi) && (rssi <= to))
			ret[rssi] ++;
	}
	return ret;
}


Messreihe * Messreihe::
copy () {
	Messreihe * ret = new Messreihe(maxData);
	std::vector<std::pair< mtime_t, uint8_t> > tmppairs;
	for( std::pair< mtime_t, uint8_t> valpair: pairs){
		tmppairs.push_back(	std::make_pair(valpair.first, valpair.second) );
	}
	ret->pairs = tmppairs;
	ret->anzahl = anzahl;
	ret->mean = mean;
	ret->isLiveData = isLiveData;
	ret->maxData = maxData;
	ret->numToSignificant = numToSignificant;
	return ret;
}


//---------------- private member
// used by insert
void Messreihe::
printPairs(uint16_t maxNum){

	// using MEAN VALUES
	if (maxNum == 1) {
		// get last known mean value (can be zero)
		uint8_t m = getMean();
		time_t k_stamp = 0;
		char timebuffer[80];
		strftime(timebuffer,80,"%T",localtime(&k_stamp));
		printf("\t\t(%s - %d)\n", 
			timebuffer, 
			m);
		return;
	}

	// using REAL VALUES
	if (anzahl < maxNum)
		maxNum = anzahl;
	std::vector<std::pair< mtime_t, uint8_t> >::iterator it;
	for ( it = pairs.end()-maxNum; it != pairs.end() ; ++it) {
		time_t k_stamp = (*it).first;
		char timebuffer[80];
		strftime(timebuffer,80,"%T",localtime(&k_stamp));
		printf("\t\t(%s - %d)\n", 
			timebuffer, 
			(*it).second);
	}
}
