#ifndef _UDPBUFFER_H_
#define _UDPBUFFER_H_

#include <packet.h>

#include <netinet/in.h>
#include <sys/socket.h>
#include <ctime>
#include <condition_variable>	// std::condition_variable, std::cv_status
#include <mutex>
#include <vector>
#include <queue>

#include <utils.h>

// when to remove packets in seconds
// should be greater than 2x TIMEOUTS
#define UDPB_DISCARD_TIME 10
// timeouts for get<X>Packet()
#define UDPB_TIMEOUT      1
// when to remove nodes in seconds
// only updated by incoming packets
// so be fair and choose high value
#define UDPB_TIME_RMNODE  120

// which lock to use
#define UDPB_BUFFER_IN  0
#define UDPB_BUFFER_OUT 1
#define UDPB_BUFFER_ACK 2

// state of UDPBuffer
#define UDPB_S_UNINIT   0 // completely shut down - nothing works
#define UDPB_S_STARTING 1 // don't take pkts, not initialized yet
#define UDPB_S_RUNNING  2 // take and send packets - normal
#define UDPB_S_SHUTDOWN 3 // don't take new pkts, send out old ones
#define UDPB_S_CLEANUP  4 // can't even send (dont access pkts!)
#define UDPB_S_OFF      5 // completely shut down - nothing works

class UDPBuffer {
private:

	int state = UDPB_S_OFF;
	std::mutex mtx_state_acc;
	void setState(int st); // blocking

	std::vector<std::pair<struct in_addr, mtime_t> > lastSeenList;

	int jobnr = 0;
	std::mutex mtx_job_acc;
	
	std::mutex mtx_in_acc;
	std::mutex mtx_in_notify;       // for cv_in
	std::condition_variable cv_in;  // mtx_in_notify
	std::vector<std::vector<Packet *> > inlist;
	
	std::mutex mtx_out_acc;
	std::mutex mtx_out_notify;      // for cv_out
	std::condition_variable cv_out; // mtx_out_notify
	std::vector<std::vector<Packet *> > outlist;
	
	std::mutex mtx_ack_acc;
	std::queue<Packet *> acklist;
	
	void lock(int mx);
	void unlock(int mx);

	// only lock, get, unlock, return
	// NEEDS DELETE ON RETURNED PACKET!!!
	Packet * findInPacket();  // nonblocking, can be null
	Packet * findOutPacket(); // nonblocking, can be null

	void setAckThis (Packet * ackPkt); // blocking
	Packet * makeAckPkt (Packet * inPkt);

public:
	UDPBuffer();

	int getState();          // blocking
	int getJobNum ();        // blocking

	// use condition vars
	// NEEDS DELETE ON RETURNED PACKET!!!
	Packet * getInPacket();  // blocking, can be null, data: 0..inf
	Packet * getOutPacket(); // blocking, can be null, data: 0..503

	// add packets
	void receivedThis(Packet * pkt);
	int sendThis(int type,
		std::vector<uint8_t> &data,
		struct in_addr target);
	void placeInBuffer (Packet * pkt, const int BUFFER);

	// false if no packet for jobnr found
	// true if ips got filled
	bool findAnswerIPs(int jobnr, std::vector<struct in_addr> &ips);

	// STATS ABOUT ALL PARTICIPANTS
	std::vector<struct in_addr> currentNodes();
	void gotSomethingFrom(struct in_addr node);


	// thread
	static void start_thread (UDPBuffer *pbu);
	void run_blocking ();
	void stop ();
};

#endif