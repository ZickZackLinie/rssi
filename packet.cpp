#include <packet.h>

#include <cstddef>
//#include <ctime>
#include <unistd.h> // time_t and time(int)
#include <arpa/inet.h> // inet_aton
#include <vector>
#include <cstring>

#include <utils.h> // datatypes and converting

Packet::
Packet (uint16_t jobnr,
		uint8_t ttype,
		uint8_t pktNum,
		uint8_t pktMax,
		struct in_addr tar) {
	job = jobnr;
	type = ttype;
	pktnum = pktNum;
	pktmax = pktMax;
	target = tar;
	ttime = time(0);
	//content.insert(content.begin(), datastream, datastream+numBytes);
}
Packet::
Packet (std::vector<uint8_t> &datastream) {
	size_t ptr = 0;
	if (datastream.size() >= 9) {
		// [type]    [1]
		ntoh_vec_any(type, datastream, ptr); ptr += sizeof(uint8_t);
		// [ttime]    [4]
		ntoh_vec_any(ttime, datastream, ptr); ptr += sizeof(mtime_t);
		// [job]     [2]
		ntoh_vec_any(job, datastream, ptr); ptr += sizeof(uint16_t);
		// [pktnum]  [1]
		ntoh_vec_any(pktnum, datastream, ptr); ptr += sizeof(uint8_t);
		// [pktmax]  [1]
		ntoh_vec_any(pktmax, datastream, ptr); ptr += sizeof(uint8_t);
		// [<data>]  [0..503]
		content.insert(content.begin(), 
			datastream.begin()+ptr, 
			datastream.end());
		ttime = time(0);
	}
}

std::vector<uint8_t> Packet::
make () {
	// [type]    [1]
	// [ttime]    [4]
	// [job]     [2]
	// [pktnum]  [1]
	// [pktmax]  [1]
	// [<data>]  [0..503]
	mtime_t ttime = time(0);
	std::vector<uint8_t> ret;
	hton_vec_any(type,   ret);
	hton_vec_any(ttime,  ret);
	hton_vec_any(job,    ret);
	hton_vec_any(pktnum, ret);
	hton_vec_any(pktmax, ret);
	ret.insert(ret.end(), content.begin(), content.end());
	return ret;
}

bool Packet::
shouldBeAcked () {
	switch (type) {
	case PKT_ACK:
	case PKT_RESEND:
	case PKT_TIMEDIFF:
	case PKT_TIMEDIFFME:
		return false;
	case PKT_UNKNOWN:
	case PKT_EXECCMD:
	case PKT_GETCIS:
	case PKT_TAKECIS:
	case PKT_RMCIS:
	case PKT_SETMAX:
//	case PKT_DATAOPEN:
	case PKT_DATAPRINT:
	case PKT_RMDATA:
	case PKT_DATASAVE:
//	case PKT_SEQHDLCRT:
//	case PKT_SEQHDLDEL:
	case PKT_PREFIX:
	case PKT_QUITALL:
	case PKT_SEQ_SETLVL:
	case PKT_SEQ_TERM:
	case PKT_TIMESYNC:
	default:
		return true;
	}
}

// is it a packet that needs instant answer?
bool Packet::
isTimeCritical () {
	switch (type) {
//	case PKT_SEQHDLCRT:
//	case PKT_SEQHDLDEL:
	case PKT_SEQ_SETLVL:
	case PKT_SEQ_TERM:
	case PKT_TIMEDIFF:
	case PKT_TIMEDIFFME:
		return true;
	case PKT_UNKNOWN:
	case PKT_ACK:
	case PKT_RESEND:
	case PKT_EXECCMD:
	case PKT_GETCIS:
	case PKT_TAKECIS:
	case PKT_RMCIS:
	case PKT_SETMAX:
//	case PKT_DATAOPEN:
	case PKT_DATAPRINT:
	case PKT_RMDATA:
	case PKT_DATASAVE:
	case PKT_PREFIX:
	case PKT_QUITALL:
	case PKT_TIMESYNC:
	default:
		return false;
	}
}

uint8_t Packet::
calcState (bool forIN, mtime_t oldestacceptable) {

	if (forIN == PKT_CS_FORIN) {

		if (reconstructed) {
			// already handled, wait for timeout
			// if it comes from sendBC (job == 0)
			// then delete ASAP
			if ((ttime < oldestacceptable) || (job == 0))
				return PKT_S_OVER;
			else
				return PKT_S_KEEP;
		} else {
			// unhandled, maybe complete packet
			return PKT_S_FRESH;
		}

	} else {

		// if unhandled
		if (cntsnd == 0)
			return PKT_S_FRESH;

		// if already sent
		if ((cntsnd > 0) && !shouldBeAcked()) {
			return PKT_S_OVER;

		} else if ((cntsnd > 0) && acked) {
			// IF IT IS BC, ONE ACK WILL DO THE STUFF TO
			// NOT RESEND THE WHOLE PACKET!
			// we get > 1 ack, wait for discard
			if (ttime < oldestacceptable)
				return PKT_S_OVER;
			else
				return PKT_S_KEEP;
		// not acked, shouldBeAcked, sent already:
		} else if (cntsnd <= PKT_MAX_RESEND) {
			// send again
			return PKT_S_FRESH;
		} else {
			// sent too much, not acked, shouldBeAcked
			if (ttime < oldestacceptable)
				return PKT_S_OVER;
			else
				return PKT_S_KEEP;
		}

	}
}


void Packet::
setContent (uint8_t * datastream, size_t numBytes) {
	content.assign (datastream, datastream + numBytes);
}

void Packet::
setContent (std::vector<uint8_t> datastream) {
	content = datastream;
}


std::string Packet::
strPktType(int type) {
	switch (type) {
	case PKT_UNKNOWN: return "UNKNOWN";
	case PKT_ACK: return "ACK";
	case PKT_RESEND: return "RESEND";
	case PKT_EXECCMD: return "EXECCMD";
	case PKT_GETCIS: return "GETCIS";
	case PKT_TAKECIS: return "TAKECIS";
	case PKT_RMCIS: return "RMCIS";
	case PKT_SETMAX: return "SETMAX";
//	case PKT_DATAOPEN:
	case PKT_DATAPRINT: return "DATAPRINT";
	case PKT_RMDATA: return "RMDATA";
	case PKT_DATASAVE: return "DATASAVE";
//	case PKT_SEQHDLCRT: return "SEQHDLCREATE";
//	case PKT_SEQHDLDEL: return "SEQHDLDELETE";
	case PKT_PREFIX: return "SETPREFIX";
	case PKT_QUITALL: return "QUITALL";
	case PKT_SEQ_SETLVL: return "SEQ_SET_LEVEL";
	case PKT_SEQ_TERM: return "SEQ_TERM";
	case PKT_TIMEDIFF: return "TIMEDIFF";
	case PKT_TIMEDIFFME: return "TIMEDIFFME";
	case PKT_TIMESYNC: return "TIMESYNC";
	default:
		return "---";
	}
}

void Packet::
printHdr(){
	std::string a[2] = {"not acked", "acked"};
	printf("Job(%d), Type(%d %s), %d/%d (%s) %s:%d [%zu]\033[0m\n",
		job, 
		type, strPktType(type).c_str(),
		pktnum, pktmax,
		a[acked].c_str(), inet_ntoa(target), port, content.size());
}

std::string Packet::
getHdr(){
	std::string a[2] = {"not acked", "acked"};
	std::string res = "";
	res += "Job(" + std::to_string(job) + "), ";
	res += "Type(" + std::to_string(type) + " " + strPktType(type).c_str() + "), ";
	res += std::to_string(pktnum) +"/"+ std::to_string(pktmax) + " ";
	res += "(" + a[acked] + ") ";
	res += inet_ntoa(target);
	res += ":"+ std::to_string(port) + " ";
	res += "[" + std::to_string(content.size()) +"]";
	return res;
}