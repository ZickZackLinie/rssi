#include <cstdio> // printf
#include <vector>
#include <map>
#include <cmath> // sqrt, pow
#include <cstring> // memset
#include <algorithm> // sort
#include <fstream> // printToFile()
#include <sstream> // initFromTxtFile()

#include <nmds.h>
#include <utils.h>

double gaussrand (double sigma, double mue) {
	double r = 0.0;
	for (int i = 0; i < 12; i++)
		r += rand() / (double) RAND_MAX;
	return mue + (r - 6.0) * sigma;
}



NMDS::
NMDS () {}



void NMDS::
init (std::vector< std::vector<double> > &coords) {
	int elements = coords.size();
	printf("%d elemente, %d platz fuer diss\n", elements, (elements*(elements-1))/2);
	dists = std::vector< std::vector<double> > 
		(elements, std::vector<double>(elements));
	disparities = std::vector< std::vector<double> > 
		(elements, std::vector<double>(elements));
	dissimilarities = std::vector< std::vector<double> > 
		((elements*(elements-1))/2, std::vector<double>(3));
	coordinates = coords;
	valid = std::vector< std::vector<bool> > 
		(elements, std::vector<bool>(elements, false));
	anchors = std::vector<bool>(elements, false);


	for (int i = 0; i < elements; i++) {
		for (int j = i+1; j < elements; j++) {
			int idx = (i*elements + j) - (i+1)*(i+2)/2;
			dissimilarities[idx][0] = 0;
			dissimilarities[idx][1] = i;
			dissimilarities[idx][2] = j;
		}
	}
	elems = elements;
	
}



int NMDS::
initFromBinFile (std::vector<uint8_t> &datastream) {
	size_t size = datastream.size();
	size_t ptr = 0;
	double X_min = 0, X_max = 0;
	double Y_min = 0, Y_max = 0;
	double X_mid = 0, Y_mid = 0;
	bool coordsInitialized = false;
	
	// size check #1:
	if (size < 3* sizeof(uint32_t))
		return -1;

	// header information
	uint32_t nNodes, nAnchors, nEdges;
	ntoh_vec_any(nNodes, 		datastream, ptr); ptr += sizeof(uint32_t);
	ntoh_vec_any(nAnchors, 		datastream, ptr); ptr += sizeof(uint32_t);
	ntoh_vec_any(nEdges, 		datastream, ptr); ptr += sizeof(uint32_t);
	
	// size check #2:
	size_t szHdr = 3*sizeof(uint32_t);
	size_t szNds = nNodes * sizeof(uint16_t);
	size_t szAns = nAnchors * (sizeof(uint16_t) + 2* sizeof(uint32_t));
	size_t szEgs = nEdges * (2*sizeof(uint16_t) + sizeof(uint32_t));
	size_t all = szHdr + szNds + szAns + szEgs;
	printf("Bytes: %u = %u + %u + %u + %u\n", all,szHdr,szNds,szAns,szEgs);
	
	if (size < all) {
		printf("ERROR: size(%u) < all(%u)\n", size, all);
		printf("---> : %u = %u + %u + %u + %u\n", all,szHdr,szNds,szAns,szEgs);
		return -2;
	}

	// plausibility check:
	if (nAnchors > nNodes)
		return -3;

	// content
	std::vector< std::vector<double> > tmp_coords =
		std::vector< std::vector<double> > 
		(nNodes, std::vector<double>(2));
	// node name resolution
	std::map<uint16_t,uint16_t> nameres;
	std::map<uint16_t,uint16_t> nameres_rev;
	std::vector<bool> tmpAnchors;

	for (uint16_t i = 0; i < nNodes; i++) {
		uint16_t node = 0;
		ntoh_vec_any(node, 	datastream, ptr); ptr += sizeof(uint16_t);
		nameres[node] = i;
		nameres_rev[i] = node;
		tmpAnchors.push_back(false);
	}
	for (uint16_t i = 0; i < nAnchors; i++) {
		uint16_t node = 0;
		ntoh_vec_any(node, 	datastream, ptr); ptr += sizeof(uint16_t);
		// [LAT] [LON] (position)	[4] [4]
		double X, Y;
		uint32_t nLat, nLng;
		ntoh_vec_any(nLat,	datastream, ptr); ptr += sizeof(uint32_t);
		ntoh_vec_any(nLng,	datastream, ptr); ptr += sizeof(uint32_t);
		X = nLng/100000.0;
		Y = nLat/100000.0;
		int idx = nameres[node];
		tmp_coords[idx][0] = X;
		tmp_coords[idx][1] = Y;
		tmpAnchors[idx] = true;
		printf("coords found: idx(%d), (%.4f,%.4f)\n", idx, X,Y);
		if (!coordsInitialized) {
			X_min = X;
			X_max = X;
			Y_min = Y;
			Y_max = Y;
			coordsInitialized = true;
		}
		X_min = (X_min > X ? X : X_min);
		X_max = (X_max < X ? X : X_max);
		Y_min = (Y_min > Y ? Y : Y_min);
		Y_max = (Y_max < Y ? Y : Y_max);
	}
	// coordinates midpoint
	X_mid = X_min + (X_max - X_min) / 2;
	Y_mid = Y_min + (Y_max - Y_min) / 2;

	// all unknown coordinates: place random
	std::map<uint16_t,uint16_t>::iterator it;
	for (uint16_t i = 0; i < nNodes; i++) {
		if ( !tmpAnchors[i] ) {
			// is not an anchor
			tmp_coords[i][0] = gaussrand(0.1, X_mid);
			tmp_coords[i][1] = gaussrand(0.1, Y_mid);
		}
	}

	// prepare object
	init(tmp_coords);
	// all known coordinates: set as anchor
	for (uint16_t i = 0; i < nNodes; i++) {
		if (tmpAnchors[i]) {
			// is an anchor
			setAnchor(i);
			printf("setAnchor(%d)\n", i);
		}
	}

	for (uint32_t i = 0; i < nEdges; i++) {
		printf("handle edge(%lu)\n", i);
		uint16_t nodeA = 0, nodeB = 0;
		double rssi;
		uint32_t nRssi;
		ntoh_vec_any(nodeA,	datastream, ptr); ptr += sizeof(uint16_t);
		ntoh_vec_any(nodeB,	datastream, ptr); ptr += sizeof(uint16_t);
		ntoh_vec_any(nRssi,	datastream, ptr); ptr += sizeof(uint32_t);
		rssi = nRssi / 100.0;
		int idxA = nameres[nodeA];
		int idxB = nameres[nodeB];
		setDissimilarities(idxA, idxB, rssi);
		printf("ds: idxA(%d)nodeA(%u), idxB(%d)nodeB(%u), rssi(%f)\n",
			idxA,nodeA,
			idxB,nodeB,
			rssi);
	}
	return 0;	
}



int NMDS::
initFromTxtFile (std::string &datastream) {
	double X_min = 0, X_max = 0;
	double Y_min = 0, Y_max = 0;
	double X_mid = 0, Y_mid = 0;
	bool coordsInitialized = false;

	std::istringstream in(datastream);

	// header information
	int nNodes, nAnchors, nEdges;
	in >> nNodes >> nAnchors >> nEdges;

	printf("nNodes(%d) nAnchors(%d) nEdges(%d) \n", nNodes, nAnchors, nEdges);

	// plausibility check:
	if (nAnchors > nNodes)
		return -3;

	// content
	std::vector< std::vector<double> > tmp_coords =
		std::vector< std::vector<double> > 
		(nNodes, std::vector<double>(2));
	// node name resolution
	std::map<int,int> nameres;
	std::map<int,int> nameres_rev;
	std::vector<bool> tmpAnchors;

	for (int i = 0; i < nNodes; i++) {
		int node = 0;
		in >> node;
		printf("found node(%d)\n", node);
		nameres[node] = i;
		nameres_rev[i] = node;
		tmpAnchors.push_back(false);
	}
	for (int i = 0; i < nAnchors; i++) {
		int node = 0;
		in >> node;
		// [LAT] [LON] (position)	[4] [4]
		double X, Y;
		double nLat, nLng;
		in >> nLat >> nLng;
		X = nLng;
		Y = nLat;
		int idx = nameres[node];
		tmp_coords[idx][0] = X;
		tmp_coords[idx][1] = Y;
		tmpAnchors[idx] = true;
		printf("coords found: idx(%d -> %d), (%.4f,%.4f)\n", idx, node, X,Y);
		if (!coordsInitialized) {
			X_min = X;
			X_max = X;
			Y_min = Y;
			Y_max = Y;
			coordsInitialized = true;
		}
		X_min = (X_min > X ? X : X_min);
		X_max = (X_max < X ? X : X_max);
		Y_min = (Y_min > Y ? Y : Y_min);
		Y_max = (Y_max < Y ? Y : Y_max);
	}
	// coordinates midpoint
	X_mid = X_min + (X_max - X_min) / 2;
	Y_mid = Y_min + (Y_max - Y_min) / 2;
	printf("Coordinate--Stuff: (%.4f)(%.4f)\n", X_mid, Y_mid);
	
	// all unknown coordinates: place random
	std::map<int,int>::iterator it;
	for (int i = 0; i < nNodes; i++) {
		if (!tmpAnchors[i]) {
			// is not an anchor
			tmp_coords[i][0] = gaussrand(0.1, X_mid);
			tmp_coords[i][1] = gaussrand(0.1, Y_mid);
			printf("no anchor: %d, random coords xy(%.4f, %.4f)\n", i,
				tmp_coords[i][0], tmp_coords[i][1]);
		}
	}

	// prepare object
	init(tmp_coords);
	// all known coordinates: set as anchor
	for (int i = 0; i < nNodes; i++) {
		if (tmpAnchors[i]) {
			// is an anchor
			setAnchor(i);
			printf("setAnchor(%d)\n", i);
		}
		printf("no anchor: %d\n", i);
	}

	for (int i = 0; i < nEdges; i++) {
		printf("handle edge(%lu)\n", i);
		int nodeA = 0, nodeB = 0;
		double rssi;
		int nRssi;
		in >> nodeA >> nodeB >> nRssi;
		rssi = nRssi / 100.0;
		int idxA = nameres[nodeA];
		int idxB = nameres[nodeB];
		setDissimilarities(idxA, idxB, rssi);
		printf("ds: idxA(%d)nodeA(%u), idxB(%d)nodeB(%u), rssi(%f)\n",
			idxA,nodeA,
			idxB,nodeB,
			rssi);
	}
	return 0;	
}



void NMDS::
printDists () {
	printf("\nDistances:");
	int elements = dists.size();
	printf("\n   |  ");
	for (size_t i = 0; i<elements; ++i) {
		printf("%02u |  ", i);
	}
	for (size_t i = 0; i<elements; ++i) {
		printf("\n%02u | ", i);
		for (size_t j = 0; j<elements; ++j) {
			if (j<i)
				printf("%4.1f| ", dists[i][j]);
			else
				printf(" -  | ");
		}
	}
	printf("\n");
}



void NMDS::
printDispars () {
	printf("\nDisparities:");
	int elements = disparities.size();
	printf("\n   |  ");
	for (size_t i = 0; i<elements; ++i) {
		printf("%02u |  ", i);
	}
	for (size_t i = 0; i<elements; ++i) {
		printf("\n%02u | ", i);
		for (size_t j = 0; j<elements; ++j) {
			if (j<i) {
				printf("%4.1f| ", disparities[i][j]);
			} else {
				printf(" -  | ");
			}
		}
	}
	printf("\n");
}



void NMDS::
printCoords () {
	printf("Coordinates:\n");
	printf("  Idx\t| \tX\t| \tY\t|\n");
	for (size_t i = 0; i<coordinates.size(); ++i) {
		if (anchors[i])
			printf(" %3u A*\t| %8.4f\t| %8.4f\t|\n", i, coordinates[i][0], coordinates[i][1]);
		else
			printf(" %3u\t| %8.4f\t| %8.4f\t|\n", i, coordinates[i][0], coordinates[i][1]);
	}
}



void NMDS::
printDissimilarities () {
	printf("\nDissimilarities: (%u)\n",dissimilarities.size());
	printf("  D\t|  i\t|  j\t|\n");
	for (size_t i = 0; i<dissimilarities.size(); ++i) {
		printf("%5.1f\t| %2d\t| %2d\t|\n", 
			dissimilarities[i][0],
			(int)dissimilarities[i][1],
			(int)dissimilarities[i][2]);
	}
	printf("=========================\n");
}



void NMDS::
printRelatives () {
	printf("\nRelatives (dist/diss)");
	//int elems = disparities.size();
	printf("\n   |  ");
	for (size_t i = 0; i<elems; ++i) {
		printf("%02u |  ", i);
	}
	for (size_t i = 0; i<elems; ++i) {
		printf("\n%02u | ", i);
		for (size_t j = 0; j<elems; ++j) {
			if (j<i) {
				if (valid[i][j]){
					int idx = (i*elems + j) - (i+1)*(i+2)/2;
					printf("%4.2f| ", dists[i][j]/dissimilarities[idx][0]);
				} else if (dists[i][j] >= threshold){
					printf(" ok | ");
				} else {
					printf(" <TH| ");
				}
			} else {
				printf(" -  | ");
			}
		}
	}
	printf("\n");
}



void NMDS::
setAnchor (int i) {
	anchors[i] = true;
}



void NMDS::
setDissimilarities (int i, int j, double d) {
	int idx = (i*elems + j) - (i+1)*(i+2)/2;
	dissimilarities[idx][0] = d;
	if (d != 0) {
		valid[i][j] = true;
		valid[j][i] = true;
	}
	//dissimilarities[idx][1] = i;
	//dissimilarities[idx][2] = j;
}



double NMDS::
calcDistance (double LON1, double LAT1, double LON2, double LAT2) {
	
	const int R = 6371000; // 6371 km
	double phi1 = LAT1 * M_PI / 180.0;
	double phi2 = LAT2 * M_PI / 180.0;
	double delPhi = (LAT2-LAT1) * M_PI / 180.0;
	double delLam = (LON2-LON1) * M_PI / 180.0;

	double a = 	sin(delPhi/2) * sin(delPhi/2) +
				cos(phi1) * cos(phi2) *
				sin(delLam/2) * sin(delLam/2);
	double c = 2* atan2(sqrt(a),sqrt(1-a));
	return c * R;
}



void NMDS::
calcDistances () {
	for (size_t i = 0; i < coordinates.size(); ++i) {
		for (size_t j = 0; j <= i; ++j) {
			if (i == j) {
				dists[i][j] = 0;
			} else {
				/* // euclidean - dont use!
				double d = sqrt(pow(coordinates[i][0]-coordinates[j][0], 2)
							+ pow(coordinates[i][1]-coordinates[j][1], 2));
							*/
				double d = calcDistance(
					coordinates[i][0],
					coordinates[i][1],
					coordinates[j][0],
					coordinates[j][1]);
				dists[i][j] = d;
				dists[j][i] = d;
			}
		}
	}
}



void NMDS::
calcDisparities () {
	size_t sz = dissimilarities.size();

	//printDispars();
	/*for (size_t e = 0; e < sz; ++e) {
		
		double diss1 = dissimilarities[e][0];
		int i1 = (int)dissimilarities[e][1];
		int j1 = (int)dissimilarities[e][2];
		double dist1 = dists[i1][j1];
		disparities[i1][j1] = diss1;
		disparities[j1][i1] = diss1;
	}*/
	if (sz == 0)
		return;
	int i = (int)dissimilarities[0][1];
	int j = (int)dissimilarities[0][2];
	double dist = dists[i][j];
	disparities[i][j] = dist;
	bool violation = false;


	for (size_t e = 1; e < sz; ++e) {
		
		//double diss1 = dissimilarities[e-1][0];
		int i1 = (int)dissimilarities[e-1][1];
		int j1 = (int)dissimilarities[e-1][2];
		double dist1 = dists[i1][j1];

		//double diss2 = dissimilarities[e][0];
		int i2 = (int)dissimilarities[e][1];
		int j2 = (int)dissimilarities[e][2];
		double dist2 = dists[i2][j2];


		if (dist2 < dist1) {
			// bei unmonotonitaet mitteln
			//printf("(%d, %d) >= (%d, %d)\n", i1,j1,i2,j2);
			double avg = (dist1+dist2)/2;
			disparities[i1][j1] = avg;
			disparities[i2][j2] = avg;
			disparities[j1][i1] = avg;
			disparities[j2][i2] = avg;
			violation = true;
		} else {
			// sonst wert uebernehmen
			//printf("(%d, %d) uebernommen\n", i2,j2);
			disparities[i2][j2] = dist2;
			disparities[j2][i2] = dist2;
		} 
		//printf("  (%d,%d), (%d,%d): cmp(%f, %f) --> %f\n",
		//	i1,j1,i2,j2,dist1,dist2,disparities[i2][j2]);
	}
	//printDispars();
	//violation = false;

	while (violation) {
		//printf("neuer durchgang\n");
		violation = false;
		for (size_t e = 1; e < sz; ++e) {
			
			//double diss1 = dissimilarities[e-1][0];
			int i1 = (int)dissimilarities[e-1][1];
			int j1 = (int)dissimilarities[e-1][2];
			double dist1 = disparities[i1][j1];

			//double diss2 = dissimilarities[e][0];
			int i2 = (int)dissimilarities[e][1];
			int j2 = (int)dissimilarities[e][2];
			double dist2 = disparities[i2][j2];

			if ((dist2+0.000001) < dist1) {
				// bei unmonotonitaet mitteln
				//printf("(%d, %d) >= (%d, %d) (%f <-> %f)\n", i1,j1,i2,j2, dist1, dist2);
				double avg = (dist1+dist2)/2;
				disparities[i1][j1] = avg;
				disparities[i2][j2] = avg;
				disparities[j1][i1] = avg;
				disparities[j2][i2] = avg;
				violation = true;
			} else {
				// sonst wert uebernehmen
				//printf("(%d, %d) uebernommen\n", i2,j2);
				disparities[i2][j2] = dist2;
				disparities[j2][i2] = dist2;
			}
		}
	}
}



void NMDS::
doRanking () {
	printf("start doRanking\n");
	int cnt = elems * (elems-1) / 2;
	printf("dr: cnt(%d), elems(%d)\n", cnt, elems);
	printf("size diss: %u\n", dissimilarities.size());
	if (dissimilarities.size() == 0)
		return;
	std::sort(dissimilarities.begin(), dissimilarities.begin()+cnt,
		[](const std::vector<double>& a, const std::vector<double>& b) {
			return a[0] < b[0];
		});
	threshold = ((int)dissimilarities[cnt-1][0]) +1;
	for (size_t i = 0; i < cnt; i++) {
		if (dissimilarities[i][0] > 0)
			break;
		dissimilarities[i][0] = threshold;
	}
	std::sort(dissimilarities.begin(), dissimilarities.begin()+cnt,
		[](const std::vector<double>& a, const std::vector<double>& b) {
			return a[0] < b[0];
		});
}



void NMDS::
doRound () {
	// first step: calc distances from coodinates
	calcDistances();
	// calc disparities from distances
	//printDists();

	calcDisparities();
	//printDispars();

	double alpha = 0.2;
	size_t sz = coordinates.size();
	// X-Coordinates:
	for (size_t i = 0; i<sz; ++i) {
		double sum = 0;
		int cnt = 0;
		if (anchors[i])
			continue; // never move an anchor..
		for (int j = 0; j<sz; ++j) {
			if (i == j)
				continue;
			if (valid[i][j]) {
				// normal calculation
				double adder = ( (1-(disparities[i][j] / dists[i][j])) 
							* (coordinates[j][0]-coordinates[i][0]) );
				if (anchors[j])
					adder *= 2;
				sum = sum + adder;
				cnt ++;
			} else {
				// unknown distance
				if (dists[i][j] < threshold) {
					// move away as usual (1.5)
					double adder = 1.5 *( (1-(disparities[i][j] / dists[i][j])) 
							* (coordinates[j][0]-coordinates[i][0]) );
					if (anchors[j])
						adder *= 2;
					sum = sum + adder;
					cnt ++;
				} // else: distance > threshold: ignore
			}
		}
		double xi = coordinates[i][0] + (alpha / (cnt-1)) * sum;
		//printf("X(%d): xi(%f) = co(%f)+al(%.1f)/(%u) * sum(%f)\n",
		//	i, xi, coordinates[i][0], alpha, (sz-1), sum);
		coordinates[i][0] = xi;
	}
	// Y-Coordinates:
	for (size_t i = 0; i<sz; ++i) {
		double sum = 0;
		int cnt = 0;
		if (anchors[i])
			continue; // never move an anchor..
		for (int j = 0; j<sz; ++j) {
			if (i == j)
				continue;
			if (valid[i][j]) {
				// normal calculation
				double adder = ( (1-(disparities[i][j] / dists[i][j])) 
							* (coordinates[j][1]-coordinates[i][1]) );
				if (anchors[j])
					adder *= 2;
				sum = sum + adder;
				cnt ++;
			} else {
				// unknown distance
				if (dists[i][j] < threshold) {
					// move away as usual
					double adder = 1.5 *( (1-(disparities[i][j] / dists[i][j])) 
							* (coordinates[j][1]-coordinates[i][1]) );
					if (anchors[j])
						adder *= 2;
					sum = sum + adder;
					cnt ++;
				} // else: distance > threshold: ignore
			}
		}
		double yi = coordinates[i][1] + (alpha / (cnt-1)) * sum;
		coordinates[i][1] = yi;
	}
}



double NMDS::
getStress1 () {
	double sum1 = 0, sum2 = 0;
	for (int i = 0; i<coordinates.size(); i++) {
		for (int  j = i+1; j<coordinates.size(); j++) {
			if (valid[i][j]) {
				double adder = pow( (dists[i][j] - disparities[i][j]) ,2);

				//printf("\t (%d, %d): dist(%f)-diss(%f) sum1(%f)+ (%f)\n",
				//	i,j, dists[i][j], disparities[i][j], sum1, adder);
				sum1 += adder;
			} /*else {
				//printf("\t (%d, %d): ignoriert\n", i,j );
			}*/
		}
	}
	for (int i = 0; i<coordinates.size(); i++) {
		for (int  j = i+1; j<coordinates.size(); j++) {
			if (valid[i][j]) 
				sum2 += pow( dists[i][j] ,2);
		}
	}
	if(sum1 == 0)
		return 0;
	//printf("stress1: sum1=%f, sum2=%f\n", sum1, sum2);
	return sqrt(sum1 / sum2);
}



double NMDS::
getStress2 () {
	double sum1 = 0, sum2 = 0, sum3 = 0;
	for (int i = 0; i<coordinates.size(); i++) {
		for (int  j = i+1; j<coordinates.size(); j++) {
			if (valid[i][j]) 
				sum1 += pow( (dists[i][j] - disparities[i][j]) ,2);
		}
	}

	// avg distances:
	int cnt = 0;
	for (int i = 0; i<coordinates.size(); i++) {
		for (int  j = i+1; j<coordinates.size(); j++) {
			if (valid[i][j]) {
				sum3 += dists[i][j];
				cnt ++;
			}
		}
	}
	if (cnt == 0)
		return 0;
	double avg = sum3/cnt;

	for (int i = 0; i<coordinates.size(); i++) {
		for (int  j = i+1; j<coordinates.size(); j++) {
			if (valid[i][j]) 
				sum2 += pow( dists[i][j]-avg ,2);
		}
	}
	return sqrt(sum1 / sum2);
}