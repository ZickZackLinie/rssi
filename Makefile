prefix = /usr/local
bindir = $(prefix)/bin
sharedir = /etc/rssipos

CC=g++
MDIR=-I$(CURDIR)
CFLAGS=-c -Wall -pthread -std=c++11 $(MDIR)
LDFLAGS= -pthread

PKG_CONFIG ?= pkg-config

SOURCES_NC =	main.cpp netlinkconnector.cpp utils.cpp \
				clientinfo.cpp messreihe.cpp \
				verbindungspool.cpp \
				control.cpp \
				udpbuffer.cpp \
				packet.cpp
# Sendet befehle via broadcast/(pseudo)multicast
SOURCES_SN =	sendbc.cpp packet.cpp
# versuch des Nonmetric MultiDimensional Scaling Algorithmus
#   benoetigt spezielles eingabeformat
SOURCES_NMDS =	mainnmds.cpp nmds.cpp
# hilfsdatei: arbeitet auf binary saves der
#   ClientInformationen (ci_<host>_<mac>.bin)
SOURCES_T =     main_test.cpp utils.cpp \
                clientinfo.cpp messreihe.cpp \
				verbindungspool.cpp \
				udpbuffer.cpp \
				packet.cpp

OBJECTS_NC=	$(SOURCES_NC:.cpp=.o)
OBJECTS_SN=	$(SOURCES_SN:.cpp=.o)
OBJECTS_NMDS=$(SOURCES_NMDS:.cpp=.o)
OBJECTS_T = $(SOURCES_T:.cpp=.o)

EXE_NC=rssipos
EXE_SN=rssisend
EXE_NMDS=rssinmds
EXE_T=helper.x

LIBS += $(shell $(PKG_CONFIG) --libs libnl-3.0 libnl-genl-3.0)
CFLAGS += $(shell $(PKG_CONFIG) --cflags libnl-3.0 )

# OPTIONEN:
# Pause in ms zwischen BATMAN-OGM-Paketen
CFLAGS += -DBAT_POLL=250
# Zeit von rssipos zwischen zwei RSSI-Abfragen
CFLAGS += -DTM_POLL=250
# maximal gespeicherte RSSI-Daten pro Position
CFLAGS += -DMAX_DATA_KEEP=100
# mindestens erforderliche RSSI-Daten bis Versand
CFLAGS += -DNUM_TO_SIGNIFICANT=100
# Pfad zu MAC-Adressen-Zuordnungsdatei
CFLAGS += -DFILE_MACS=/etc/rssipos/rssi.macs
# Pfad zur Logdatei des Sendeprogramms
CFLAGS += -DFILE_LOGSND=/etc/rssipos/rssisend.log
# Pfad zum Zwischenspeicher-Verzeichnis
CFLAGS += -DDIR_QUICKSAVE=/var/www/html/map/web


all: $(SOURCES_NC) $(EXE_NC) \
	$(SOURCES_SN) $(EXE_SN) \
	$(SOURCES_NMDS) $(EXE_NMDS)

# main software (rssipos)
$(EXE_NC): $(SOURCES_NC) $(OBJECTS_NC)
	$(CC) $(LDFLAGS) $(OBJECTS_NC) $(LIBS) -o $@

# helping tool (rssisend)
$(EXE_SN): $(SOURCES_SN) $(OBJECTS_SN)
	$(CC) $(LDFLAGS) $(OBJECTS_SN) -o $@

# helping tool (rssinmds)
$(EXE_NMDS): $(SOURCES_NMDS) $(OBJECTS_NMDS)
	$(CC) $(LDFLAGS) $(OBJECTS_NMDS) -o $@

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm -f $(EXE_NC) $(OBJECTS_NC) \
	$(EXE_SN) $(OBJECTS_SN) \
	$(EXE_NMDS) $(OBJECTS_NMDS)

install: all
	install $(EXE_NC) -m 755 -D $(DESTDIR)$(bindir)/$(EXE_NC)
	install $(EXE_SN) -m 755 -D $(DESTDIR)$(bindir)/$(EXE_SN)
	install $(EXE_NMDS) -m 755 -D $(DESTDIR)$(bindir)/$(EXE_NMDS)
	install rssi.macs -m 644 -D $(DESTDIR)$(sharedir)/rssi.macs
	install ntp.server.conf -m 644 -D $(DESTDIR)$(sharedir)/ntp.server.conf
	install ntp.client.conf -m 644 -D $(DESTDIR)$(sharedir)/ntp.client.conf
	
helper: $(SOURCES_T) $(EXE_T)

$(EXE_T): $(SOURCES_T) $(OBJECTS_T)
	$(CC) $(LDFLAGS) $(OBJECTS_T) $(LIBS) -o $@
	rm -f $(OBJECTS_T)


