#include <iostream> // std::out
#include <thread>	// std::thread
#include <chrono>	// std::chrono::seconds
#include <mutex>    // std::mutex, std::unique_lock
#include <condition_variable>	// std::condition_variable, std::cv_status
#include <errno.h>	// errno number

#include <sys/types.h>
#include <unistd.h>	// ..., usleep
#include <stdlib.h> // printf
#include <string.h> // bzero, bcopy
#include <sys/socket.h>
#include <sys/poll.h> // polling sockets
#include <netinet/in.h>
#include <arpa/inet.h> // inet_aton
#include <netdb.h>
#include <stdio.h>
#include <ctime> // time_t and time(int)

#include <utils.h>
#include <verbindungspool.h>
#include <netlinkconnector.h>
#include <packet.h>
#include <udpbuffer.h>
#include <control.h>



NetlinkConnector * Control::pNC = nullptr;

UDPBuffer * Control::pUDPB = nullptr;
std::thread Control::th_UDPB;

struct in_addr Control::sin_addr_bc;// my BC address
struct in_addr Control::sin_addr;	// my own ip address
std::string Control::ssin_addr; 	// same, but string


// CARE ABOUT OWN STATE
int Control::state = 0;
std::mutex Control::mtx_state_acc;

int Control::
getState () {
	int ret = 0;
	mtx_state_acc.lock();
	ret = state;
	mtx_state_acc.unlock();
	return ret;
}
void Control::
setState (int st) {
	mtx_state_acc.lock();
	if (st >= state)
		state = st;
	mtx_state_acc.unlock();
}

// ==================================================================
// RECEIVING
// ==================================================================

std::thread Control::th_NW_RX;

void Control::
nwReceive () {
	int sockfd_all, sockfd_sg; // for all/bc and single ip
	bool allIPs = Options::listenAllIPs;
	struct sockaddr_in serv_addr_all;
	struct sockaddr_in serv_addr_sg;
	struct pollfd ufds[2];
	// other side's address
	struct sockaddr_in adr;
	const int yes = 1;

	// buffer for message:
	char buffer[PKT_BUF_SIZE];

	// create socket
	sockfd_all = socket(AF_INET, SOCK_DGRAM, 0);
	if(sockfd_all < 0)
		fprintf(stderr,"\033[1;33mERROR opening socket_all\033[0m");
	// create socket
	sockfd_sg = socket(AF_INET, SOCK_DGRAM, 0);
	if(sockfd_sg < 0)
		fprintf(stderr,"\033[1;33mERROR opening socket_sg\033[0m");
	
	// set up address information
	// use either _all on ANY itf, or _all on BC and _sg on own ip
	memset(&serv_addr_all, 0, sizeof(serv_addr_all));
	memset(&serv_addr_sg, 0, sizeof(serv_addr_sg));
	serv_addr_all.sin_family = AF_INET;
	serv_addr_sg.sin_family = AF_INET;
	if (allIPs) {
		serv_addr_all.sin_addr.s_addr = htonl(INADDR_ANY); // every channel?!
		serv_addr_sg.sin_addr.s_addr = htonl(INADDR_ANY);
	} else {
		serv_addr_all.sin_addr.s_addr = sin_addr_bc.s_addr; // my bc addr
		serv_addr_sg.sin_addr.s_addr = sin_addr.s_addr; // my own ip (bat0)
	}
	serv_addr_all.sin_port = htons(PT_UDP_BC);
	serv_addr_sg.sin_port = htons(PT_UDP_TK);

	// socketoptions - save address
	// in case of error shutdown
	// now it can get same address again
	setsockopt(sockfd_all, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));
	setsockopt(sockfd_sg, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));

	int optval;
	socklen_t optlen;
	getsockopt(sockfd_all, SOL_SOCKET, SO_BROADCAST, &optval, &optlen);
	if (optval != 0) {
		printf("\033[1;33mSO_BROADCAST on *_all aktiviert...\033[0m\n");
	}
	
	// bind socket to address
	int rc = bind( sockfd_all, (struct sockaddr *) &serv_addr_all, sizeof serv_addr_all);
	if(rc < 0){
		printf ("\033[1;33mcannot bind *_all on port %d (%s)\033[0m\n", PT_UDP_BC, inet_ntoa(serv_addr_all.sin_addr));
	} else {
		printf ("\033[1;33mbound *_all to port %d (%s)\033[0m\n\r", PT_UDP_BC, inet_ntoa(serv_addr_all.sin_addr));
	}
	//if (! allIPs) {
		rc = bind( sockfd_sg, (struct sockaddr *) &serv_addr_sg, sizeof serv_addr_sg);
		if(rc < 0){
			printf ("\033[1;33mcannot bind *_sg on port %d (%s)\033[0m\n", PT_UDP_TK, inet_ntoa(serv_addr_sg.sin_addr));
		} else {
			printf ("\033[1;33mbound *_sg to port %d (%s)\033[0m\n", PT_UDP_TK, inet_ntoa(serv_addr_sg.sin_addr));
		}
	//}
	// prepare socketset
	ufds[0].fd = sockfd_all;
	ufds[0].events = POLLIN;
	// do we need second socket?
	ufds[1].fd = sockfd_sg; // won't receive if not bound..
	ufds[1].events = POLLIN;

	// start receiving:
	socklen_t x = sizeof adr;
	int rv = 0;
	while(1){

		int cur_state = getState();
		while (cur_state != C_RUNNING) {
			cur_state = getState();
			if (cur_state > C_RUNNING) {
				close(sockfd_all);
				close(sockfd_sg);
				return; // someone switched us off, thats okay
			}
		}
		
		// poll! - wait 1000 ms
		rv = poll(ufds, 2, 1000);
		if (rv == -1) {
			close(sockfd_all);
			close(sockfd_sg);
			setState(C_SHUTDOWN);
			return;
		} else if (rv == 0) {
			// timeout
			// do something usefull ..?
		} else {
			// clean buffer
			memset(buffer,0,sizeof(buffer));
			// detect socket
			int len = 0;
			if (ufds[0].revents & POLLIN)
				len = recvfrom(sockfd_all, buffer, sizeof(buffer), 0, (struct sockaddr *)&adr, &x);
			else if (ufds[1].revents & POLLIN)
				len = recvfrom(sockfd_sg, buffer, sizeof(buffer), 0, (struct sockaddr *)&adr, &x);
			if (len < 0)
				fprintf(stderr, "recvfrom error)\n");
					
			// print result and message
			printf("\033[1;30mresult (%d bytes) from \033[0;37m%s\033[1;30m port %u \033[0m\n", // : '%s'\033[0m\n",
				len,
				inet_ntoa(adr.sin_addr),
				(unsigned)ntohs(adr.sin_port));//,
				//buffer);
			
			if (len >= 9) { // minimum size for Packet
				std::vector<uint8_t> datastream (buffer, buffer+len);
				Packet * pRXPacket = new Packet(datastream);
				pRXPacket->target = adr.sin_addr;
				
				/*if (pRXPacket->type == PKT_QUITALL) {
					printf("received kind of shutdown packet - so shutdown\n");
					close(sockfd_all);
					close(sockfd_sg);
					pUDPB->stop();
					setState(C_SHUTDOWN);
					delete pRXPacket;
					return;
				}*/
				pUDPB->receivedThis(pRXPacket);
			}
		}
	}
}

// ==================================================================
// TRANSMITTING
// ==================================================================

std::thread Control::th_NW_TX;

void Control::
nwTransmit () {
	int sockfd;
	struct sockaddr_in their_addr; // connector's address information
	int numbytes;
	int broadcast = 1; // ALWAYS BROADCASTING FLAG

	if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
		perror("socket creation sendUDP");
		setState(C_SHUTDOWN);
		return;
	}

	if (setsockopt(sockfd, SOL_SOCKET, SO_BROADCAST, &broadcast,
		sizeof broadcast) == -1) {
		perror("setsockopt (SO_BROADCAST) sendUDP");
		setState(C_SHUTDOWN);
		close(sockfd);
		return;
	}

	their_addr.sin_family = AF_INET;			// host byte order
	their_addr.sin_port = htons(PT_UDP_TK);	// short, network byte order
	
	while(1){

		int cur_state = getState();
		while (cur_state != C_RUNNING) {
			cur_state = getState();
			if (cur_state > C_RUNNING) {
				close(sockfd);
				return; // someone switched us off, thats okay
			}
		}
		
		// PREPARE PACKET
		Packet * pTXPacket = pUDPB->getOutPacket(); // blocking
		if (pTXPacket == nullptr) {
			setState(C_SHUTDOWN);
			pUDPB->stop();
			close(sockfd);
			return;
		}
		printf("\033[1;30mShall send Packet on %d:\033[0m ", pTXPacket->port);
		pTXPacket->printHdr();
		their_addr.sin_addr = pTXPacket->target;
		their_addr.sin_port = htons(pTXPacket->port);
		std::vector<uint8_t> datastream = pTXPacket->make();
		//printf("shall transmit: ");
		//pTXPacket->printHdr();
		std::string stype = pTXPacket->strPktType(pTXPacket->type);

		if ((numbytes = sendto(sockfd, &datastream[0], datastream.size(), 0,
				(struct sockaddr *)&their_addr, sizeof their_addr)) == -1) {
			perror("sendto sendUDP");
			setState(C_SHUTDOWN);
			delete pTXPacket;
			close(sockfd);
			return;
		}
		// CLEANUP
		delete pTXPacket;
		printf("\033[1;30mSENDER sent %d byts to %s (%s)\033[0m\n\n", numbytes,
			inet_ntoa(their_addr.sin_addr), stype.c_str());
	}
}

void Control::
nwSendToAll (Packet * pShape, bool viaBC, bool inclMe) {
	if (viaBC) {
		pShape->target = sin_addr_bc;
		pShape->port = PT_UDP_BC;
		pShape->useBC = true;
		pShape->cntsnd = PKT_MAX_RESEND; // send only once!
		//pShape->acked = true;
		//printf("Paket target %s:%d ", inet_ntoa(pShape->target), pShape->port);
		//pShape->printHdr();
		pUDPB->placeInBuffer(pShape, UDPB_BUFFER_OUT);
		return;
	}
}

std::vector<int> Control::
nwSendToList (Packet * pShape, std::vector<uint8_t> hosts) {
	std::vector<int> joblist;
	if ((hosts.size() == 1) && (hosts[0] == 0)) {
		joblist.push_back(pShape->job);
		nwSendToAll(pShape, true, true);
	} else {
		for (auto hostnum : hosts) {
			joblist.push_back(
				pUDPB->sendThis(
					pShape->type,
					pShape->content,
					hostToIP(hostnum) ) );
		}
		delete pShape;
	}
	return joblist;
}

// ==================================================================
// HANDLE PACKETS / DOING STUFF
// ==================================================================

std::thread Control::th_NW_HANDLE;

void Control::
nwHandle () {
	Packet * pFullPacket;
	std::thread sequenceWorker;
	
	while (1) {
		pFullPacket = pUDPB->getInPacket(); // blocking
		// 
		// handle the packet by type
		//
		if (pFullPacket == nullptr) {
			setState(C_SHUTDOWN);
			// check sequence worker
			mtx_sequence_acc.lock();
			int running = sequenceState;
			mtx_sequence_acc.unlock();
			if (running != C_UNINIT) {
				doSequenceStop();
				sequenceWorker.join();
				sequenceState = C_UNINIT;
			}
			break;
		}
		printf("\033[1;33mHANDLE PACKET: ");
		pFullPacket->printHdr();

		switch (pFullPacket->type) {

		case PKT_QUITALL: {
			printf("HANDLER: start stopping\n");
			setState(C_SHUTDOWN);
			printf("HANDLER: set C_SHUTDOWN\n");
			mtx_sequence_acc.lock();
			int running = sequenceState;
			mtx_sequence_acc.unlock();
			if (running != C_UNINIT) {
				doSequenceStop();
				sequenceWorker.join();
				sequenceState = C_UNINIT;
			}
			printf("HANDLER: stopped seq.worker\n");
			pUDPB->stop();
			printf("HANDLER: stopped buffer\n");
			delete pFullPacket;	return;
		}
		case PKT_EXECCMD: {
			uint16_t strLen;
			ntoh_vec_any(strLen, pFullPacket->content, 0);
			std::string command(
				pFullPacket->content.begin() + sizeof(uint16_t),
				pFullPacket->content.end() );
			if (strLen != command.size()) {
				printf("HANDLE: execcmd error: "
					"unexpected length (%zu) shall be %u\n",
					command.size(), strLen);
			} else {
				// assume correct string
				printf("HANDLE: exec command ended with %d\n", doExecCmd(command));
			}
			delete pFullPacket;	break;
		}
		case PKT_GETCIS: {
			std::vector<uint8_t> datastream;
			uint16_t maxPerStation;
			ntoh_vec_any(maxPerStation, pFullPacket->content, 0);
			// for every level available
			std::vector<uint16_t> levels = VerbindungsPool::get("")->getLevels(false);
			VerbindungsPool::get("")->serialize(datastream,false,maxPerStation, levels);
			pUDPB->sendThis(
				PKT_TAKECIS, datastream, pFullPacket->target);
			delete pFullPacket;	break;
		}
		case PKT_TAKECIS: { 
			ClientInfo * ci = new ClientInfo("00:00:00:00:00:00",0,0,CI_AN_UNKNOWN,0,0);
			if (ci->deserialize(pFullPacket->content) == 0) {
				std::string ciname = ci->getName();
				VerbindungsPool::mergeOrInsert(ci);
				ci = VerbindungsPool::get(ciname);
				if (doCreateDir(Options::usePathForOutFile + "/" + Options::curPrefix)==0) {
					std::vector<uint16_t> levels = ci->getLevels(false);
					// save every available distance file
					for (auto lvl : levels)
						ci->buildDistanceFiles(lvl, Options::curPrefix);
					// build histogram from ci to all available
					for (int _host = 1; _host <= (int)Options::MACS.size(); _host++)
						ci->buildHistFile(_host, Options::curPrefix);
				} else {
					printf("HANDLE: TAKECIS saving data error: "
						"returncode \n");
				}				
			}
			delete pFullPacket;	break;
		}
		case PKT_RMCIS: {
			VerbindungsPool::cleanUp(VP_RM_CIOTHER); // data and other cis
			delete pFullPacket;	break;
		}
		case PKT_SETMAX: {
			uint16_t maxsize;
			ntoh_vec_any(maxsize, pFullPacket->content, 0);
			VerbindungsPool::get("")->allSetMaxKeep(maxsize);
			delete pFullPacket; break;
		}
		case PKT_DATAPRINT: {
			VerbindungsPool::print(false, 5); // NUM_TO_SIGNIFICANT
			delete pFullPacket;	break;
		}
		case PKT_RMDATA: {
			VerbindungsPool::cleanUp(VP_RM_DATA); // just the data
			delete pFullPacket;	break;
		}
		case PKT_DATASAVE: {
			uint16_t strLen;
			ntoh_vec_any(strLen, pFullPacket->content, 0);
			std::string absDir(
				pFullPacket->content.begin() + sizeof(uint16_t),
				pFullPacket->content.end() );
			if (strLen != absDir.size()) {
				printf("HANDLE: saving data error: "
					"unexpected length (%zu) shall be %u\n",
					absDir.size(), strLen);
			} else {
				// assume correct string
				absDir += "/" + Options::curPrefix;
				int ret;
				if ((ret=doCreateDir(absDir))==0) {
					VerbindungsPool::saveAll(absDir);
				} else {
					printf("HANDLE: saving data error: "
						"returncode %d\n", ret);
				}
			}
			printf("HANDLE: saving data to %s\n", absDir.c_str());
			delete pFullPacket;	break;
		}
		case PKT_PREFIX: {
			uint16_t strLen;
			ntoh_vec_any(strLen, pFullPacket->content, 0);
			std::string prfx(
				pFullPacket->content.begin() + sizeof(uint16_t),
				pFullPacket->content.end() );
			if (strLen != prfx.size()) {
				printf("HANDLE: set prefix error: "
					"unexpected length (%zu) shall be %u\n",
					prfx.size(), strLen);
			} else {
				// assume correct string
				Options::curPrefix = prfx;
			}
			printf("HANDLE: changing prefix to %s\n", prfx.c_str());
			delete pFullPacket;	break;
		}
		case PKT_SEQ_SETLVL: {
			mtx_sequence_acc.lock();
			int running = sequenceState;
			mtx_sequence_acc.unlock();
			uint16_t level;
			mtime_t waittime;
			size_t ptr = 0;
			std::vector<uint8_t> datastream = pFullPacket->content;
			ptr+=ntoh_vec_any(level, datastream, ptr);
			ptr+=ntoh_vec_any(waittime, datastream, ptr);
			if (running == C_UNINIT) { // (re)init worker thread
				sequenceWorker = std::thread(
					doSequenceStart, 
					pFullPacket->target, 
					waittime, 
					level);
			} else if (running == C_RUNNING) { // change block
				mtx_sequence_acc.lock();
				sequenceLevel = level;
				mtx_sequence_acc.unlock();
			}
			delete pFullPacket;	break;
		}
		case PKT_SEQ_TERM: {
			mtx_sequence_acc.lock();
			int running = sequenceState;
			mtx_sequence_acc.unlock();
			if (running != C_UNINIT) {
				doSequenceStop();
				sequenceWorker.join();
				sequenceState = C_UNINIT;
			}
			delete pFullPacket;	break;
		}
		case PKT_TIMEDIFF: {
			uint8_t _host;
			ntoh_vec_any(_host, pFullPacket->content, 0);
			std::vector<uint8_t> datastream;
			// put current time into packet
			mtime_t tmnow = time(0);
			hton_vec_any(tmnow, datastream);
			struct in_addr target = hostToIP(_host);
			int ret = pUDPB->sendThis(PKT_TIMEDIFFME, datastream, target);
			printf("HANDLE: timediff to 192.168.2.%u: job %d\n", _host, ret);
			delete pFullPacket;	break;
		}
		case PKT_TIMEDIFFME: {
			size_t pktlen = pFullPacket->content.size();
			if (pktlen == (2 * sizeof(mtime_t)) ) {
				// is a response!
				size_t ptr = 0;
				mtime_t tmmine, tmother;
				ptr += ntoh_vec_any(tmmine, pFullPacket->content, ptr);
				ptr += ntoh_vec_any(tmother, pFullPacket->content, ptr);
				printf("HANDLE: timediff FULL \033[1;33mme --(%d)-> %s --(%d)-> me\033[0m\n",
					(tmother-tmmine),
					inet_ntoa(pFullPacket->target),
					((mtime_t)time(0))-tmother );			
			} else {
				mtime_t tmother;
				ntoh_vec_any(tmother, pFullPacket->content, 0);
				std::vector<uint8_t> datastream;
				// put current time into packet
				mtime_t tmnow = time(0);
				hton_vec_any(tmother, datastream);
				hton_vec_any(tmnow, datastream);
				int ret = pUDPB->sendThis(
					PKT_TIMEDIFFME,
					datastream,
					pFullPacket->target);
				printf("HANDLE: timediff PART \033[1;33m%s --(%d)-> me\033[0m: job %d\n",
					inet_ntoa(pFullPacket->target),
					tmnow-tmother, ret);
			}
			delete pFullPacket;	break;
		}
		case PKT_TIMESYNC: {
			printf("HANDLE: sync time ended with %d\n", doSetTime());
			delete pFullPacket;	break;
		}
		
		// DONT KNOW HOW TO HANDLE:
		case PKT_UNKNOWN:
		case PKT_ACK:
		case PKT_RESEND:
		default: {
			printf("HANDLE: cannot handle pkt type %d alias %c\n",
				pFullPacket->type, (char)pFullPacket->type);
			break; }
		}
	}
}

int Control::
doExecCmd(std::string cmd){
	int ret = system(cmd.c_str());
	printf("'%s' ausgefuehrt mit returncode %d\n", cmd.c_str(), ret);
	return ret;
}

int Control::
doSetTime(){
	int ret = system("service ntp stop && ntpd -gq && service ntp start");
	//int ret = system("service ntp restart");
	printf("'service ntp restart' ausgefuehrt mit returncode %d\n", ret);
	return ret;
}


// ==================================================================
// SEQUENCE CONTROL
// ==================================================================

std::mutex Control::mtx_sequence_acc;
int Control::sequenceState = C_UNINIT;
uint16_t Control::sequenceLevel = 0;

void Control::
doSequenceStart (struct in_addr master, mtime_t sleepSecs, uint16_t level) {
	int counter, ret;
	int cur_state;
	uint16_t cur_level = level;
	uint16_t new_level = level;
	bool levelChanged = true;

	mtx_sequence_acc.lock();
	sequenceState = C_RUNNING;
	sequenceLevel = level;
	printf("SEQ.WORKER: START NEW SEQUENCE IN %u\n", level);
	mtx_sequence_acc.unlock();
	
	/*while (sleepSecs > 0) {
		mtx_sequence_acc.lock();
		cur_state = sequenceState;
		mtx_sequence_acc.unlock();
		if (cur_state != C_RUNNING)
			return;
		printf("SEQ.WORKER: START SEQUENCE in %u sekunden\n", sleepSecs);
		sleep(1);
		sleepSecs--;
	}*/
	VerbindungsPool::cleanUp(VP_RM_CIOTHER); // delete everything (not me)
	
	while (1) {
		mtx_sequence_acc.lock();
		cur_state = sequenceState;
		new_level = sequenceLevel;
		if (new_level == 0) {
			// CONTIUOUSLY SCANNING and writing to box 0
			if (cur_level != 0)
				printf("SEQ.WORKER: START NEW SEQUENCE[--] IN 0\n");
			levelChanged = false;
			cur_level = 0;
			new_level = 0;
			sequenceLevel = 0;
		} else if ((new_level != cur_level) || levelChanged)  {
			levelChanged = true;
			cur_level = new_level; // move to block
			new_level = new_level +1; // after all this will be cur_lvl
			sequenceLevel = new_level;
		}
		mtx_sequence_acc.unlock();
		if (cur_state != C_RUNNING)
			return;

		// SPECIFIC SCAN
		if (levelChanged) {
			levelChanged = false;
			counter = NUM_TO_SIGNIFICANT;
			printf("SEQ.WORKER: START NEW SEQUENCE[%d] IN %u\n", counter, cur_level);
			while (counter > 0) {
				if ((ret = pNC->lookForRssi(cur_level)) < 0){
					mtx_sequence_acc.lock();
					sequenceState = C_OFF;
					mtx_sequence_acc.unlock();
					return;
				}
				usleep(TM_POLL * 1000);
				counter--;
			}
			cur_level = new_level; // begin new block
			counter = NUM_TO_SIGNIFICANT;
			/*printf("SEQ.WORKER: START NEW SEQUENCE[%d] IN %u\n", counter, cur_level);
			while (counter > 0) {
				if ((ret = pNC->lookForRssi(cur_level)) < 0){
					mtx_sequence_acc.lock();
					sequenceState = C_OFF;
					mtx_sequence_acc.unlock();
					return;
				}
				sleep(1);
				counter--;
			}*/
			// send report
			printf("SEQ.WORKER: SEQUENCE perfectly done....\n\n");
			std::vector<uint8_t> datastream;
			std::vector<uint16_t> levels;
			//levels.push_back(cur_level);
			levels.push_back(cur_level-1);
			VerbindungsPool::get("")->serialize(
				datastream,false,
				/*NUM_TO_SIGNIFICANT*/MAX_DATA_KEEP,levels);
			pUDPB->sendThis(PKT_TAKECIS, datastream, master);
		}
		// NORMAL CONTINUOUSLY SCAN
		else {
			if ((ret = pNC->lookForRssi(cur_level)) < 0){
				mtx_sequence_acc.lock();
				sequenceState = C_OFF;
				mtx_sequence_acc.unlock();
				return;
			}
			usleep(TM_POLL * 1000);
			//counter--;
		}
	}
}

void Control::
doSequenceStop () {
	mtx_sequence_acc.lock();
	sequenceState = C_OFF;
	mtx_sequence_acc.unlock();
}

// ==================================================================
// BATMAN-ADV CONTROL
// ==================================================================

int Control::
batadvStart (/*unsigned int mtu, unsigned int channel*/){
	int ret = 0;
	char command[255] = {0};
	const char * name = pNC->getItfName();

	// [set MTU to 1532]
	memset(command, 0, 255);
	sprintf(command, "ifconfig %s mtu %u", name, BAT_MTU);
	ret = system(command);
	printf("\033[0;36m'%s'\033[0m ausgefuehrt mit returncode %d\n", command, ret);
	if (ret != 0)
		return ret;
	// [set Itf down]
	memset(command, 0, 255);
	sprintf(command, "ifconfig %s down", name);
	ret = system(command);
	printf("\033[0;36m'%s'\033[0m ausgefuehrt mit returncode %d\n", command, ret);
	if (ret != 0)
		return ret;
	// [set Itf to ad-hoc mode]
	memset(command, 0, 255);
	sprintf(command, "iwconfig %s mode ad-hoc essid mymesh "
					"ap 02:12:34:56:78:9a channel %u", name, BAT_CHANNEL);
	ret = system(command);
	printf("\033[0;36m'%s'\033[0m ausgefuehrt mit returncode %d\n", command, ret);
	if (ret != 0)
		return ret;
	// [set Itf up]
	memset(command, 0, 255);
	sprintf(command, "ifconfig %s up", name);
	ret = system(command);
	printf("\033[0;36m'%s'\033[0m ausgefuehrt mit returncode %d\n", command, ret);
	if (ret != 0)
		return ret;
	// [load batadv module]
	memset(command, 0, 255);
	sprintf(command, "/sbin/modprobe batman-adv");
	ret = system(command);
	printf("\033[0;36m'%s'\033[0m ausgefuehrt mit returncode %d\n", command, ret);
	if (ret != 0)
		return ret;
	// [add Itf to batctl]
	memset(command, 0, 255);
	sprintf(command, "/usr/local/sbin/batctl if add %s", name);
	ret = system(command);
	printf("\033[0;36m'%s'\033[0m ausgefuehrt mit returncode %d\n", command, ret);
	if (ret != 0)
		return ret;
	// [set intervall time]
	memset(command, 0, 255);
	sprintf(command, "/usr/local/sbin/batctl it %u", BAT_POLL);
	ret = system(command);
	printf("\033[0;36m'%s'\033[0m ausgefuehrt mit returncode %d\n", command, ret);
	if (ret != 0)
		return ret;
	// [set Itf up again]
	memset(command, 0, 255);
	sprintf(command, "ip link set up %s", name);
	ret = system(command);
	printf("\033[0;36m'%s'\033[0m ausgefuehrt mit returncode %d\n", command, ret);
	if (ret != 0)
		return ret;
	// [set batman Itf up again]
	memset(command, 0, 255);
	sprintf(command, "ip link set up bat0");
	ret = system(command);
	printf("\033[0;36m'%s'\033[0m ausgefuehrt mit returncode %d\n", command, ret);
	if (ret != 0)
		return ret;
	// [set ip address on bat0]
	memset(command, 0, 255);
	sprintf(command, "ip addr add %s/24 broadcast %s dev bat0",
					ssin_addr.c_str(), inet_ntoa(sin_addr_bc));
	ret = system(command);
	printf("\033[0;36m'%s'\033[0m ausgefuehrt mit returncode %d\n", command, ret);
	if ((ret != 0) && (ret != 512)) // 512 = "file already exists"
		return ret;
	if (Options::handleNTP) {
		// [save original ntp configuration file]
		memset(command, 0, 255);
		sprintf(command, "mv /etc/ntp.conf /etc/rssipos/ntp.conf.orig");
		ret = system(command);
		printf("\033[0;36m'%s'\033[0m ausgefuehrt mit returncode %d\n", command, ret);
		if ((ret != 0) && (ret != 512)) // 512 = "file already exists"
			return ret;
		if (VerbindungsPool::get("")->hostnum == 1) {
			// [be ntp server]
			memset(command, 0, 255);
			sprintf(command, "ln -s /etc/rssipos/ntp.server.conf /etc/ntp.conf");
			ret = system(command);
			printf("\033[0;36m'%s'\033[0m ausgefuehrt mit returncode %d\n", command, ret);
			if ((ret != 0) && (ret != 512)) // 512 = "file already exists"
				return ret;
		} else {
			// [be ntp client]
			memset(command, 0, 255);
			sprintf(command, "ln -s /etc/rssipos/ntp.client.conf /etc/ntp.conf");
			ret = system(command);
			printf("\033[0;36m'%s'\033[0m ausgefuehrt mit returncode %d\n", command, ret);
			if ((ret != 0) && (ret != 512)) // 512 = "file already exists"
				return ret;
		}
	}
	return 0;
}

int Control::
batadvStop (int seconds){
	int ret = 0;
	char command[255] = {0};
	const char * name = pNC->getItfName();

	// [wait seconds to disable network]
	for (int i = seconds; i>0; i--) {
		printf("\tshutting down batman-adv in %ds...\n", i);
		sleep(1);
	}

	// [deactivate batman-adv]
	memset(command, 0, 255);
	sprintf(command, "/usr/local/sbin/batctl if del %s", name);
	ret = system(command);
	printf("\033[0;36m'%s'\033[0m ausgefuehrt mit returncode %d\n", command, ret);
	if (ret != 0)
		return ret;
	
	if (Options::handleNTP) {
		// [remove our ntp symlink]
		memset(command, 0, 255);
		sprintf(command, "unlink /etc/ntp.conf");
		ret = system(command);
		printf("\033[0;36m'%s'\033[0m ausgefuehrt mit returncode %d\n", command, ret);
		if ((ret != 0) && (ret != 512)) // 512 = "file already exists"
			return ret;
		
		// [get back original ntp configuration file]
		memset(command, 0, 255);
		sprintf(command, "mv /etc/rssipos/ntp.conf.orig /etc/ntp.conf");
		ret = system(command);
		printf("\033[0;36m'%s'\033[0m ausgefuehrt mit returncode %d\n", command, ret);
		if ((ret != 0) && (ret != 512)) // 512 = "file already exists"
			return ret;
	}

	return 0;
}



// ==================================================================
// PUBLIC STUFF
// ==================================================================


int Control::
setup () {
	
	int st = getState();
	if (st != C_UNINIT)
		return 1; // already set up - never do twice!
	setState(C_STARTING);
	
	// Setup Listening Interface
	// via Connector
	pNC = new NetlinkConnector ();

	bool itfFound = false;
	if (Options::smac_address != "")
		itfFound = pNC->lookForMac(Options::smac_address.c_str());
	else
		itfFound = pNC->lookForMac(0);

	// Result Interface
	char macstring[ETH_CLEN] = {0};
	uint32_t txpower = 0;
	uint8_t hostnum = 0;
	
	if (itfFound) {
		mac_addr_n2a(macstring, pNC->getItfMac());
		txpower = pNC->getTxPower();
		// get ip address from MAC
		int idxOfMac = get_num_of_mac(macstring);
		if (idxOfMac < 0 ) {
			if (Options::hostnum > 0) {
				hostnum = (uint8_t) Options::hostnum;
			} else {
				printf("keine Hostnummer zu unbekanntem Itf spezifiziert\n");
				delete pNC;
				return 3;
			}
		} else {
			hostnum = (uint8_t)(idxOfMac +1);
		}
		
		char ipaddress[16] = {0};
		sprintf(ipaddress, "192.168.2.%u", hostnum);
		ssin_addr = std::string(ipaddress);
		printf("baue NC ueber %s auf (ip=%s)\n", macstring, ipaddress);
		inet_aton(ipaddress, &sin_addr);
		//inet_aton("129.168.2.255", &sin_addr_bc);
		inet_aton("192.168.2.255", &sin_addr_bc);
	
	} else {
		printf("kein passendes Itf gefunden\n");
		delete pNC;
		return 2;
	}

	// create ClientInfo
	VerbindungsPool::createMyInfo(	macstring, 
									hostnum,
									txpower,
									Options::anchorState,
									Options::latitude,
									Options::longitude	);

	int ret = 0;
	// do the batman-adv startup if wanted
	if (Options::handleBatAdv) {
		ret = batadvStart();
		printf("Batman-adv gestartet mit returncode %d\n",ret);
		if (ret != 0)
			delete pNC;
	}
	return ret;
}


int Control::
startNetworking () 
{
	// ===============================
	// 1. set up buffer
	//    wait until is ready to use
	// 2. set up receiving packets
	// 3. set up transmitting packets
	// 4. set up handling packets
	// 5. set up state to run all
	// 6. join all
	// 7. stop batman-adv if necessary
	// 8. free buffer
	// 9. return (RESTART) | (QUIT)
	// ===============================
	
	// ===============================
	// x. set up buffer
	//    wait until is ready to use
	pUDPB = new UDPBuffer();
	th_UDPB = std::thread(UDPBuffer::start_thread, pUDPB);
	int curUDPBstate = UDPB_S_STARTING;
	while ((curUDPBstate = pUDPB->getState()) == UDPB_S_STARTING) {
		printf("C-NW: warte auf UDPB...\n");
		sleep(1);
	}
	// x. set up receiving packets
	th_NW_RX = std::thread( nwReceive );
	// x. set up transmitting packets
	th_NW_TX = std::thread( nwTransmit );
	// x. set up handling packets
	th_NW_HANDLE = std::thread( nwHandle );
	// x. set up state to run all
	setState(C_RUNNING);
	

	// find out neighborhood
	// determine if master
	// foreach let synchronize time
	// foreach send timetable
	// x. do cool stuff
	/*for (int i = 0; i < 20; i++ ) {
		pNC->lookForRssi();
		sleep(1);
	}
	VerbindungsPool::buildDistanceFiles("5m");*/
	/*
	Packet * shape = new Packet (
		pUDPB->getJobNum(), 
		PKT_SEQHDLCRT, 1, 1, sin_addr);
	uint8_t distance = 5;
	mtime_t waittime = 20;
	uint8_t continueOnMissingNodes = 1;
	hton_vec_any(distance, shape->content);
	hton_vec_any(waittime, shape->content);
	hton_vec_any(continueOnMissingNodes, shape->content);

	printf("=========================\n");
	sleep(5);
	pUDPB->placeInBuffer(shape, UDPB_BUFFER_OUT);

	sleep(5);
	int cur_state = C_UNINIT;
	int counter = 1;
	while ((cur_state != C_OFF) && (counter < 120)) {
		sleep(1);
		counter ++;
		mtx_sequenceHandler_acc.lock();
		cur_state = sequenceHandlerState;
		mtx_sequenceHandler_acc.unlock();
	}
	sleep(10);

	printf("Shutdown in 20 sekunden....\n\n");
	sleep(20);
	pUDPB->stop();*/


	// x. join all
	th_NW_HANDLE.join();
	th_NW_RX.join();
	th_NW_TX.join();
	th_UDPB.join();
	// x. stop batman-adv if necessary
	int ret = 0;
	if (Options::handleBatAdv) {
		ret = batadvStop(10);
		printf("Batman-adv beendet mit returncode %d\n",ret);
	}
	// x. free buffer
	delete pUDPB;
	// x. return (RESTART) | (QUIT)
	return (ret + 1);
}
