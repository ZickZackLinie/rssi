#include <cstdio> // printf
#include <cstdlib> // rand()
#include <fstream> // read from file given at argv
//#include <clientinfo.h>

#include <nmds.h>

int main(int argc, char *argv[]) {
	
	std::string filename = "";
	bool useBin = false;

	if (argc == 1)
		return 1; // no input given
	for (int i = 1; i < argc; i++) {
		if (std::string(argv[i]) == "seed") {
			int seed = atoi(argv[i+1]);
			srand(seed);
			i++;
		} else if (std::string(argv[i]) == "txt") {
			useBin = false;
		} else if (std::string(argv[i]) == "bin") {
			useBin = true;
		} else if (std::string(argv[i]) == "path") {
			filename = std::string(argv[i+1]);
			i++;
		} else {
			printf("usage: %s [<args>]\n", argv[0]);
		}
	}

	if (filename.size() == 0)
		return 1;

	NMDS a;
	
	if (useBin) {
		// read from binary file
		std::ifstream infile(filename.c_str(), std::ifstream::in | std::ifstream::binary);
		char buffer[8192] = {0};
		infile.seekg (0,infile.end);
		int length = infile.tellg();
		infile.seekg (0,infile.beg);
		printf("read %d bytes from file %s\n", length, argv[1]);
		if (length>=8192)
			return -1;
		infile.read(buffer,length);
		int used = infile.gcount();
		if (!infile) {
			printf("only %d bytes read, closing\n", used);
			return -2;
		}
		std::vector<uint8_t> streambuffer;
		streambuffer.assign(buffer, buffer+used);
		printf("%u bytes in streambuffer\n", streambuffer.size());
		
		int ret = a.initFromBinFile(streambuffer);
		if (ret < 0) {
			printf("ERROR in initFromFile: %d\n", ret);
			return ret;
		}
	} else {
		// read from text file
		std::ifstream infile(filename.c_str(), std::ifstream::in);
		char buffer[8192] = {0};
		infile.seekg (0,infile.end);
		int length = infile.tellg();
		infile.seekg (0,infile.beg);
		printf("read %d bytes from file %s\n", length, argv[1]);
		if (length>=8192)
			return -1;
		infile.read(buffer,length);
		int used = infile.gcount();
		if (!infile) {
			printf("only %d bytes read, closing\n", used);
			return -2;
		}
		std::string streambuffer = std::string(buffer);
		
		int ret = a.initFromTxtFile(streambuffer);
		if (ret < 0) {
			printf("ERROR in initFromFile: %d\n", ret);
			return ret;
		}
	}
	std::vector<double> stresses;
	printf("initialisiert\n");
	a.doRanking();
	a.printDissimilarities();
	a.calcDistances();
	a.calcDisparities();
	stresses.push_back(a.getStress1());
	stresses.push_back(a.getStress2());
	double s0 = 1, s1 = 1, s2 = 1;
	for (int r = 0; r <40; r++) {
		printf("ROUND (%d)\n", r);
		a.doRound();
		a.calcDistances();
		//a.printDists();
		a.calcDisparities();
		s0 = s1;
		s1 = s2;
		s2 = a.getStress1();
		stresses.push_back(s2);
		stresses.push_back(a.getStress2());
		if (((s2+0.0001) >= s1) && ((s1+0.0001) >= s0) ||
			(s2 >= s1) && (s1 >= s0))
			break;
	}
	a.printCoords();
	for (int s = 0; s < stresses.size(); s+=2) {
		printf("Stress1 (%d) = %.3f, Stress2 = %.3f\n", s/2, stresses[s], stresses[s+1]);
	}
	printf("======= AFTER\n");
	a.printDispars();
	a.printDists();
	a.printRelatives();
	return 0;
	

	/*std::vector< std::vector<double> > coordinates(4, std::vector<double> (2));
	unsigned int PRNG_SEED = 1024;
	srand(PRNG_SEED);
	coordinates[0][0] = 1;
	coordinates[0][1] = 1;
	coordinates[1][0] = gaussrand(2.0, 3); //4;
	coordinates[1][1] = gaussrand(2.0, 3); //2;
	coordinates[2][0] = 5;
	coordinates[2][1] = 4;
	coordinates[3][0] = gaussrand(2.0, 3); //3;
	coordinates[3][1] = gaussrand(2.0, 3); //3;*/
	
	/*srand(PRNG_SEED);
	double b1 = gaussrand(2.0, 3);
	double b = gaussrand(2.0, 3);
	double c = gaussrand(2.0, 3);
	double d = gaussrand(2.0, 3);
	printf("(a+b+c+d)/4=%f\n", (b1+b+c+d)/4);
	printf("%f, %f, %f, %f\n", b1,b,c,d);
	double xsum = 0.0;
	for (int x = 0; x < 100; x++) {
		xsum += gaussrand(2.0, 10);
	}
	printf("%f\n", xsum/100);*/

	/*coordinates[0][0] = 2.77;
	coordinates[0][1] = 1.96;
	coordinates[1][0] = 3.73;
	coordinates[1][1] = 1.75;
	coordinates[2][0] = 4.08;
	coordinates[2][1] = 3.04;
	coordinates[3][0] = 2.45;
	coordinates[3][1] = 3.29;*/

	//NMDS a;
	//a.init(coordinates);
	
	/*int idx = 0;
	for (int i = 0; i<4; i++) {
		for (int  j = 0; j<i; j++) {
			a.setDissimilarities (idx++, i,j,3);
		}
	}*/
	/*double val1 = 3.3;
	//double val2 = 4.7;
	a.setDissimilarities(0,1,val1);
	//a.setDissimilarities(0,2,val2);
	a.setDissimilarities(0,3,val1);
	a.setDissimilarities(1,2,val1);
	//a.setDissimilarities(1,3,val2);
	a.setDissimilarities(2,3,val1);
	
	a.setAnchor(0); // dont move first node
	a.setAnchor(2); // neither third
	a.printCoords();
	
	std::vector<double> stresses;
	
	a.doRanking();
	a.printDissimilarities();
	a.calcDistances();
	a.calcDisparities();
	stresses.push_back(a.getStress1());
	stresses.push_back(a.getStress2());
	double s0 = 1, s1 = 1, s2 = 1;
	for (int r = 0; r <40; r++) {
		printf("ROUND (%d)\n", r);
		a.doRound();
		a.calcDistances();
		//a.printDists();
		a.calcDisparities();
		s0 = s1;
		s1 = s2;
		s2 = a.getStress1();
		stresses.push_back(s2);
		stresses.push_back(a.getStress2());
		if (((s2+0.0001) >= s1) && ((s1+0.0001) >= s0))
			break;
	}
	a.printCoords();
	for (int s = 0; s < stresses.size(); s+=2) {
		printf("Stress1 (%d) = %.3f, Stress2 = %.3f\n", s/2, stresses[s], stresses[s+1]);
	}
	printf("======= AFTER\n");
	a.printDispars();
	a.printDists();
	a.printRelatives();

	
	return 0;*/
}