# set terminal png transparent nocrop enhanced size 450,320 font "arial,8" 
# set output 'heatmaps.3.png'
unset key
set view map scale 1
set style data lines
set xtics border in scale 0,0 mirror norotate  autojustify
set ytics border in scale 0,0 mirror norotate  autojustify
set ztics border in scale 0,0 nomirror norotate  autojustify
unset cbtics

#set style line 100 lt 1 lc rgb "gray" lw 2
#set style line 101 lt 0.5 lc rgb "gray" lw 1
#set grid mxtics mytics ls 100, ls 101
#set grid xtics ytics ls 100, ls 101

#set mxtics 10
#set mytics 10
set xtics 5
set ytics 4
set grid front
#set rtics axis in scale 0,0 nomirror norotate  autojustify
#set paxis 1 tics border in scale 0,0 nomirror norotate  autojustify
#set paxis 1 tics autofreq  rangelimit
#set paxis 2 tics border in scale 0,0 nomirror norotate  autojustify
#set paxis 2 tics autofreq  rangelimit
#set paxis 3 tics border in scale 0,0 nomirror norotate  autojustify
#set paxis 3 tics autofreq  rangelimit
#set paxis 4 tics border in scale 0,0 nomirror norotate  autojustify
#set paxis 4 tics autofreq  rangelimit
#set paxis 5 tics border in scale 0,0 nomirror norotate  autojustify
#set paxis 5 tics autofreq  rangelimit
#set paxis 6 tics border in scale 0,0 nomirror norotate  autojustify
#set paxis 6 tics autofreq  rangelimit
#set paxis 7 tics border in scale 0,0 nomirror norotate  autojustify
#set paxis 7 tics autofreq  rangelimit
#set title "Heat map with non-zero pixel values written as labels" 
set yrange [ 20.500000 : 86.50000 ] noreverse nowriteback
set xrange [ -0.500000 : 101.50000 ] noreverse nowriteback
set xlabel "Entfernung [m]"
set cblabel "Score" 
set cbrange [ 0.00000 : 50] noreverse nowriteback
#set palette rgbformulae 33,13,10
set palette model RGB rgbformulae -4,-4,2
x = 0.0
## Last datafile plotted: "$map1"
datei(nname, nhA, nhB)=sprintf("/var/www/html/map/web/%s/h%d_%d.txt", nname, nhA, nhB)
set title datei(hname,hA,hB)
plot datei(hname,hA,hB) using 2:1:3 with image,      datei(hname,hA,hB) using 2:1:($3 == 0 ? "" : sprintf("%g",$3) ) with labels
